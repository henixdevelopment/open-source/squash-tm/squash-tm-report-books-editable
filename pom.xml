<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>org.squashtest.tm.plugin</groupId>
  <artifactId>report.books.editable</artifactId>
  <version>${revision}${sha1}${changelist}</version>
  <packaging>pom</packaging>
  <name>report-books-editable</name>
  <description>Squash reports plugins (Requirements and TestCases)</description>
  <organization>
    <name>Henix, henix.fr</name>
    <url>http://www.squashtest.org</url>
  </organization>
  <inceptionYear>2010</inceptionYear>
  <issueManagement>
    <system>mantis</system>
    <url>https://ci.squashtest.org/mantis</url>
  </issueManagement>

  <licenses>
    <license>
      <name>GNU Lesser General Public License V3</name>
    </license>
  </licenses>

  <properties>
    <!-- PROJECT -->
    <revision>10.0.0</revision>
    <sha1></sha1>
    <changelist>-SNAPSHOT</changelist>
    <java.version>17</java.version>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

    <!-- DEPENDENCIES -->
    <squash.core.version>10.0.0-SNAPSHOT</squash.core.version>

    <!-- MAVEN PLUGINS -->
    <ci-friendly.version>1.0.20</ci-friendly.version>
    <groovy-eclipse-batch.version>3.0.10-02</groovy-eclipse-batch.version>
    <groovy-eclipse-compiler.version>3.7.0</groovy-eclipse-compiler.version>
    <license-maven-plugin.version>4.5</license-maven-plugin.version>
    <maven-assembly-plugin.version>3.7.1</maven-assembly-plugin.version>
    <maven-compiler-plugin.version>3.13.0</maven-compiler-plugin.version>
    <maven-release-plugin.version>3.1.1</maven-release-plugin.version>
    <maven-remote-resources-plugin.version>3.2.0</maven-remote-resources-plugin.version>
    <maven-surefire-plugin.version>3.3.1</maven-surefire-plugin.version>

    <!-- SONAR -->
    <sonar.projectKey>${project.groupId}:${project.artifactId}${env.APPEND_SONAR_PROJECT_KEY}</sonar.projectKey>
    <sonar.projectName>${project.name}${env.APPEND_SONAR_PROJECT_NAME}</sonar.projectName>
  </properties>

  <modules>
    <module>report.books.requirements</module>
    <module>report.books.testcases</module>
    <module>report.books.distribution</module>
  </modules>

  <scm>
    <connection>scm:git:${project.basedir}</connection>
    <developerConnection>scm:git:${project.basedir}</developerConnection>
    <tag>HEAD</tag>
  </scm>


  <distributionManagement>
    <repository>
      <id>squash-release-deploy-repo</id>
      <name>Squash releases deployment repo</name>
      <url>${deploy-repo.release.url}</url>
    </repository>
    <snapshotRepository>
      <id>squash-snapshot-deploy-repo</id>
      <name>Squash snapshot deployment repo</name>
      <url>${deploy-repo.snapshot.url}</url>
    </snapshotRepository>
  </distributionManagement>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <artifactId>maven-remote-resources-plugin</artifactId>
          <version>${maven-remote-resources-plugin.version}</version>
        </plugin>
        <plugin>
          <artifactId>maven-surefire-plugin</artifactId>
          <version>${maven-surefire-plugin.version}</version>
        </plugin>
        <plugin>
          <artifactId>maven-release-plugin</artifactId>
          <version>${maven-release-plugin.version}</version>
        </plugin>

        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-compiler-plugin</artifactId>
          <version>${maven-compiler-plugin.version}</version>
          <configuration>
            <compilerId>groovy-eclipse-compiler</compilerId>
            <source>${java.version}</source>
            <target>${java.version}</target>
          </configuration>
          <dependencies>
            <dependency>
              <groupId>org.codehaus.groovy</groupId>
              <artifactId>groovy-eclipse-compiler</artifactId>
              <version>${groovy-eclipse-compiler.version}</version>
            </dependency>
            <dependency>
              <groupId>org.codehaus.groovy</groupId>
              <artifactId>groovy-eclipse-batch</artifactId>
              <version>${groovy-eclipse-batch.version}</version>
            </dependency>
          </dependencies>
        </plugin>
      </plugins>
    </pluginManagement>

    <plugins>
      <plugin>
        <!-- Checks license headers throughout the project -->
        <groupId>com.mycila</groupId>
        <artifactId>license-maven-plugin</artifactId>
        <version>${license-maven-plugin.version}</version>
        <inherited>false</inherited>
        <configuration>
          <aggregate>true</aggregate>
          <strictCheck>true</strictCheck>
          <properties>
            <license.copyrightOwner>${project.organization.name}</license.copyrightOwner>
            <license.yearSpan>${project.inceptionYear}</license.yearSpan>
          </properties>
          <licenseSets>
            <licenseSet>
              <header>src/material/header.txt</header>
              <excludes>
                <exclude>template.mf</exclude>
                <!-- license files -->
                <exclude>**/NOTICE.*</exclude>
                <exclude>**/README.*</exclude>
                <exclude>**/NOTICE</exclude>
                <exclude>**/README</exclude>
                <exclude>**/README*.*</exclude>
                <exclude>**/readme*.*</exclude>

                <!-- maven -->
                <exclude>**/src/main/assembly/assembly.xml</exclude>
                <exclude>**/pom.xml</exclude>
                <exclude>**/pom.xml.*</exclude>
                <exclude>build.properties</exclude>
                <!-- ides -->
                <exclude>.settings</exclude>
                <exclude>.idea/**</exclude>
                <exclude>*.iml</exclude>
                <exclude>.classpath</exclude>
                <exclude>.project</exclude>
                <!-- hg -->
                <exclude>.hgignore</exclude>
                <exclude>.hgtags</exclude>
                <exclude>*.orig</exclude>
                <exclude>*.rej</exclude>

                <exclude>.editorconfig</exclude>
                <exclude>template.mf</exclude>
                <exclude>**/material/**</exclude>
                <exclude>META-INF/**</exclude>
                <exclude>**/*.sql</exclude>
                <exclude>**/.ci-friendly-pom.xml</exclude>
                <exclude>.m2/**</exclude>
              </excludes>
            </licenseSet>
          </licenseSets>
          <mapping>
            <tag>DYNASCRIPT_STYLE</tag>
            <jrxml>XML_STYLE</jrxml>
          </mapping>
        </configuration>
        <executions>
          <execution>
            <goals>
              <goal>check</goal>
            </goals>
            <phase>validate</phase>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>com.outbrain.swinfra</groupId>
        <artifactId>ci-friendly-flatten-maven-plugin</artifactId>
        <version>${ci-friendly.version}</version>
        <executions>
          <execution>
            <goals>
              <!-- Ensure proper cleanup. Will run on clean phase-->
              <goal>clean</goal>
              <!-- Enable ci-friendly version resolution. Will run on process-resources phase-->
              <goal>flatten</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>org.squashtest.tm</groupId>
        <artifactId>squash-tm-bom</artifactId>
        <version>${squash.core.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <dependencies>

    <dependency>
      <groupId>org.squashtest.tm</groupId>
      <artifactId>core.api</artifactId>
      <version>${squash.core.version}</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.squashtest.tm</groupId>
      <artifactId>tm.domain</artifactId>
      <version>${squash.core.version}</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.squashtest.tm</groupId>
      <artifactId>core.report.api</artifactId>
      <version>${squash.core.version}</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>javax.servlet</groupId>
      <artifactId>javax.servlet-api</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.squashtest.tm</groupId>
      <artifactId>database.h2.fragment</artifactId>
      <version>${squash.core.version}</version>
      <scope>test</scope>
    </dependency>
  </dependencies>

  <repositories>
    <repository>
      <id>squash</id>
      <url>http://repo.squashtest.org/maven2/releases</url>
    </repository>
    <repository>
      <id>squash-snapshots</id>
      <url>http://repo.squashtest.org/maven2/snapshots</url>
    </repository>
  </repositories>

  <profiles>
    <profile>
      <id>minimal</id>
      <properties>
        <skipITs>true</skipITs>
        <skipTests>true</skipTests>
        <deploy-repo.release.url />
        <deploy-repo.snapshot.url />
      </properties>
    </profile>
  </profiles>
</project>
