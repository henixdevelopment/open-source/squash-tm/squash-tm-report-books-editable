/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.testcases.query

import spock.lang.Specification

class TestCaseHierarchyHelperTest extends Specification {
	def "should compute paragraph hierarchy from depth levels"() {
		given:
		def hierarchyHelper = new TestCaseHierarchyHelper()
		def result = []

		when:
		for (Integer depthLevel in depthLevels) {
			result.add(hierarchyHelper.getNextParagraphLevel(depthLevel))
		}

		then:
		result == expectedResult

		where:
		depthLevels 				| expectedResult
		[0, 0, 0]					| [[1], [2], [3]]
		[0, 0, 1, 2, 2, 1, 1, 0]	| [[1], [2], [2,1], [2,1,1], [2,1,2], [2,2], [2,3], [3]]
		[0, 1, 2, 3, 0]				| [[1], [1, 1], [1,1,1], [1,1,1,1], [2]]
		[0, 4, 2, 0]				| [[1], [1,1,1,1,1], [1,1,2], [2]]
	}
}
