select TCLN.PROJECT_ID as PROJECT_ID, MAX(P.NAME) as PROJECT_NAME, TCLN.TCLN_ID as ITEM_ID
from TEST_CASE_LIBRARY_NODE TCLN
 join TCLN_RELATIONSHIP_CLOSURE CLOS on CLOS.ANCESTOR_ID = TCLN.TCLN_ID
 left join TEST_CASE_LIBRARY_CONTENT TCLC_PARENT on CLOS.ANCESTOR_ID = TCLC_PARENT.CONTENT_ID
 left join TCLN_RELATIONSHIP TCLN_REL_PARENT on CLOS.ANCESTOR_ID = TCLN_REL_PARENT.DESCENDANT_ID
 join PROJECT P on P.PROJECT_ID=TCLN.PROJECT_ID
 join (
		-- the sorting key for each node is <reference><name>
		select tclnrew.TCLN_ID, CONCAT(COALESCE(tcrew.REFERENCE , ''), tclnrew.NAME ) as SORTING_NAME
		from TEST_CASE_LIBRARY_NODE tclnrew left outer join  TEST_CASE tcrew on tclnrew.TCLN_ID = tcrew.TCLN_ID
	) SORT on SORT.TCLN_ID = TCLN.TCLN_ID
where TCLN.TCLN_ID  in (:nodeIds)
  and TCLN.PROJECT_ID in (:projectIds)
group by P.PROJECT_ID, TCLN.TCLN_ID
order by case when (:testCaseSortOrder) = 'POSITIONAL' then concat(LPAD(cast(P.TCL_ID as char(5)), 5, '0'), '-', group_concat(coalesce(LPAD(cast(TCLN_REL_PARENT.CONTENT_ORDER as char(5)), 5, '0'), LPAD(cast(TCLC_PARENT.CONTENT_ORDER as char(5)), 5, '0')) order by CLOS.DEPTH desc)) end,
         case when (:testCaseSortOrder) = 'ALPHABETICAL' then MAX(P.NAME) end,
         case when (:testCaseSortOrder) = 'ALPHABETICAL' then MAX(SORT.SORTING_NAME) end
