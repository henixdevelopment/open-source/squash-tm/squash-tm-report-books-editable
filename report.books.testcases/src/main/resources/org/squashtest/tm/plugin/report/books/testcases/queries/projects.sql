select TCLN.PROJECT_ID as PROJECT_ID,
	  P.NAME as PROJECT_NAME,
	  TCLN.TCLN_ID as ITEM_ID,
	SORT.SORTING_NAME as SORTING_CHAIN
from TEST_CASE_LIBRARY_NODE TCLN
join TEST_CASE_LIBRARY_CONTENT TCLC on TCLC.CONTENT_ID = TCLN.TCLN_ID
join PROJECT P on P.PROJECT_ID=TCLN.PROJECT_ID
join (
	-- the sorting key for each node is <reference><name>
	select tclnrew.TCLN_ID, CONCAT(COALESCE(tcrew.REFERENCE , '') , tclnrew.NAME ) as SORTING_NAME
	from TEST_CASE_LIBRARY_NODE tclnrew left outer join  TEST_CASE tcrew on tclnrew.TCLN_ID = tcrew.TCLN_ID

) SORT on SORT.TCLN_ID = TCLC.CONTENT_ID
where P.PROJECT_ID  in (:projectIds)
and P.PROJECT_TYPE = 'P'
order by case when (:testCaseSortOrder) = 'POSITIONAL' then TCLC.LIBRARY_ID end,
         case when (:testCaseSortOrder) = 'POSITIONAL' then TCLC.CONTENT_ORDER end,
         case when (:testCaseSortOrder) = 'ALPHABETICAL' then P.NAME end,
         case when (:testCaseSortOrder) = 'ALPHABETICAL' then SORT.SORTING_NAME end
