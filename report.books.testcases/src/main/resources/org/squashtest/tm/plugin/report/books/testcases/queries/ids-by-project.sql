select distinct TC.TCLN_ID
from TEST_CASE TC
inner join TEST_CASE_LIBRARY_NODE TCLN on TC.TCLN_ID = TCLN.TCLN_ID
where TCLN.PROJECT_ID in (:projectIds)
