SELECT *
FROM (
         SELECT kts.TEST_STEP_ID   AS STEP_ID,
                'TEXT'             AS TYPE,
                awt.TEXT           AS VALUE,
                awf.FRAGMENT_ORDER AS FRAGMENT_ORDER
         FROM TEST_CASE_STEPS tcs
                  INNER JOIN KEYWORD_TEST_STEP kts ON kts.test_step_id = tcs.step_id
                  INNER JOIN ACTION_WORD_FRAGMENT awf ON awf.action_word_id = kts.action_word_id
                  INNER JOIN ACTION_WORD_TEXT awt ON awt.action_word_fragment_id = awf.action_word_fragment_id
         WHERE tcs.test_case_id in (:testcaseIds)
         UNION
         SELECT kts.TEST_STEP_ID   AS STEP_ID,
                'PARAMETER'        AS TYPE,
                awpv.VALUE         AS VALUE,
                awf.FRAGMENT_ORDER AS FRAGMENT_ORDER
         FROM TEST_CASE_STEPS tcs
                  INNER JOIN KEYWORD_TEST_STEP kts ON kts.test_step_id = tcs.step_id
                  INNER JOIN ACTION_WORD_FRAGMENT awf ON awf.action_word_id = kts.action_word_id
                  INNER JOIN ACTION_WORD_PARAMETER_VALUE awpv
                             ON awpv.action_word_fragment_id = awf.action_word_fragment_id AND awpv.keyword_test_step_id = kts.test_step_id
         WHERE tcs.test_case_id in (:testcaseIds)
     ) as result
ORDER BY result.STEP_ID, result.FRAGMENT_ORDER;
