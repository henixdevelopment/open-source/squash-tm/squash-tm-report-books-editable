select MAX(TCLN.PROJECT_ID) as PROJECT_ID,
	   MAX(P.NAME) as PROJECT_NAME,
	   TCLN.TCLN_ID as ITEM_ID,
	   MAX(SORT.SORTING_NAME) as SORTING_CHAIN
from TEST_CASE_LIBRARY_NODE TCLN
		 join TEST_CASE_LIBRARY_CONTENT TCLC on TCLC.CONTENT_ID = TCLN.TCLN_ID
		 join PROJECT P on P.PROJECT_ID=TCLN.PROJECT_ID
		 join CUSTOM_FIELD_BINDING CFB on P.PROJECT_ID = CFB.BOUND_PROJECT_ID
		 join CUSTOM_FIELD_VALUE CFV on CFB.CFB_ID=CFV.CFB_ID
		 join CUSTOM_FIELD_VALUE_OPTION CFVO on CFVO.CFV_ID = CFV.CFV_ID

		 join (
	-- the sorting key for each node is <isfolder><reference><name> ensuring that folders
	-- are ranked after the test cases
	select TCLNREW.TCLN_ID, CONCAT(COALESCE(TCREW.REFERENCE , ' ') , TCLNREW.NAME ) as SORTING_NAME
	from TEST_CASE_LIBRARY_NODE TCLNREW left outer join  TEST_CASE TCREW on TCLNREW.TCLN_ID = TCREW.TCLN_ID

) SORT on SORT.TCLN_ID = TCLC.CONTENT_ID
where P.PROJECT_TYPE = 'P'
  and CFB.BOUND_ENTITY= 'TEST_CASE'
  and CFV.FIELD_TYPE = 'TAG'
  and CFVO.LABEL in (:tags)
  and P.PROJECT_ID in (:projectIds)
group by TCLN.TCLN_ID
order by case when (:testCaseSortOrder) = 'POSITIONAL' then MAX(TCLC.LIBRARY_ID) end,
         case when (:testCaseSortOrder) = 'POSITIONAL' then MAX(TCLC.CONTENT_ORDER) end,
         case when (:testCaseSortOrder) = 'ALPHABETICAL' then MAX(P.NAME) end,
         case when (:testCaseSortOrder) = 'ALPHABETICAL' then MAX(SORT.SORTING_NAME) end
