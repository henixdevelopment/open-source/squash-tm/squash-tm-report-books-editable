
-- comments
--
-- 1/ chain_id is now a placeholder for a data that isn't used anymore,
-- 2/ same goes for node_type
-- 3/ we test that a given node is a folder or a test case by testing if it's tc_type is null (that cannot happen for regular test cases)
-- 4/ use MAX(value) to bluff postgresql when he complains about "value must be in group by or blabla"
-- 5/ count attachment in a sub query to avoid join with attachment table as it messes with chain and sorting_key values
-- 6/ Same as above: group concat on milestone labels in sub query to avoid join with MILESTONE and MILESTONE_TEST_CASE tables
-- because it messes with chain and sorting_key values

select
    group_concat(concat(COALESCE((select distinct TC_CLOS.REFERENCE), ''), TCLN_CLOS.NAME, '=Sep=') order by CLOS.DEPTH desc) as CHAIN,
    (select distinct TCLN.TCLN_ID) as ID,
    MAX((case when TC.TC_TYPE is NULL then 1 else 0 end)) as FOLDER,
    TCLN.NAME as NAME,
    max(CLOS.DEPTH) as LEVEL,
    MAX(COALESCE(TC.IMPORTANCE, 'UNDEFINED')) as IMPORTANCE,
    MAX(COALESCE(TC_NATURE.LABEL, 'UNDEFINED')) as TCNATURE,
    MAX(COALESCE(TC_TYPE.LABEL, 'UNDEFINED')) as TCTYPE,
    MAX(COALESCE(TC_NATURE.ITEM_TYPE, 'UNDEFINED')) as TCNATURETYPE,
    MAX(COALESCE(TC_TYPE.ITEM_TYPE, 'UNDEFINED')) as TCTYPETYPE,
    MAX(COALESCE(TC.TC_STATUS, 'WORK_IN_PROGRESS')) as TCSTATUS,
    MAX((case when TC.TA_TEST is NULL then 0 else 1 end)) as EXECUTION_MODE,
    MAX(COALESCE(TC.PREREQUISITE, ' ')) as PREREQUISITES,
    MAX(COALESCE(TC.REFERENCE, ' ')) as REFERENCE,
    TCLN.CREATED_ON as CREATED_ON,
    TCLN.CREATED_BY as CREATED_BY,
    TCLN.LAST_MODIFIED_ON as LAST_MODIFIED_ON,
    TCLN.LAST_MODIFIED_BY as LAST_MODIFIED_BY,
    TCLN.DESCRIPTION as DESCRIPTION,
    (select count(distinct ATTACH.ATTACHMENT_ID) from ATTACHMENT ATTACH where ATTACH.ATTACHMENT_LIST_ID = TCLN.ATTACHMENT_LIST_ID) as ATTACHMENTS,
    group_concat(concat(COALESCE(TC_CLOS.REFERENCE, ''), TCLN_CLOS.NAME, '=Sep=') order by CLOS.DEPTH desc) as SORTING_KEY,
    case when MAX(TC.TC_TYPE) is NULL then '' else
        (
            select group_concat(distinct MSTONES.LABEL)
            from MILESTONE MSTONES
                     left join MILESTONE_TEST_CASE TCSTONES on MSTONES.MILESTONE_ID = TCSTONES.MILESTONE_ID
                     left join TEST_CASE TC on TCSTONES.TEST_CASE_ID = TC.TCLN_ID
            where MSTONES.MILESTONE_ID = TCSTONES.MILESTONE_ID and TC.TCLN_ID in (:testcaseIds)
        ) end as MILESTONES,
    group_concat(concat((select distinct TCLN_CLOS.TCLN_ID),'=Sep=') order by CLOS.DEPTH desc) as NODEID,
    STC.SCRIPT as SCRIPT,
    MAX(case when STC.TCLN_ID is not NULL then 'SCRIPTED'
             when KWTC.TCLN_ID is not NULL then 'KEYWORD'
             else 'STANDARD'
        end) as KIND,
    MAX((case when P.ALLOW_AUTOMATION_WORKFLOW is false then 0 else 1 end)) as ALLOW_AUTOMATION_WORKFLOW,
    TC.AUTOMATABLE as AUTOMATABLE,
    AR.REQUEST_STATUS as REQUEST_STATUS,
    AR.AUTOMATION_PRIORITY as AUTOMATION_PRIORITY,
    case
        when (:testCaseSortOrder) = 'POSITIONAL'
            then concat(
            LPAD(cast(P.TCL_ID as char(5)), 5, '0'),
            '-',
            group_concat(
                coalesce(LPAD(cast(TCLN_REL_PARENT.CONTENT_ORDER as char(5)), 5, '0'), LPAD(cast(TCLC_PARENT.CONTENT_ORDER as char(5)), 5, '0')) order by CLOS.DEPTH desc
            ))
        else REPLACE(group_concat(concat(COALESCE(TC_CLOS.REFERENCE, ''), TCLN_CLOS.NAME) order by CLOS.DEPTH desc), ' ', '')
    end as ORDER_KEY
from TCLN_RELATIONSHIP_CLOSURE CLOS
         inner join TEST_CASE_LIBRARY_NODE TCLN_CLOS on CLOS.ANCESTOR_ID=TCLN_CLOS.TCLN_ID
         left join TEST_CASE TC_CLOS on TCLN_CLOS.TCLN_ID = TC_CLOS.TCLN_ID
         inner join TEST_CASE_LIBRARY_NODE TCLN on CLOS.DESCENDANT_ID=TCLN.TCLN_ID
         left join TEST_CASE_LIBRARY_CONTENT TCLC_PARENT on CLOS.ANCESTOR_ID = TCLC_PARENT.CONTENT_ID
         left join TEST_CASE TC on TCLN.TCLN_ID = TC.TCLN_ID
         left join INFO_LIST_ITEM TC_NATURE on TC.TC_NATURE = TC_NATURE.ITEM_ID
         left join INFO_LIST_ITEM TC_TYPE on TC.TC_TYPE = TC_TYPE.ITEM_ID
         left join SCRIPTED_TEST_CASE STC on TC.TCLN_ID = STC.TCLN_ID
         left join KEYWORD_TEST_CASE KWTC on TC.TCLN_ID = KWTC.TCLN_ID
         left join PROJECT P on TCLN.PROJECT_ID = P.PROJECT_ID
         left join AUTOMATION_REQUEST AR on TC.AUTOMATION_REQUEST_ID = AR.AUTOMATION_REQUEST_ID
         left join TCLN_RELATIONSHIP TCLN_REL_PARENT on CLOS.ANCESTOR_ID = TCLN_REL_PARENT.DESCENDANT_ID
where CLOS.DESCENDANT_ID in (select CLOS1.ANCESTOR_ID from TCLN_RELATIONSHIP_CLOSURE CLOS1 where CLOS1.DESCENDANT_ID in (:testcaseIds))
   or CLOS.DESCENDANT_ID in (select CLOS2.DESCENDANT_ID from TCLN_RELATIONSHIP_CLOSURE CLOS2 where CLOS2.ANCESTOR_ID in (:testcaseIds))
group by CLOS.DESCENDANT_ID, TCLN.TCLN_ID, STC.SCRIPT, TC.AUTOMATABLE, AR.REQUEST_STATUS, AR.AUTOMATION_PRIORITY, TC.TCLN_ID, P.PROJECT_ID
order by ORDER_KEY
