SELECT
d.DATASET_ID as DATASET_ID,
p.NAME as NAME,
dpv.PARAM_VALUE as VALUE
FROM DATASET d
INNER JOIN DATASET_PARAM_VALUE dpv ON d.DATASET_ID = dpv.DATASET_ID
INNER JOIN PARAMETER p ON p.PARAM_ID = dpv.PARAM_ID
WHERE d.TEST_CASE_ID IN (:testcaseIds)
union
SELECT
d.DATASET_ID as DATASET_ID,
p.NAME as NAME,
dpv.PARAM_VALUE as VALUE
FROM DATASET d
INNER JOIN DATASET_PARAM_VALUE dpv ON d.DATASET_ID = dpv.DATASET_ID
INNER JOIN PARAMETER p ON p.PARAM_ID = dpv.PARAM_ID
LEFT JOIN CALL_TEST_STEP cts on p.TEST_CASE_ID = cts.CALLED_TEST_CASE_ID
WHERE d.TEST_CASE_ID IN (:testcaseIds) AND cts.DELEGATE_PARAMETER_VALUES is true;
