SELECT *
FROM (SELECT tcs.STEP_ID                                                AS ID,
			 ats.ACTION                                                 AS ACTION,
			 ats.EXPECTED_RESULT                                        AS EXPECTED_RESULT,
			 'S'                                                        AS TYPE,
			 tcs.STEP_ORDER                                             AS STEP_ORDER,
			 null                                                       AS STEP_DATASET,
			 (SELECT count(*)
			  FROM ATTACHMENT attach
			  WHERE ats.ATTACHMENT_LIST_ID = attach.ATTACHMENT_LIST_ID) AS attach,
			 (SELECT count(*)
			  FROM VERIFYING_STEPS verif
			  WHERE tcs.STEP_ID = verif.TEST_STEP_ID)                   AS verif_step,
			 tcs.TEST_CASE_ID                                           AS TEST_CASE_ID,
			 null                                                       AS KEYWORD,
			 null                                                       AS DATATABLE,
			 null                                                       AS DOCSTRING,
			 null                                                       AS COMMENT,
			 null                                                       AS BDD_SCRIPT_LANGUAGE,
			 'false'                                                    AS DELEGATE_PARAMETER_VALUES
	  FROM TEST_CASE_STEPS tcs
			   JOIN ACTION_TEST_STEP ats ON tcs.STEP_ID = ats.TEST_STEP_ID
	  WHERE tcs.TEST_CASE_ID in (:testcaseIds)
	  UNION
	  SELECT cts.CALLED_TEST_CASE_ID                                                      AS ID,
			 tcln.NAME                                                                    AS ACTION,
			 ' '                                                                          AS EXPECTED_RESULT,
			 'C'                                                                          AS TYPE,
			 tcs.STEP_ORDER                                                               AS STEP_ORDER,
			 dataset.NAME                                                                 as STEP_DATASET,
			 0                                                                            AS attach,
			 0                                                                            AS verif_step,
			 tcs.TEST_CASE_ID                                                             as TEST_CASE_ID,
			 null                                                                         AS KEYWORD,
			 null                                                                         AS DATATABLE,
			 null                                                                         AS DOCSTRING,
			 null                                                                         AS COMMENT,
			 null                                                                         AS BDD_SCRIPT_LANGUAGE,
			 case when cts.DELEGATE_PARAMETER_VALUES is true then 'true' else 'false' end AS DELEGATE_PARAMETER_VALUES
	  FROM TEST_CASE_STEPS tcs
			   JOIN CALL_TEST_STEP cts ON cts.TEST_STEP_ID = tcs.STEP_ID
			   JOIN TEST_CASE_LIBRARY_NODE tcln ON tcln.TCLN_ID = cts.CALLED_TEST_CASE_ID
			   LEFT JOIN DATASET dataset on cts.CALLED_DATASET = dataset.DATASET_ID
	  WHERE tcs.TEST_CASE_ID in (:testcaseIds)
	  UNION
	  SELECT tcs.STEP_ID           AS ID,
			 ''                    AS ACTION,
			 ''                    AS EXPECTED_RESULT,
			 'K'                   as TYPE,
			 tcs.STEP_ORDER        AS STEP_ORDER,
			 null                  AS STEP_DATASET,
			 0                     AS attach,
			 0                     as verif_step,
			 tcs.TEST_CASE_ID      AS TEST_CASE_ID,
			 kts.KEYWORD           AS KEYWORD,
			 kts.DATATABLE         AS DATATABLE,
			 kts.DOCSTRING         AS DOCSTRING,
			 kts.COMMENT           AS COMMENT,
			 p.BDD_SCRIPT_LANGUAGE AS BDD_SCRIPT_LANGUAGE,
			 'false'               AS DELEGATE_PARAMETER_VALUES
	  FROM TEST_CASE_STEPS tcs
			   JOIN KEYWORD_TEST_STEP kts ON kts.TEST_STEP_ID = tcs.STEP_ID
			   JOIN TEST_CASE_LIBRARY_NODE tcln ON tcln.TCLN_ID = tcs.TEST_CASE_ID
			   JOIN PROJECT p on tcln.PROJECT_ID = p.PROJECT_ID
	  WHERE tcs.TEST_CASE_ID in (:testcaseIds)) sub
ORDER BY sub.STEP_ORDER
