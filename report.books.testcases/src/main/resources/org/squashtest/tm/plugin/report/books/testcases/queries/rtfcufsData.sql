SELECT cfv.LARGE_VALUE as largeValue,
cf.label as label,
cf.INPUT_TYPE as inputType,
cfv.BOUND_ENTITY_ID
FROM CUSTOM_FIELD_VALUE cfv
JOIN CUSTOM_FIELD_BINDING cfb on cfv.CFB_ID = cfb.CFB_ID
JOIN CUSTOM_FIELD cf on cf.CF_ID = cfb.CF_ID
WHERE cfv.BOUND_ENTITY_ID in (:entityIds)
AND cfb.BOUND_ENTITY = :entityType
AND cfv.FIELD_TYPE ='RTF'
order by cfb.POSITION
