/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.testcases.beans;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Data {

	public static final String KEY_PREFIX = "report.books.testcases.";

	private List<Project> projects;

	private static final String LABEL_DESCRIPTION = KEY_PREFIX + "label.Description";
	private static final String LABEL_ATTACHMENT = KEY_PREFIX + "label.attachment";
	private static final String LABEL_AUTOMATED = KEY_PREFIX + "label.automated";
	private static final String LABEL_CREATED = KEY_PREFIX + "label.createdOn";
	private static final String LABEL_ID = KEY_PREFIX + "label.id";
	private static final String LABEL_IMPORTANCE = KEY_PREFIX + "label.importance";
	private static final String LABEL_LAST_MODIFIED = KEY_PREFIX + "label.lastModifiedOn";
	private static final String LABEL_NATURE = KEY_PREFIX + "label.nature";
	private static final String LABEL_PAGE = KEY_PREFIX + "label.page";
	private static final String LABEL_PROJET = KEY_PREFIX + "label.projet";
	private static final String LABEL_STATUS = KEY_PREFIX + "label.status";
	private static final String LABEL_TESTCASES_REPORT = KEY_PREFIX + "label.testcasesreport";
	private static final String LABEL_TYPE = KEY_PREFIX + "label.type";
	private static final String LABEL_VERSION = KEY_PREFIX + "label.version";
	private static final String LABEL_REFERENCE = KEY_PREFIX + "label.reference";
	private static final String LABEL_NAME = KEY_PREFIX + "label.name";
	private static final String LINKED_REQUIREMENTS_NAME = KEY_PREFIX + "linked.requirements.name";
	private static final String LINKED_REQUIREMENTS_PROJECT_NAME = KEY_PREFIX + "linked.requirements.project.name";
	private static final String LINKED_REQUIREMENTS_TITLE = KEY_PREFIX + "linked.requirements.title";
	private static final String LABEL_CRITICALITY = KEY_PREFIX + "label.criticality";
	private static final String SUMMARY = KEY_PREFIX + "summary";
	private static final String SUMMARY_MESSAGE = KEY_PREFIX + "summary.message";
	private static final String TESTCASE_CREATED_BY = KEY_PREFIX + "createdBy";
	private static final String TESTCASE_CREATED_ON = KEY_PREFIX + "createdOn";
	private static final String TESTCASE_EXECUTION_MODE = KEY_PREFIX + "executionMode";
	private static final String TESTCASE_IMPORTANCE = KEY_PREFIX + "importance";
	private static final String TESTCASE_MODIFIED_BY = KEY_PREFIX + "modifiedBy";
	private static final String TESTCASE_MODIFIED_ON = KEY_PREFIX + "modifiedOn";
	private static final String TESTCASE_STEPS_ACTION = KEY_PREFIX + "steps.action";
	private static final String TESTCASE_STEPS_CALL = KEY_PREFIX + "steps.call";
	private static final String TESTCASE_STEPS_CALL_DATASET = KEY_PREFIX + "steps.call.dataset";
	private static final String TESTCASE_STEPS_EXPECTED_RESULT = KEY_PREFIX + "steps.expectedResult";
	private static final String TESTCASE_STEP_TITLE = KEY_PREFIX + "step.title";
	private static final String TESTCASE_STEPS_TITLE = KEY_PREFIX + "steps.title";
	private static final String TESTCASES_REPORT_LABEL = KEY_PREFIX + "report.label";
	private static final String TESTCASES_TITLE = KEY_PREFIX + "title";
	private static final String TESTCASES_TITLE_DATE_FORMAT = KEY_PREFIX + "title.dateformat";
	private static final String TESTSTEPS_TITLE = KEY_PREFIX + "teststeps.title";
	private static final String TESTCASES_INDEX = KEY_PREFIX + "index";
	private static final String GENERATED = KEY_PREFIX + "generated";
	private static final String PREREQUISITE = KEY_PREFIX + "prerequisite";
	private static final String PREREQUISITE_TITLE = KEY_PREFIX + "prerequisiteTitle";
	private static final String GENERATED_TITLE = KEY_PREFIX + "generated.title";
	private static final String GENERATED_DATE = KEY_PREFIX + "book.dateformat";
	private static final String GENERATED_HOUR = KEY_PREFIX + "book.hourformat";
	private static final String LABEL_MILESTONES = KEY_PREFIX + "milestones";
	private static final String TITLE_MILESTONE = KEY_PREFIX + "title.milestone";
	private static final String SCRIPT_TITLE = KEY_PREFIX + "script.title";
	private static final String PARAMETER_TITLE = KEY_PREFIX + "parameter.title";
	private static final String TABLE_NAME = KEY_PREFIX + "table.name";
	private static final String TABLE_DESCRIPTION = KEY_PREFIX + "table.description";
	private static final String TABLE_TEST_CASE_SOURCE = KEY_PREFIX + "table.testcase.source";
	private static final String TABLE_VALUE = KEY_PREFIX + "table.value";
	private static final String DATASET_TITLE = KEY_PREFIX + "dataset.title";
	private static final String LABEL_AUTOMATABLE = KEY_PREFIX + "label.automatable";
	private static final String LABEL_AUTOMATION_PRIORITY = KEY_PREFIX + "label.automationPriority";
	private static final String LABEL_REQUEST_STATUS = KEY_PREFIX + "label.requestStatus";

	private final String milestoneName;
	private final String testCaseSortOrder;
	private final String templateFileName;

	public Data() {
		this(null, null, null);
	}

	public Data(String milestoneLabel, String testCaseSortOrder, String templateFileName) {
		super();
		this.milestoneName = milestoneLabel;
		this.testCaseSortOrder = testCaseSortOrder;
		this.templateFileName = templateFileName;
	}

	private static String translate(String string) {
		return I18nHelper.translate(string);
	}

	private String translate(String string, Object[] params) {
		return I18nHelper.translate(string, params);
	}
	public String getTestcasesIndex() {
		return translate(TESTCASES_INDEX);
	}

	public String getPrerequisite() {
		return translate(PREREQUISITE);
	}

	public String getPrerequisiteTitle() {
		return translate(PREREQUISITE_TITLE);
	}

	public String getGeneratedDate() {
		SimpleDateFormat sdfDate = new SimpleDateFormat(translate(GENERATED_DATE));
		Calendar cal = Calendar.getInstance();
		return sdfDate.format(cal.getTime());
	}

	public String getGeneratedHour() {
		SimpleDateFormat sdfHour = new SimpleDateFormat(translate(GENERATED_HOUR));
		Calendar cal = Calendar.getInstance();
		return sdfHour.format(cal.getTime());
	}

	public String getGeneratedTitle() {

		Object[] params = new Object[2];
		params[0] = getGeneratedDate();
		params[1] = getGeneratedHour();
		return translate(GENERATED_TITLE, params);
	}

	public String getGenerated() {
		return translate(GENERATED);
	}

	public String getLabelDescription() {

		return translate(LABEL_DESCRIPTION);
	}

	public String getLabelAttachment() {
		return translate(LABEL_ATTACHMENT);
	}

	public String getLabelAutomated() {
		return translate(LABEL_AUTOMATED);
	}

	public String getLabelCreated() { return translate(LABEL_CREATED);	}
	public String getLabelId() {
		return translate(LABEL_ID);
	}

	public String getLabelImportance() {
		return translate(LABEL_IMPORTANCE);
	}

	public String getLabelLastModified() {
		return translate(LABEL_LAST_MODIFIED);
	}

	public String getLabelNature() {
		return translate(LABEL_NATURE);
	}

	public String getLabelPage() {
		return translate(LABEL_PAGE);
	}

	public String getLabelProjet() {
		return translate(LABEL_PROJET);
	}

	public String getLabelStatus() {
		return translate(LABEL_STATUS);
	}

	public String getLabelTestcasesreport() {
		return translate(LABEL_TESTCASES_REPORT);
	}

	public String getLabelType() {
		return translate(LABEL_TYPE);
	}

	public String getLabelVersion() {
		return translate(LABEL_VERSION);
	}

	public String getLabelReference() {
		return translate(LABEL_REFERENCE);
	}

	public String getLabelName() {
		return translate(LABEL_NAME);
	}

	public String getLinkedRequirementsName() {
		return translate(LINKED_REQUIREMENTS_NAME);
	}

	public String getLinkedRequirementsProjectName() {
		return translate(LINKED_REQUIREMENTS_PROJECT_NAME);
	}

	public String getLinkedRequirementsTitle() {
		return translate(LINKED_REQUIREMENTS_TITLE);
	}

	public String getLabelCriticality() {
		return translate(LABEL_CRITICALITY);
	}

	public String getSummary() {
		return translate(SUMMARY);
	}

	public String getSummaryMessage() {
		return translate(SUMMARY_MESSAGE);
	}

	public String getTestcaseCreatedBy() {
		return translate(TESTCASE_CREATED_BY);
	}

	public String getTestcaseCreatedOn() {
		return translate(TESTCASE_CREATED_ON);
	}

	public String getTestcaseExecutionMode() {
		return translate(TESTCASE_EXECUTION_MODE);
	}

	public String getTestcaseImportance() {
		return translate(TESTCASE_IMPORTANCE);
	}

	public String getTestcaseModifiedBy() {
		return translate(TESTCASE_MODIFIED_BY);
	}

	public String getTestcaseModifiedOn() {
		return translate(TESTCASE_MODIFIED_ON);
	}

	public String getTestcaseStepsAction() {
		return translate(TESTCASE_STEPS_ACTION);
	}

	public String getTestcaseStepsCall() {
		return translate(TESTCASE_STEPS_CALL);
	}

	public String getTestcaseStepsCallDataset() {
		return translate(TESTCASE_STEPS_CALL_DATASET);
	}

	public String getTestcaseStepsExpectedResult() {
		return translate(TESTCASE_STEPS_EXPECTED_RESULT);
	}

	public String getTestcaseStepTitle() {
		return translate(TESTCASE_STEP_TITLE);
	}

	public String getTestcaseStepsTitle() {
		return translate(TESTCASE_STEPS_TITLE);
	}

	public String getTestcasesReportLabel() {
		return translate(TESTCASES_REPORT_LABEL);
	}

	public String getTestcasesTitle() {
		return translate(TESTCASES_TITLE);
	}

	public String getFileTitleTestCasesReport() {
		String title = I18nHelper.translate(TESTCASES_TITLE);
		SimpleDateFormat formatter = new SimpleDateFormat(I18nHelper.translate(TESTCASES_TITLE_DATE_FORMAT));
		String strDate = formatter.format(new Date());
		return title + "-" + strDate;
	}

	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	public String getLabelMilestones() {
		return translate(LABEL_MILESTONES);
	}

	public String getTitleMilestone() {
		return (milestoneName != null) ? translate(TITLE_MILESTONE) + " " + milestoneName : "";
	}

	public String getScriptTitle() {
		return translate(SCRIPT_TITLE);
	}

	public String getTeststepsTitle() {
		return translate(TESTSTEPS_TITLE);
	}

	public String getParameterTitle() {
		return translate(PARAMETER_TITLE);
	}

	public String getTableName() {
		return translate(TABLE_NAME);
	}

	public String getTableDescription() {
		return translate(TABLE_DESCRIPTION);
	}

	public String getTableTestCaseSource() {
		return translate(TABLE_TEST_CASE_SOURCE);
	}

	public String getTableValue() {
		return translate(TABLE_VALUE);
	}

	public String getDatasetTitle() {
		return translate(DATASET_TITLE);
	}

	public String getLabelAutomatable() {
		return translate(LABEL_AUTOMATABLE);
	}

	public String getLabelAutomationPriority() {
		return translate(LABEL_AUTOMATION_PRIORITY);
	}

	public String getLabelRequestStatus() {
		return translate(LABEL_REQUEST_STATUS);
	}

	public String getTestCaseSortOrder() {
		return testCaseSortOrder;
	}

	public String getTemplateFileName() {
		return templateFileName;
	}

}
