/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.testcases.beans;


import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TestCase {

	// meta attributes for rendering

	private boolean printChain = true;

	// real attributes

	private Long id;
	private String name;
	private String description;
	private String prerequisites;
	private Long level;
	private String createdBy;
	private String createdOn;
	private String lastModifiedBy;
	private String lastModifiedOn;
	private String paragraph;
	private String reference;
	private String importance;
	private String nature;
	private String type;
	private String natureType;
	private String typeType;
	private String status;
	private Long allowAutomationWorkflow;
	private String automatable;
	private String requestStatus;
	private String automationPriority;
	private Long executionMode;
	private Long folder;
	private String chain;
	// 6928 - Add folder description
	private String chainDescription;
	private Long attachments;
	/**
	 * the sorting chain ensures that within a folder all tests cases will be ranked before all folders, and that each
	 * sort of bean will be sorted using reference+name among themselves. See the awfull SQL to see how it's done.
	 */
	private String sortingChain;
	private String kind;
	private List<TestCaseSteps> tcSteps = new ArrayList<>();
	private List<PrintableCuf> printableCufs;
	private List<LinkedRequirements> linkedRequirements;
	private String milestoneNames;
	private List<Long> nodeIds;
	private String script;
	private List<Parameter> parameters = new ArrayList<>();
	private List<Dataset> datasets = new ArrayList<>();
	private boolean printParameters;

	private boolean printSteps;
	private boolean printLinkedReq;
	private boolean printFolder;
	private boolean printMilestones;

	public boolean acceptAsCuf(Cuf cufBean) {
		return  cufBean.getEntityId().equals(id);
	}

	public boolean acceptAsPrintCuf(PrintableCuf printCufBean) { return  printCufBean.getEntityId().equals(id);	}

	// ********** small predicates that help the template engine *****************

	public boolean isFolder() {
		return (folder == 1L);
	}

	public boolean isUnmodified() {
		return (StringUtils.isBlank(lastModifiedOn));
	}

	public boolean isNoReference() {
		return (StringUtils.isBlank(reference));
	}

	public boolean isPrintFolder() {
		return printFolder;
	}

	public void setPrintFolder(boolean printFolder) {
		this.printFolder = printFolder;
	}

	public boolean isHasNoMilestones() {
		return (StringUtils.isBlank(milestoneNames));
	}

	public boolean isAtLibraryRoot() {
		return (level == 0);
	}

	public boolean isHasNoSteps() { return tcSteps.isEmpty(); }

	/**
	 * Says whether we should not print the chain
	 *
	 * @return
	 */
	public boolean isNoPrintChain() {
		return (level == 0 || (!printChain));
	}

	public boolean isStandard() {
		return "STANDARD".equals(this.kind);
	}

	public boolean isScripted() {
		return "SCRIPTED".equals(this.kind);
	}

	public boolean isKeyword() {
		return "KEYWORD".equals(this.kind);
	}

	public boolean isNoAllowAutomationWorkflow() { return allowAutomationWorkflow == 0L; }

	public boolean isNoEligible() { return ! "Y".equals(automatable); }

	public boolean isPrintAttachment() {return (attachments > 0L);}

	public boolean isHasTcSteps() {return tcSteps.size() > 0;}
	public boolean isHasParameters() {return parameters.size() > 0;}
	public boolean isHasDataset() {return datasets.size() > 0;}
	public boolean isHasLinkedReq() {
		if (linkedRequirements == null) {
			return false;
		} else {
			return linkedRequirements.size() > 0;
		}
	}

	public boolean isScriptEmpty() {
		return script.isEmpty();
	}



	/* getters and setters with special treatment */

	public String getParagraph() {
		return paragraph;
	}
	public void setParagraph(List<Integer> paragraphLevel) {
		paragraph = "";
		if (paragraphLevel != null && paragraphLevel.size() > 0) {
			Iterator<Integer> itr = paragraphLevel.iterator();
			String delimiter = "";
			while (itr.hasNext()) {
				paragraph = paragraph + delimiter + itr.next();
				delimiter = ".";
			}
		}
	}

	public String getImportance() {
		String msg;
		switch (importance) {
			case "LOW":
				msg = "report.books.testcases.importance.low";
				break;
			case "MEDIUM":
				msg = "report.books.testcases.importance.medium";
				break;
			case "HIGH":
				msg = "report.books.testcases.importance.high";
				break;
			case "VERY_HIGH":
				msg = "report.books.testcases.importance.veryhigh";
				break;
			default:
				msg = "report.books.testcases.importance.undefined";
		}
		return I18nHelper.translate(msg);
	}
	public void setImportance(String importance) {
		this.importance = importance;
	}

	public String getNature() {
		if (natureType.equals("SYS")) {
			return I18nHelper.translate(Data.KEY_PREFIX + nature.toLowerCase());
		} else {
			return nature;
		}
	}
	public void setNature(String nature) {
		this.nature = nature;
	}

	public String getType() {
		if (typeType.equals("SYS")) {
			return I18nHelper.translate(Data.KEY_PREFIX + type.toLowerCase());
		} else {
			return type;
		}
	}
	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		String msg;
		switch (status) {
			case "TO_BE_UPDATED":
				msg = "report.books.testcases.status.tobeupdated";
				break;
			case "UNDER_REVIEW":
				msg = "report.books.testcases.status.underreview";
				break;
			case "APPROVED":
				msg = "report.books.testcases.status.approved";
				break;
			case "OBSOLETE":
				msg = "report.books.testcases.status.obsolete";
				break;
			default:
				msg = "report.books.testcases.status.workinprogress";
		}
		return I18nHelper.translate(msg);
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getAutomatable() {
		String msg;
		switch (automatable) {
			case "Y":
				msg = "report.books.testcases.automatable.Y";
				break;
			case "N":
				msg = "report.books.testcases.automatable.N";
				break;
			default:
				msg = "report.books.testcases.automatable.M";
		}
		return I18nHelper.translate(msg);
	}
	public void setAutomatable(String automatable) {
		this.automatable = automatable;
	}

	public String getRequestStatus() {
		String msg;
		switch (requestStatus) {
			case "TRANSMITTED":
				msg = "report.books.testcases.request-status.transmitted";
				break;
			case "AUTOMATION_IN_PROGRESS":
				msg = "report.books.testcases.request-status.automation-in-progress";
				break;
			case "SUSPENDED":
				msg = "report.books.testcases.request-status.suspended";
				break;
			case "REJECTED":
				msg = "report.books.testcases.request-status.rejected";
				break;
			case "AUTOMATED":
				msg = "report.books.testcases.request-status.automated";
				break;
			case "READY_TO_TRANSMIT":
				msg = "report.books.testcases.request-status.ready-to-transmit";
				break;
			default:
				msg = "report.books.testcases.request-status.work-in-progress";
		}
		return I18nHelper.translate(msg);
	}
	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public String getChain() {
		return chain.replace(" > " + name, "");
	}
	public void setChain(String chain) {
		this.chain = chain;
	}

	public String getAttachments() {
		return String.valueOf(attachments);

	}
	public void setAttachments(Long attachments) {
		this.attachments = attachments;
	}

	/* plain getters & setters */

	public String getNatureType() {
		return natureType;
	}
	public void setNatureType(String natureType) {
		this.natureType = natureType;
	}

	public String getTypeType() {
		return typeType;
	}
	public void setTypeType(String typeType) {
		this.typeType = typeType;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public Long getLevel() {
		return level;
	}
	public void setLevel(Long level) {
		this.level = level;
	}

	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getAutomationPriority() {
		return automationPriority;
	}
	public void setAutomationPriority(String automationPriority) {
		this.automationPriority = automationPriority;
	}

	public Long getAllowAutomationWorkflow() {
		return allowAutomationWorkflow;
	}
	public void setAllowAutomationWorkflow(Long allowAutomationWorkflow) {
		this.allowAutomationWorkflow = allowAutomationWorkflow;
	}

	public void setExecutionMode(Long executionMode) {
		this.executionMode = executionMode;
	}
	public Long getExecutionMode() {
		return executionMode;
	}

	public Long getFolder() {
		return folder;
	}
	public void setFolder(Long folder) {
		this.folder = folder;
	}

	public String getChainDescription() {
		return chainDescription;
	}
	public void setChainDescription(String chainDescription) {
		this.chainDescription = chainDescription;
	}

	public void setPrerequisites(String prerequisites) {
		this.prerequisites = prerequisites;
	}
	public String getPrerequisites() {
		return prerequisites;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public String getCreatedOn() {
		return createdOn;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedOn(String lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}
	public String getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setTcSteps(List<TestCaseSteps> tcStepsBeans) {
		this.tcSteps = tcStepsBeans;
	}
	public List<TestCaseSteps> getTcSteps() {
		return tcSteps;
	}

	public List<LinkedRequirements> getLinkedRequirements() {
		return linkedRequirements;
	}
	public void setLinkedRequirements(List<LinkedRequirements> linkedRequirements) {
		this.linkedRequirements = linkedRequirements;
	}

	public String getSortingChain() {
		return sortingChain;
	}
	public void setSortingChain(String sortingChain) {
		this.sortingChain = sortingChain;
	}

	public String getMilestoneNames() {
		return milestoneNames;
	}
	public void setMilestoneNames(String milestoneLabels) {
		this.milestoneNames = milestoneLabels;
	}

	public boolean isPrintChain() {
		return printChain;
	}
	public void setPrintChain(boolean printChain) {
		this.printChain = printChain;
	}

	public List<Long> getNodeIds() {
		return nodeIds;
	}
	public void setNodeIds(List<Long> nodeIds) {
		this.nodeIds = nodeIds;
	}

	public String getScript() {
		return script;
	}
	public void setScript(String script) {
		this.script = script;
	}

	public String getKind() {
		return kind;
	}
	public void setKind(String kind) {
		this.kind = kind;
	}

	public List<Parameter> getParameters() {
		return parameters;
	}
	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}

	public List<Dataset> getDatasets() {
		return datasets;
	}
	public void setDatasets(List<Dataset> datasets) {
		this.datasets = datasets;
	}

	public boolean isPrintParameters() {
		return printParameters;
	}
	public void setPrintParameters(boolean printParameters) {
		this.printParameters = printParameters;
	}

	public boolean isPrintLinkedReq() {
		return printLinkedReq;
	}
	public void setPrintLinkedReq(boolean printLinkedReq) {
		this.printLinkedReq = printLinkedReq;
	}

	public List<PrintableCuf> getPrintableCufs() {
		return printableCufs;
	}

	public void setPrintableCufs(List<PrintableCuf> printableCufs) {
		this.printableCufs = printableCufs;
	}

	public boolean isPrintSteps() {
		return printSteps;
	}

	public void setPrintSteps(boolean printSteps) {
		this.printSteps = printSteps;
	}

	public boolean isPrintMilestones() { return printMilestones; }

	public void setPrintMilestones(boolean printMilestones) { this.printMilestones = printMilestones; }
}
