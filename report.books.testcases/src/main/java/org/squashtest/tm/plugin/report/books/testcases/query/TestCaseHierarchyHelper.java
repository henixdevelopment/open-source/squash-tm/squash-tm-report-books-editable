/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.testcases.query;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.List;

/**
 * This helper processes a sequence of depth level (with root at 0) and returns, for each level,
 * a new paragraph numbering.
 * Example : given depth values [0, 1, 1, 0], it will produce the numberings : [1, 1.1, 1.2, 3].
 *
 * You should create a new TestCaseHierarchyHelper instance each time you want to reset the output numbering.
 */
public class TestCaseHierarchyHelper {
	private static final int FIRST_INDEX = 1;

	private final DequeBasedStack<Integer> sectionStack = new DequeBasedStack<>();

	public List<Integer> getNextParagraphLevel(int depth) {
		if (sectionStack.isEmpty()) {
			sectionStack.push(FIRST_INDEX);
		} else {
			final int currentDepth = sectionStack.size() - 1;

			if (depth == currentDepth) {
				advanceInCurrentDepth();
			} else if (depth > currentDepth) {
				for (int i = 0; i < depth - currentDepth; ++i) {
					pushLevel();
				}
			} else {
				for (int i = 0; i < currentDepth - depth; ++i) {
					popLevel();
				}
			}
		}

		return sectionStack.getElements();
	}

	private void advanceInCurrentDepth() {
		final int top = sectionStack.pop();
		sectionStack.push(top + 1);
	}

	private void pushLevel() {
		sectionStack.push(FIRST_INDEX);
	}

	private void popLevel() {
		sectionStack.pop();
		advanceInCurrentDepth();
	}
}

// Simple wrapper around Deque<T> to restrict it to LIFO semantics
class DequeBasedStack<E> {
	private final Deque<E> deque = new ArrayDeque<>();

	public boolean isEmpty() {
		return deque.isEmpty();
	}

	public int size() {
		return deque.size();
	}

	public void push(E elem) {
		deque.push(elem);
	}

	public E pop() {
		return deque.pop();
	}

	public List<E> getElements() {
		final List<E> list = new ArrayList<>(deque);
		Collections.reverse(list);
		return list;
	}
}
