/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.testcases.query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.util.HtmlUtils;
import org.squashtest.tm.api.utils.AttachmentImageHelper;
import org.squashtest.tm.plugin.report.books.testcases.beans.ActionWordFragmentValueDto;
import org.squashtest.tm.plugin.report.books.testcases.beans.ActionWordParameterValueDto;
import org.squashtest.tm.plugin.report.books.testcases.beans.ActionWordTextValueDto;
import org.squashtest.tm.plugin.report.books.testcases.beans.Cuf;
import org.squashtest.tm.plugin.report.books.testcases.beans.Dataset;
import org.squashtest.tm.plugin.report.books.testcases.beans.DatasetParamValue;
import org.squashtest.tm.plugin.report.books.testcases.beans.I18nHelper;
import org.squashtest.tm.plugin.report.books.testcases.beans.LinkedRequirements;
import org.squashtest.tm.plugin.report.books.testcases.beans.Node;
import org.squashtest.tm.plugin.report.books.testcases.beans.Parameter;
import org.squashtest.tm.plugin.report.books.testcases.beans.PrintableCuf;
import org.squashtest.tm.plugin.report.books.testcases.beans.TestCase;
import org.squashtest.tm.plugin.report.books.testcases.beans.TestCaseSteps;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class TestCasesTreeFormatter {
	public static final Logger LOGGER = LoggerFactory.getLogger(TestCasesTreeFormatter.class);
	private static final String END_SEPARATOR_PLACEHOLDER = "=Sep=";
	private static final String SORTING_CHAIN_SEPARATOR = " ";
	private static final String CHAIN_SEPARATOR = " > ";
	private static final String SEPARATOR_PLACEHOLDER = END_SEPARATOR_PLACEHOLDER + ",";
	private static final String MILESTONE_SEPARATOR = "," + SORTING_CHAIN_SEPARATOR;
	private static final String TAG_EMPTY = "";
	private static final String TAG_SEPARATOR = " | ";
	private static final String DATE_REGEX = "\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12][0-9]|3[01])*";
	private static final int MAX_IMAGE_SIZE = 700;

	private TestCaseHierarchyHelper hierarchyHelper;

	private String projectName;

	private List<Long> tcIdsAlreadyAdd;

	private AttachmentImageHelper attachmentImageHelper;

	public void setAttachmentImageHelper(AttachmentImageHelper attachmentImageHelper) {
		this.attachmentImageHelper = attachmentImageHelper;
	}

	public Collection<TestCaseSteps> toTestCaseSteps(Collection<Object[]> testCaseStepsData,
													 Collection<ActionWordFragmentValueDto> bddActionFragments,
													 boolean printAttachments, boolean printLinkedReq, boolean printCufs) {

		Collection<TestCaseSteps> result = new ArrayList<>();
		for (Object[] array: testCaseStepsData) {
			TestCaseSteps testCaseStepsBean = new TestCaseSteps();
			testCaseStepsBean.setId(evaluateExpressionToLong(array[0]));
			String action = evaluateExpressionToString(array[1]);
			if("C".equals(evaluateExpressionToString(array[3]))) {
				testCaseStepsBean.setHasNoResult(true);
				List<String> params = new ArrayList<>();
				params.add(action);
				action = I18nHelper.translate("report.books.testcases.steps.call", params.toArray());
				testCaseStepsBean.setDelegateParameterValues(evaluateExpressionToBoolean(array[14]));
				if (!(evaluateExpressionToString(array[5]).isBlank())) {
					params.add(evaluateExpressionToString(array[5]));
					action = I18nHelper.translate("report.books.testcases.steps.call.dataset", params.toArray());
				}else if(testCaseStepsBean.isDelegateParameterValues()){
					params.add(evaluateExpressionToString(I18nHelper.translate("report.books.callstepdataset.Delegate")));
					action = I18nHelper.translate("report.books.testcases.steps.call.dataset", params.toArray());
				}
			} else if ("K".equals(evaluateExpressionToString(array[3]))) {
				testCaseStepsBean.setKeyword(evaluateExpressionToString(array[9]));
				testCaseStepsBean.setBddAction(buildBddActionForStep(testCaseStepsBean.getId(), bddActionFragments));
				testCaseStepsBean.setDatatable(evaluateExpressionToString(array[10]));
				testCaseStepsBean.setDocstring(evaluateExpressionToString(array[11]));
				testCaseStepsBean.setComment(evaluateExpressionToString(array[12]));
				testCaseStepsBean.setBddScriptLocale(I18nHelper.toLocale(evaluateExpressionToString(array[13])));
			}
			testCaseStepsBean.setAction(action);
			testCaseStepsBean.setExpectedResult(evaluateExpressionToString(array[2]));
			testCaseStepsBean.setType(evaluateExpressionToString(array[3]));
			testCaseStepsBean.setOrder(evaluateExpressionToLong(array[4]));
			testCaseStepsBean.setDataset(evaluateExpressionToString(array[5]));
			testCaseStepsBean.setAttach(evaluateExpressionToLong(array[6]));
			testCaseStepsBean.setRequirement(evaluateExpressionToLong(array[7]));
			testCaseStepsBean.setTestCaseId(evaluateExpressionToLong(array[8]));
			testCaseStepsBean.setPrintAttachments(printAttachments);
			testCaseStepsBean.setPrintStepLinkedReq(printLinkedReq);
			testCaseStepsBean.setPrintCufs(printCufs);
			result.add(testCaseStepsBean);
		}
		return result;
	}

	private String buildBddActionForStep(Long stepId, Collection<ActionWordFragmentValueDto> bddActionFragments) {
		StringBuilder stringBuilder = new StringBuilder();
		for (ActionWordFragmentValueDto fragment : bddActionFragments) {
			if (stepId.equals(fragment.getKeywordStepId())) {
				stringBuilder.append(fragment.getUnstyledAction());
			}
		}
		return stringBuilder.toString();
	}

	public Collection<PrintableCuf> toPrintableCufCollection(Collection<Cuf> cufsCollection,
												   Collection<Cuf> numCufsCollection,
												   Collection<Cuf> tagCufsCollection,
												   Collection<Cuf> rtfCufsCollection,
															 List<String> html) {
		Collection<PrintableCuf> result = new ArrayList<>();

		for (Cuf cuf : cufsCollection) {
			toPrintableCuf(cuf, result, html);
		}
		for (Cuf numCuf: numCufsCollection) {
			toPrintableCuf(numCuf, result, html);
		}
		for (Cuf tagCuf: tagCufsCollection) {
			toPrintableCuf(tagCuf, result, html);
		}
		for (Cuf rtfCuf: rtfCufsCollection) {
			toPrintableCuf(rtfCuf, result, html);
		}

		return result;
	}

	public void toPrintableCuf(Cuf cuf, Collection<PrintableCuf> resultList, List<String> html) {
		PrintableCuf printableCufBean = new PrintableCuf();
		printableCufBean.setLabel(evaluateExpressionToString(cuf.getLabel()));
		printableCufBean.setType(evaluateExpressionToString(cuf.getType()));
		printableCufBean.setRichText(printableCufBean.isRichText());
		printableCufBean.setEntityId(evaluateExpressionToLong(cuf.getEntityId()));

		if (printableCufBean.isRichText()) {
			printableCufBean.setRichTextValue(richTextReplace(cuf.getValue(), html));
		} else {
			printableCufBean.setValue(cuf.getValue());
		}

		resultList.add(printableCufBean);
	}
	public String richTextReplace(String string, List<String> html) {
		string = encodeSpecialChars(string);

		if (string.contains("img")) {
			string = findAndReplaceImageHeightWidthValue(string);
		}

		if (string.contains("/attachments/download/")) {
            Pattern pattern = Pattern.compile("<img.*?src=([\"'])?(?<src>(?:(?!\\2)[^\"'])+attachments\\/download\\/(?<id>\\d+)([^\"'])?).*?>");
			Matcher matcher = pattern.matcher(string);
			while (matcher.find()) {
				String src = matcher.group("src");
				String id = matcher.group("id");

				String str = attachmentImageHelper.getImageBase64String(Long.parseLong(id));

                if(!str.isBlank()) {
                    string = string.replace(src, "data:image/png;base64," + str);
                }
			}
		}

		StringBuilder sb = new StringBuilder(string);
		sb.insert(0, "<html>");
		sb.append("</html>");
		String newVal = "<w:altChunk r:id='toto" + html.size() + "'/>";
		html.add(sb.toString());

		return newVal;
	}

	private String encodeSpecialChars(String richText) {
		return richText.replace("“", "&ldquo;")
			.replace("”", "&rdquo;")
			.replace("’", "&rsquo;")
            // issue 9 - remove heading html tags (from Jira or GitLab) because it is used for the table of contents
            .replaceAll("<h\\d\\b[^>]*>", "<p>")
            .replaceAll("</h\\d+>", "</p>");
	}

	public Collection<Cuf> toCufBean(Collection<Object[]> cufsData) {

		Collection<Cuf> result = new ArrayList<>();
		for (Object[] array: cufsData) {
			Cuf cufBean = new Cuf();
			cufBean.setValue(evaluateExpressionToString(array[0]));
			cufBean.setLabel(evaluateExpressionToString(array[1]));
			cufBean.setType(evaluateExpressionToString(array[2]));
			cufBean.setEntityId(evaluateExpressionToLong(array[3]));
			result.add(cufBean);
		}
		return result;
	}

	Collection<Cuf> toTagCufBean(Collection<Object[]> tagCufData) {
		Collection<Cuf> cufs = new ArrayList<>();
		for (Object[] row : tagCufData) {
			Cuf cuf = new Cuf();
			if (row[0] != null) {
				String chain = evaluateExpressionToString(row[0]).replace(SEPARATOR_PLACEHOLDER, TAG_SEPARATOR);
				cuf.setValue(evaluateExpressionToString(chain.substring(0, chain.length() - END_SEPARATOR_PLACEHOLDER.length())));
			} else {
				cuf.setValue(TAG_EMPTY);
			}
			cuf.setLabel(evaluateExpressionToString(row[1]));
			cuf.setType(evaluateExpressionToString(row[2]));
			cuf.setEntityId(evaluateExpressionToLong(row[3]));
			cufs.add(cuf);
		}
		return cufs;
	}

	public Collection<LinkedRequirements> toLinkedRequirementBean(Collection<Object[]> linkedRequirementData) {

		Collection<LinkedRequirements> result = new ArrayList<>();
		for(Object[] array: linkedRequirementData) {
			LinkedRequirements linkedRequirementsBean = new LinkedRequirements();
			linkedRequirementsBean.setId(evaluateExpressionToLong(array[0]));
			linkedRequirementsBean.setName(evaluateExpressionToString(array[1]));
			linkedRequirementsBean.setProjectName(evaluateExpressionToString(array[2]));
			linkedRequirementsBean.setCriticality(evaluateExpressionToString(array[3]));
			linkedRequirementsBean.setStatus(evaluateExpressionToString(array[4]));
			linkedRequirementsBean.setVersion(evaluateExpressionToLong(array[5]));
			linkedRequirementsBean.setReference(evaluateExpressionToString(array[6]));
			linkedRequirementsBean.setTestCaseId(evaluateExpressionToLong(array[7]));
			linkedRequirementsBean.setMilestoneNames(evaluateExpressionToString(array[8]).replace(",", MILESTONE_SEPARATOR));
			result.add(linkedRequirementsBean);
		}
		return result;
	}

	public Collection<LinkedRequirements> toLinkedRequirementByStepsBean(Collection<Object[]> linkedRequirementData) {

		Collection<LinkedRequirements> result = new ArrayList<>();
		for(Object[] array: linkedRequirementData) {
			LinkedRequirements linkedRequirementsBean = new LinkedRequirements();
			linkedRequirementsBean.setId(evaluateExpressionToLong(array[0]));
			linkedRequirementsBean.setName(evaluateExpressionToString(array[1]));
			linkedRequirementsBean.setProjectName(evaluateExpressionToString(array[2]));
			linkedRequirementsBean.setReference(evaluateExpressionToString(array[3]));
			linkedRequirementsBean.setTestCaseId(evaluateExpressionToLong(array[4]));
			linkedRequirementsBean.setCriticality(evaluateExpressionToString(array[5]));
			linkedRequirementsBean.setStatus(evaluateExpressionToString(array[6]));
			result.add(linkedRequirementsBean);
		}
		return result;
	}

	public Collection<TestCase> toTestCaseBean(Collection<Object[]> testCaseData, boolean printLinkedReq,
											   boolean printParameters, boolean printFolder, boolean printSteps,
											   boolean printMilestones) {

		Collection<TestCase> result = new ArrayList<>();
		for(Object[] array: testCaseData) {
			TestCase testcase = new TestCase();
			testcase.setFolder(evaluateExpressionToLong(array[2]));

			testcase.setId(evaluateExpressionToLong(array[1]));

			testcase.setName(evaluateExpressionToString(array[3]));
			testcase.setLevel(evaluateExpressionToLong(array[4]));
			testcase.setImportance(evaluateExpressionToString(array[5]));
			testcase.setNature(evaluateExpressionToString(array[6]));
			testcase.setType(evaluateExpressionToString(array[7]));
			testcase.setNatureType(evaluateExpressionToString(array[8]));
			testcase.setTypeType(evaluateExpressionToString(array[9]));
			testcase.setStatus(evaluateExpressionToString(array[10]));
			testcase.setExecutionMode(evaluateExpressionToLong(array[11]));
			testcase.setPrerequisites(evaluateExpressionToString(array[12]));
			testcase.setReference(evaluateExpressionToString(array[13]));
			testcase.setCreatedOn(evaluateExpressionToString(array[14]));
			testcase.setCreatedBy((evaluateExpressionToString(array[15])));
			testcase.setLastModifiedOn(evaluateExpressionToString(array[16]));
			testcase.setLastModifiedBy(evaluateExpressionToString(array[17]));
			testcase.setDescription(evaluateExpressionToString(array[18]));
			testcase.setAttachments(evaluateExpressionToLong(array[19]));
			String sortingChain = evaluateExpressionToString(array[20]).replace(SEPARATOR_PLACEHOLDER,
				SORTING_CHAIN_SEPARATOR);
			testcase.setSortingChain(HtmlUtils.htmlEscape(sortingChain.substring(0,
				sortingChain.length() - END_SEPARATOR_PLACEHOLDER.length())));
			String milestoneLabels = evaluateExpressionToString(array[21]).replace(",",
				MILESTONE_SEPARATOR);
			testcase.setMilestoneNames(milestoneLabels);
			String nodeIds = evaluateExpressionToString(array[22]);
			String[] nodesIds = nodeIds.split(SEPARATOR_PLACEHOLDER);
			List<Long> hierachyNodes = new ArrayList<>();
			for(String node: nodesIds) {
				hierachyNodes.add(Long.parseLong(node.replace(END_SEPARATOR_PLACEHOLDER, "")));
			}
			testcase.setNodeIds(hierachyNodes);
			if (testcase.getFolder() == 1L) {
				String chain = evaluateExpressionToString(array[0]).replace(SEPARATOR_PLACEHOLDER, CHAIN_SEPARATOR);
				testcase.setChain(HtmlUtils.htmlEscape(chain.substring(0, chain.length() - END_SEPARATOR_PLACEHOLDER.length())));
			} else {
				String chain = evaluateExpressionToString(array[0]);
				int index = chain.lastIndexOf(SEPARATOR_PLACEHOLDER);
				if(index >= 0) {
					chain = chain.substring(0, index);
				}
				chain = chain.replace(SEPARATOR_PLACEHOLDER, CHAIN_SEPARATOR);
				testcase.setChain(chain);
			}

			testcase.setScript(evaluateExpressionToString(array[23]));
			testcase.setKind(evaluateExpressionToString(array[24]));
			testcase.setAllowAutomationWorkflow(evaluateExpressionToLong(array[25]));
			testcase.setAutomatable(evaluateExpressionToString(array[26]));
			testcase.setRequestStatus(evaluateExpressionToString(array[27]));
			testcase.setAutomationPriority(evaluateExpressionToString(array[28]));
			testcase.setPrintLinkedReq(printLinkedReq);
			testcase.setPrintParameters(printParameters);
			testcase.setPrintFolder(printFolder);
			testcase.setPrintSteps(printSteps);
			testcase.setPrintMilestones(printMilestones);

			result.add(testcase);

		}
		return result;
	}

	boolean rawMilestoneConfigToBoolean(Collection<Object[]> rawConfigurationData) {
		if (rawConfigurationData.isEmpty()) {
			return false;
		} else if (rawConfigurationData.size() == 1) {
			Object[] array = rawConfigurationData.iterator().next();
			String value = evaluateExpressionToString(array[1]);
			return "true".equalsIgnoreCase(value);
		} else {
			throw new IndexOutOfBoundsException("bug : sql query results exceed expected quantity, returned: "
				+ rawConfigurationData.size());
		}
	}
	List<Node> toNodesBean(Collection<Object[]> nodesData) {
		List<Node> nodeBeans = new ArrayList<>();
		for(Object[] array: nodesData) {
			Node nodeBean = new Node();
			nodeBean.setProjectId(evaluateExpressionToLong(array[0]));
			nodeBean.setProjectName(evaluateExpressionToString(array[1]));
			nodeBean.setItemId(evaluateExpressionToLong(array[2]));
			nodeBeans.add(nodeBean);
		}
		return nodeBeans;
	}

	Collection<TestCaseSteps> toCallStepBean(Collection<Object[]> objects, Long idTestCase, boolean printAttachments,
											 boolean printLinkedReq, boolean printCufs) {
		Collection<TestCaseSteps> result = new ArrayList<>();
		for (Object[] array: objects) {
			TestCaseSteps testCaseStepsBean = new TestCaseSteps();
			testCaseStepsBean.setId(evaluateExpressionToLong(array[0]));
			testCaseStepsBean.setAction(evaluateExpressionToString(array[1]));
			testCaseStepsBean.setExpectedResult(evaluateExpressionToString(array[2]));
			testCaseStepsBean.setType(evaluateExpressionToString(array[3]));
			testCaseStepsBean.setOrder(evaluateExpressionToLong(array[4]));
			testCaseStepsBean.setDataset(evaluateExpressionToString(array[5]));
			testCaseStepsBean.setAttach(evaluateExpressionToLong(array[6]));
			testCaseStepsBean.setRequirement(evaluateExpressionToLong(array[7]));
			testCaseStepsBean.setTestCaseId(evaluateExpressionToLong(idTestCase));
			testCaseStepsBean.setPrintAttachments(printAttachments);
			testCaseStepsBean.setPrintCufs(printCufs);
			testCaseStepsBean.setPrintStepLinkedReq(printLinkedReq);
			result.add(testCaseStepsBean);
		}
		return result;
	}

	Collection<Parameter> toParameter(Collection<Object[]> objects) {
		Collection<Parameter> parameters = new ArrayList<>();
		for(Object[] array: objects) {
			Parameter parameter = new Parameter();
			parameter.setName(evaluateExpressionToString(array[0]));
			parameter.setDescription(evaluateExpressionToString(array[1]));
			parameter.setTestCaseId(evaluateExpressionToLong(array[2]));
			parameter.setTestCaseSourceName(evaluateExpressionToString(array[3]));
			parameter.setTestCaseSourceRef(evaluateExpressionToString(array[4]));
			parameter.setTestCaseSourceProjectName(evaluateExpressionToString(array[5]));
			parameters.add(parameter);
		}
		return parameters;
	}

	Collection<Dataset> toDataSet(Collection<Object[]> objects) {
		Collection<Dataset> datasets = new ArrayList<>();
		for(Object[] array: objects) {
			Dataset dataset = new Dataset();
			dataset.setId(evaluateExpressionToLong(array[0]));
			dataset.setName(evaluateExpressionToString(array[1]));
			dataset.setTestCaseId(evaluateExpressionToLong(array[2]));
			datasets.add(dataset);
		}
		return datasets;
	}

	Collection<DatasetParamValue> toDataSetParamValue(Collection<Object[]> objects) {
		Collection<DatasetParamValue> datasetParamValues = new ArrayList<>();
		for(Object[] array: objects) {
			DatasetParamValue datasetParamValue = new DatasetParamValue();
			datasetParamValue.setDataSetId(evaluateExpressionToLong(array[0]));
			datasetParamValue.setName(evaluateExpressionToString(array[1]));
			datasetParamValue.setValue(evaluateExpressionToString(array[2]));
			datasetParamValues.add(datasetParamValue);
		}
		return datasetParamValues;
	}

	public Collection<ActionWordFragmentValueDto> toActionWordFragmentValues(Collection<Object[]> rawBddActionFragments) {
		Collection<ActionWordFragmentValueDto> fragmentValues = new ArrayList<>();
		for (Object[] array: rawBddActionFragments) {
			Long stepId = evaluateExpressionToLong(array[0]);
			String value = evaluateExpressionToString(array[2]);
			if ("PARAMETER".equals(evaluateExpressionToString(array[1]))) {
				fragmentValues.add(
					new ActionWordParameterValueDto(stepId, value));
			} else {
				fragmentValues.add(
					new ActionWordTextValueDto(stepId, value));
			}
		}
		return fragmentValues;
	}

	public void bindAll(Collection<TestCaseSteps> testCaseStepsBeans, Collection<LinkedRequirements> linkedRequirementsBeans,
						Collection<TestCase> testCaseBeans, Collection<LinkedRequirements> linkedRequirementsBySteps,
						Collection<Parameter> parameters, Collection<Dataset> datasets, Collection<DatasetParamValue> datasetParamValues,
						Collection<Node> nodes, Collection<PrintableCuf> printCufMap, Collection<PrintableCuf> printTestCaseCufMap) {

		tcIdsAlreadyAdd = new ArrayList<>();
		hierarchyHelper = new TestCaseHierarchyHelper();

		if(!testCaseStepsBeans.isEmpty()) {
			bindPrintCufToStep(testCaseStepsBeans, printCufMap);
			bindLinkedReqToTestStep(testCaseStepsBeans, linkedRequirementsBySteps);
			bindTestStepToTestCase(testCaseBeans, testCaseStepsBeans);
			updateStepOrder(testCaseBeans);
		}
		bindParamValuesToDataset(datasets, datasetParamValues);
		bindDatasetToTestCase(datasets, testCaseBeans);
		bindParameterForTestCase(testCaseBeans, parameters);
		bindPrintCufToTestCase(testCaseBeans, printTestCaseCufMap);
		if(!linkedRequirementsBeans.isEmpty()) {
			bindLinkedReqToTc(testCaseBeans, linkedRequirementsBeans);
		}
		bindTestCaseToNode(testCaseBeans, nodes);
	}

	private void updateStepOrder(Collection<TestCase> testCases) {

		for (TestCase testCase: testCases) {
			Long order = 0L;
			for (TestCaseSteps testCaseSteps: testCase.getTcSteps()) {
				testCaseSteps.setOrder(order);
				order++;
			}
		}
	}

	private void bindTestCaseToNode(Collection<TestCase> testCaseBeans, Collection<Node> nodeBeans) {
		Optional<Node> firstNodeBean = nodeBeans.stream().findFirst();
		firstNodeBean.ifPresent(node -> projectName = node.getProjectName());
		for(Node nodeBean: nodeBeans) {
			List<TestCase> testCaseBeans1 = groupTestCaseForNode(testCaseBeans,nodeBean);
			nodeBean.setTestCasesBeans(testCaseBeans1);
		}
	}

	private void bindTestStepToTestCase(Collection<TestCase> testCaseBeans, Collection<TestCaseSteps> testCaseStepsBeans) {
		for(TestCase testCaseBean: testCaseBeans) {
			List<TestCaseSteps> testCaseStepBeanList = groupTestStepForTestCase(testCaseStepsBeans, testCaseBean);
			testCaseBean.setTcSteps(testCaseStepBeanList);
		}
	}

	private void bindLinkedReqToTc(Collection<TestCase> testCaseBeans, Collection<LinkedRequirements>linkedRequirementsBeans) {
		for(TestCase testCaseBean: testCaseBeans) {
			List<LinkedRequirements> linkedRequirementsBeanList = groupLinkedRequirementForTestCase(linkedRequirementsBeans, testCaseBean);
			testCaseBean.setLinkedRequirements(linkedRequirementsBeanList);
		}
	}

	private void bindLinkedReqToTestStep(Collection<TestCaseSteps> testCaseSteps, Collection<LinkedRequirements>linkedRequirementsBeans) {
		for(TestCaseSteps caseSteps: testCaseSteps) {
			List<LinkedRequirements> linkedRequirementsBeanList = groupLinkedRequirementForTestStep(linkedRequirementsBeans, caseSteps);
			caseSteps.setLinkedRequirements(linkedRequirementsBeanList);
		}
	}

	private void bindPrintCufToStep(Collection<TestCaseSteps> testCaseStepsBeans,
									Collection<PrintableCuf> printableCufCollection) {
		for (TestCaseSteps testCaseStepsBean: testCaseStepsBeans) {
			List<PrintableCuf> testStepPrintCufs = groupPrintableCufsForTestSteps(printableCufCollection, testCaseStepsBean);
			testCaseStepsBean.setPrintableCufs(testStepPrintCufs);
		}

	}

	private void bindPrintCufToTestCase(Collection<TestCase> testCaseBeans,
											Collection<PrintableCuf> printableTestCaseCufCollection) {

		for (TestCase testCaseBean: testCaseBeans) {
			List<PrintableCuf> testCasePrintCufs = groupPrintableCufsForTestCases(printableTestCaseCufCollection, testCaseBean);
			testCaseBean.setPrintableCufs(testCasePrintCufs);
		}

	}

	private void bindParameterForTestCase(Collection<TestCase> testCases, Collection<Parameter> parameters) {
		for (TestCase testCase: testCases) {
			List<Parameter> params = groupParameterForTestCase(parameters, testCase);
			testCase.setParameters(params);
		}
	}

	private void bindParamValuesToDataset(Collection<Dataset> datasets, Collection<DatasetParamValue> datasetParamValues) {
		for(Dataset dataset: datasets) {
			List<DatasetParamValue> paramValues = groupParamValueToDataset(datasetParamValues, dataset);
			dataset.setDatasetParamValues(paramValues);
		}
	}

	private void bindDatasetToTestCase(Collection<Dataset> datasets, Collection<TestCase> testCases) {
		for(TestCase testCase: testCases) {
			List<Dataset> datasetList = groupDatasetToTestCase(datasets, testCase);
			testCase.setDatasets(datasetList);
		}
	}

	public List<PrintableCuf> groupPrintableCufsForTestCases(Collection<PrintableCuf> printCufs, TestCase testCaseBean) {
		List<PrintableCuf> cufs = new ArrayList<>();
		for(PrintableCuf cuf : printCufs) {
			if (testCaseBean.acceptAsPrintCuf(cuf)) {
				cufs.add(cuf);
			}
		}
		cufs = cufs.stream().sorted(Comparator.comparing(PrintableCuf::getLabel, String::compareToIgnoreCase)).collect(Collectors.toList());
		return cufs;
	}

	public List<TestCase> groupTestCaseForNode(Collection<TestCase> testCaseBeans, Node nodeBean) {
		Iterator<TestCase> iterator = testCaseBeans.iterator();
		List<TestCase> tc = new ArrayList<>();
		Set<Long> tcIds = new HashSet<>();
		while (iterator.hasNext()) {

			TestCase next = iterator.next();
			if(next.getNodeIds().contains(nodeBean.getItemId())) {
				tcIds.addAll(next.getNodeIds());
			}
		}

		for (TestCase testCaseBean : testCaseBeans) {
			if (tcIds.contains(testCaseBean.getId()) && !tcIdsAlreadyAdd.contains(testCaseBean.getId())) {
				if (!projectName.equals(nodeBean.getProjectName())) {
					hierarchyHelper = new TestCaseHierarchyHelper();
					projectName = nodeBean.getProjectName();
				}

				testCaseBean.setParagraph(hierarchyHelper.getNextParagraphLevel(testCaseBean.getLevel().intValue()));
				tc.add(testCaseBean);
				tcIdsAlreadyAdd.add(testCaseBean.getId());
			}
		}

		return tc;
	}

	public List<TestCaseSteps> groupTestStepForTestCase(Collection<TestCaseSteps> testCaseStepsBeans, TestCase testCaseBean) {
		Iterator<TestCaseSteps> iterator = testCaseStepsBeans.iterator();
		List<TestCaseSteps> tc = new ArrayList<>();
		while (iterator.hasNext()) {

			TestCaseSteps next = iterator.next();
			if(testCaseBean.getId().equals(next.getTestCaseId())) {
				tc.add(next);
				iterator.remove();
			}
		}
		return tc;
	}

	public List<Parameter> groupParameterForTestCase(Collection<Parameter> parameters, TestCase testCase) {
		List<Parameter> params = new ArrayList<>();
		for(Parameter param: parameters) {
			if(testCase.getId().equals(param.getTestCaseId())) {
				params.add(param);
			}
		}
		return params;
	}

	public List<LinkedRequirements> groupLinkedRequirementForTestCase(Collection<LinkedRequirements> linkedRequirementsBeans, TestCase testCaseBean) {
		Iterator<LinkedRequirements> iterator = linkedRequirementsBeans.iterator();
		List<LinkedRequirements> linkedRequirementsBeanArrayList = new ArrayList<>();
		while (iterator.hasNext()) {

			LinkedRequirements next = iterator.next();
			if(testCaseBean.getId().equals(next.getTestCaseId())) {
				linkedRequirementsBeanArrayList.add(next);
				iterator.remove();
			}
		}
		return linkedRequirementsBeanArrayList;
	}

	public List<LinkedRequirements> groupLinkedRequirementForTestStep(Collection<LinkedRequirements> linkedRequirementsBeans, TestCaseSteps testCaseSteps) {
		List<LinkedRequirements> linkedRequirementsBeanArrayList = new ArrayList<>();
		for(LinkedRequirements linkedRequirements: linkedRequirementsBeans) {
			if(testCaseSteps.getId().equals(linkedRequirements.getTestCaseId())) {
				linkedRequirementsBeanArrayList.add(linkedRequirements);
			}
		}
		return linkedRequirementsBeanArrayList;
	}

	public List<PrintableCuf> groupPrintableCufsForTestSteps(Collection<PrintableCuf> printCufs, TestCaseSteps testCaseStepsBean) {
		return printCufs.stream()
			.filter(testCaseStepsBean::acceptAsPrintCuf)
			.sorted(Comparator.comparing(PrintableCuf::getLabel, String::compareToIgnoreCase))
			.collect(Collectors.toList());
	}

	private List<DatasetParamValue> groupParamValueToDataset(Collection<DatasetParamValue> datasetParamValues, Dataset dataset) {
		List<DatasetParamValue> result = new ArrayList<>();
		for(DatasetParamValue datasetParamValue : datasetParamValues) {
			if(dataset.getId().equals(datasetParamValue.getDataSetId())) {
				result.add(datasetParamValue);
			}
		}
		return result;
	}

	private List<Dataset> groupDatasetToTestCase(Collection<Dataset> datasets, TestCase testCase) {
		List<Dataset> result = new ArrayList<>();
		for(Dataset dataset : datasets) {
			if(dataset.getTestCaseId().equals(testCase.getId())) {
				result.add(dataset);
			}
		}
		return result;
	}

	/**
	 * Convert an object to string and, if the object is a date, formats it accordingly.
	 * The regex checks if a string object contains a date in the yyyy-MM-dd format because date cufs are stored as
	 * strings in the database.
	 * @param obj
	 * @return string
	 */
	private String evaluateExpressionToString(Object obj) {

		if (obj instanceof Date){
			return convertDateTimeToString((Date) obj);
		} else if (isStringDate(obj)) {
			return formatSimpleDateString(obj.toString());
		}

		return obj != null ? obj.toString() : " ";
	}

	private boolean isStringDate(Object obj) {
		return obj instanceof String && obj.toString().matches(DATE_REGEX);
	}

	private Locale currentLocale() {
		return LocaleContextHolder.getLocale();
	}

	private String convertDateTimeToString(Date date) {
		return DateFormat.getDateTimeInstance(DateFormat.SHORT,
			DateFormat.SHORT, currentLocale()).format(date);
	}

	private String formatSimpleDateString(String stringDate) {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", currentLocale());
			Date date = formatter.parse(stringDate);
			return  DateFormat.getDateInstance(DateFormat.SHORT, currentLocale()).format(date);
		} catch (ParseException e) {
			LOGGER.warn("An error occurred when parsing the date string " + stringDate
				+ " . The date will not be converted to local format.");
			return stringDate;
		}
	}

	protected Long evaluateExpressionToLong(Object obj) {

		return Long.valueOf(obj.toString());

	}

	protected boolean evaluateExpressionToBoolean(Object obj) {

		return Boolean.parseBoolean(obj.toString());
	}

	/**
	 * Replace image height/width attribute values if they exceed the max
	 * @param string containing html image tag
	 * @return string containing html image tag with new values if applicable
	 */
	private String findAndReplaceImageHeightWidthValue(String string) {
		String regexPattern = "(?<=\")([0-9]+)(?=\")";
		Pattern pattern = Pattern.compile(regexPattern);
		Matcher matcher = pattern.matcher(string);
		while(matcher.find())	{
			String stringAttribute = matcher.group();
			int intAttribute = Integer.parseInt(stringAttribute);
			intAttribute = Math.min(intAttribute, MAX_IMAGE_SIZE);
			string = string.replace(stringAttribute, Integer.toString(intAttribute));
		}
		return string;
	}
}
