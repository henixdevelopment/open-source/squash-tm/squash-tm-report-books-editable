/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.testcases.query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.Resource;
import org.springframework.web.util.HtmlUtils;
import org.squashtest.tm.api.report.criteria.Criteria;
import org.squashtest.tm.api.report.query.ReportQuery;
import org.squashtest.tm.api.repository.SqlQueryRunner;
import org.squashtest.tm.api.utils.CurrentUserHelper;
import org.squashtest.tm.plugin.report.books.testcases.beans.ActionWordFragmentValueDto;
import org.squashtest.tm.plugin.report.books.testcases.beans.Cuf;
import org.squashtest.tm.plugin.report.books.testcases.beans.Data;
import org.squashtest.tm.plugin.report.books.testcases.beans.Dataset;
import org.squashtest.tm.plugin.report.books.testcases.beans.DatasetParamValue;
import org.squashtest.tm.plugin.report.books.testcases.beans.LinkedRequirements;
import org.squashtest.tm.plugin.report.books.testcases.beans.Node;
import org.squashtest.tm.plugin.report.books.testcases.beans.Parameter;
import org.squashtest.tm.plugin.report.books.testcases.beans.PrintableCuf;
import org.squashtest.tm.plugin.report.books.testcases.beans.Project;
import org.squashtest.tm.plugin.report.books.testcases.beans.TestCase;
import org.squashtest.tm.plugin.report.books.testcases.beans.TestCaseSteps;
import org.squashtest.tm.plugin.report.books.testcases.foundation.CriteriaEntry;
import org.squashtest.tm.plugin.report.books.testcases.foundation.CufType;
import org.squashtest.tm.plugin.report.books.testcases.foundation.EntityType;
import org.squashtest.tm.plugin.report.books.testcases.foundation.ReportOptions;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;

public class TestCasesTreeQuery implements ReportQuery, InitializingBean {
	public static final Logger LOGGER = LoggerFactory.getLogger(TestCasesTreeQuery.class);
	private static final String DATA = "data";
	private static final String DEFAULT_SORT_ORDER = "ALPHABETICAL";
	private static final String DEFAULT_TEMPLATE_VALUE = "defaultTemplate";
	private static final String FILENAME = "fileName";
	private static final String HTML = "html";
	private static final String MILESTONE_ID = "milestoneId";
	private static final String MILESTONE_LABEL = "milestoneLabel";
	private static final String MILESTONE_PICKER = "MILESTONE_PICKER";
	private static final String SQL_FIND_MILESTONE_LABEL = "select label from MILESTONE where milestone_id = :milestoneId";
	private static final String TAG_PICKER = "TAG_PICKER";
	private static final String TEMPLATE_FILE_NAME = "templateFileName";
	private static final String TEST_CASE_SELECTION_MODE = "testcasesSelectionMode";
	private static final String TEST_CASE_SORT_ORDER = "testCaseSortOrder";
	private static final String TREE_PICKER = "TREE_PICKER";

	private SqlQueryRunner runner;
	private TestCasesTreeFormatter formatter = new TestCasesTreeFormatter();
	private TestCasesTreeQueryFinder queryFinder = new TestCasesTreeQueryFinder();
	private CurrentUserHelper currentUserHelper;

	public void setFormatter(TestCasesTreeFormatter formatter) {
		this.formatter = formatter;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		queryFinder.setRunner(runner);
	}


	public void setRunner(SqlQueryRunner runner) {
		this.runner = runner;
	}

	public void setCurrentUserHelper(CurrentUserHelper currentUserHelper) {
		this.currentUserHelper = currentUserHelper;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.squashtest.tm.api.report.query.ReportQuery#executeQuery(java.util .Map, java.util.Map)
	 */
	@SuppressWarnings("unchecked")
	public void executeQuery(Map<String, Criteria> crit, Map<String, Object> res) {

		List<String> html = new ArrayList<String>();
		Map<String, Boolean> optionsMap = new HashMap<String, Boolean>();

		optionsMap.put(ReportOptions.PRINT_STEPS.getValue(), false);
		optionsMap.put(ReportOptions.PRINT_LINKED_REQUIREMENTS.getValue(), false);
		optionsMap.put(ReportOptions.PRINT_CALL_STEPS.getValue(), false);
		optionsMap.put(ReportOptions.PRINT_PARAMETERS.getValue(), false);
		optionsMap.put(ReportOptions.PRINT_STEP_CUFS.getValue(), false);
		optionsMap.put(ReportOptions.PRINT_STEP_ATTACHMENTS.getValue(), false);
		optionsMap.put(ReportOptions.PRINT_STEP_LINKED_REQUIREMENTS.getValue(), false);
		optionsMap.put(ReportOptions.PRINT_FOLDER.getValue(), false);

		Criteria options = crit.get(CriteriaEntry.REPORT_OPTIONS.getValue());
		Collection<String> selectedOptions = (Collection<String>) options.getValue();

		for (String option : selectedOptions) {
			optionsMap.put(option, true);
		}
		String testCaseSortOrder = Objects.nonNull(crit.get(TEST_CASE_SORT_ORDER))
				? (String) crit.get(TEST_CASE_SORT_ORDER).getValue()
				: DEFAULT_SORT_ORDER;
		String templateFileName = retrieveTemplateFileName(crit.get(TEMPLATE_FILE_NAME));
		Collection<Object[]> rawNodesData = getNodeBean(crit, testCaseSortOrder);
		List<Node> nodesBeans = formatter.toNodesBean(rawNodesData);
		Collection<Long> tcIds = getTcIds(crit);
		if (!tcIds.isEmpty()) {


			Collection<Object[]> rawTestCaseData = queryFinder.getTestCasesData(tcIds, testCaseSortOrder);

			Collection<Object[]> rawConfigurationData = queryFinder.getMilestoneConfiguration();
			boolean printMilestones = formatter.rawMilestoneConfigToBoolean(rawConfigurationData);
			boolean printLinkedRequirement = optionsMap.get(ReportOptions.PRINT_LINKED_REQUIREMENTS.getValue());
			boolean printParameters = optionsMap.get(ReportOptions.PRINT_PARAMETERS.getValue());
			boolean printStepCufs = optionsMap.get(ReportOptions.PRINT_STEP_CUFS.getValue());
			boolean printStepLinkedRequirement = optionsMap.get(ReportOptions.PRINT_STEP_LINKED_REQUIREMENTS.getValue());
			boolean printStepAttachment = optionsMap.get(ReportOptions.PRINT_STEP_ATTACHMENTS.getValue());
			boolean printFolder = optionsMap.get(ReportOptions.PRINT_FOLDER.getValue());
			boolean printSteps = optionsMap.get(ReportOptions.PRINT_STEPS.getValue());
			Collection<LinkedRequirements> linkedRequirementsBeans = new ArrayList<>();
			if (printLinkedRequirement) {
				Collection<Object[]> rawLinkedReqData = queryFinder.getLinkedReqData(tcIds);
				linkedRequirementsBeans = formatter.toLinkedRequirementBean(rawLinkedReqData);
			}

			Collection<Parameter> parameters = new ArrayList<>();
			Collection<Dataset> datasets = new ArrayList<>();
			Collection<DatasetParamValue> datasetParamValuesData = new ArrayList<>();
			if (printParameters) {
				Collection<Object[]> rawParametersData = queryFinder.getParametersData(tcIds);
				parameters = formatter.toParameter(rawParametersData);
				Collection<Object[]> rawDataSetData = queryFinder.getDataSetData(tcIds);
				datasets = formatter.toDataSet(rawDataSetData);
				Collection<Object[]> rawDataSetParamValuesData = queryFinder.getDataSetParamValueData(tcIds);
				datasetParamValuesData = formatter.toDataSetParamValue(rawDataSetParamValuesData);
			}


			Collection<TestCaseSteps> testCaseStepsBeans = new ArrayList<>();
			Map<String, Collection<Cuf>> stepCufMap = new HashMap<>();
			Collection<LinkedRequirements> linkedRequirementsBySteps = new ArrayList<>();
			Collection<PrintableCuf> printableStepCufs = new ArrayList<>();
			if (optionsMap.get(ReportOptions.PRINT_STEPS.getValue())) {
				Collection<Object[]> rawTestCaseStepsData = queryFinder.getTestCaseStepsData(tcIds);
				boolean printAttachments = optionsMap.get(ReportOptions.PRINT_STEP_ATTACHMENTS.getValue());
				Collection<Object[]> rawBddActionFragments = queryFinder.getBddActionFragmentsData(tcIds);
				Collection<ActionWordFragmentValueDto> actionWordFragmentValues = formatter.toActionWordFragmentValues(rawBddActionFragments);
				testCaseStepsBeans = formatter.toTestCaseSteps(rawTestCaseStepsData, actionWordFragmentValues, printAttachments, printStepLinkedRequirement, printStepCufs);
				if (optionsMap.get(ReportOptions.PRINT_CALL_STEPS.getValue())) {
					Collection<TestCaseSteps> caseStepsBeans = new ArrayList<>();
					addCalledSteps(testCaseStepsBeans, caseStepsBeans, printStepAttachment, printStepLinkedRequirement, printStepCufs);
					testCaseStepsBeans = caseStepsBeans;
				}
				List<Long> stepIds = new ArrayList<>();
				for (TestCaseSteps steps : testCaseStepsBeans) {
					stepIds.add(steps.getId());
				}


				Collection<Cuf> stepCuf = new ArrayList<>();
				Collection<Cuf> stepNumCuf = new ArrayList<>();
				Collection<Cuf> stepTagCuf = new ArrayList<>();
				Collection<Cuf> stepRtfCuf = new ArrayList<>();
				if (!stepIds.isEmpty()) {
					if (optionsMap.get(ReportOptions.PRINT_STEP_CUFS.getValue())) {
						Collection<Object[]> rawStepCuf = queryFinder.getCufData(EntityType.TEST_STEP.name(), stepIds);
						Collection<Object[]> rawStepNumCuf = queryFinder.getNumCufData(EntityType.TEST_STEP.name(), stepIds);
						Collection<Object[]> rawStepTagCuf = queryFinder.getTagCufData(EntityType.TEST_STEP.name(), stepIds);
						Collection<Object[]> rawStepRtfCuf = queryFinder.getRtfCufData(EntityType.TEST_STEP.name(), stepIds);
						stepCuf = formatter.toCufBean(rawStepCuf);
						stepNumCuf = formatter.toCufBean(rawStepNumCuf);
						stepTagCuf = formatter.toTagCufBean(rawStepTagCuf);
						stepRtfCuf = formatter.toCufBean(rawStepRtfCuf);
					}

					if (optionsMap.get(ReportOptions.PRINT_STEP_LINKED_REQUIREMENTS.getValue())) {
						Collection<Object[]> rawLinkedReqTestSteps = queryFinder.getLinkedReqStepsData(stepIds);
						linkedRequirementsBySteps = formatter.toLinkedRequirementByStepsBean(rawLinkedReqTestSteps);
					}
				}
				stepCufMap.put(CufType.CUFS.getValue(), stepCuf);
				stepCufMap.put(CufType.NUM_CUFS.getValue(), stepNumCuf);
				stepCufMap.put(CufType.TAG_CUFS.getValue(), stepTagCuf);
				stepCufMap.put(CufType.RTF_CUFS.getValue(), stepRtfCuf);

				printableStepCufs = formatter.toPrintableCufCollection(stepCuf, stepNumCuf, stepTagCuf, stepRtfCuf, html);
			}

			Collection<TestCase> testCaseBeans = formatter.toTestCaseBean(rawTestCaseData, printLinkedRequirement,
				printParameters, printFolder, printSteps, printMilestones);
			List<Long> folderIds = getFolderIds(testCaseBeans);
			Map<String, Collection<Cuf>> testCaseCufMap = new HashMap<>();
			Collection<Object[]> rawTestCaseCuf = queryFinder.getCufData(EntityType.TEST_CASE.name(), tcIds);
			rawTestCaseCuf.addAll(queryFinder.getCufData(EntityType.TESTCASE_FOLDER.name(), folderIds));
			Collection<Cuf> testCaseCuf = formatter.toCufBean(rawTestCaseCuf);
			testCaseCufMap.put(CufType.CUFS.getValue(), testCaseCuf);
			Collection<Object[]> rawTestCaseNumCuf = queryFinder.getNumCufData(EntityType.TEST_CASE.name(), tcIds);
			rawTestCaseNumCuf.addAll(queryFinder.getNumCufData(EntityType.TESTCASE_FOLDER.name(), folderIds));
			Collection<Cuf> testCaseNumCuf = formatter.toCufBean(rawTestCaseNumCuf);
			testCaseCufMap.put(CufType.NUM_CUFS.getValue(), testCaseNumCuf);
			Collection<Object[]> rawTestCaseTagCuf = queryFinder.getTagCufData(EntityType.TEST_CASE.name(), tcIds);
			rawTestCaseTagCuf.addAll(queryFinder.getTagCufData(EntityType.TESTCASE_FOLDER.name(), folderIds));
			Collection<Cuf> testCaseTagCuf = formatter.toTagCufBean(rawTestCaseTagCuf);
			testCaseCufMap.put(CufType.TAG_CUFS.getValue(), testCaseTagCuf);
			Collection<Object[]> rawTestCaseRtfCuf = queryFinder.getRtfCufData(EntityType.TEST_CASE.name(), tcIds);
			rawTestCaseRtfCuf.addAll(queryFinder.getRtfCufData(EntityType.TESTCASE_FOLDER.name(), folderIds));
			Collection<Cuf> testCaseRtfCuf = formatter.toCufBean(rawTestCaseRtfCuf);
			testCaseCufMap.put(CufType.RTF_CUFS.getValue(), testCaseRtfCuf);
			Collection<PrintableCuf> testCasePrintCufs = formatter.toPrintableCufCollection(testCaseCuf, testCaseNumCuf, testCaseTagCuf, testCaseRtfCuf, html);

			formatter.bindAll(testCaseStepsBeans, linkedRequirementsBeans, testCaseBeans, linkedRequirementsBySteps,
				parameters,	datasets, datasetParamValuesData, nodesBeans, printableStepCufs, testCasePrintCufs);
		}
		String milestoneLabel = null;
		// special milestone mode
		if (isMilestonePicker(crit)) {

			List<Integer> milestoneIds = (List<Integer>) crit.get(CriteriaEntry.MILESTONES.getValue()).getValue();

			Map<String, Object> params = new HashMap<>();
			params.put(MILESTONE_ID, milestoneIds.get(0));

			milestoneLabel = runner.executeUniqueSelect(SQL_FIND_MILESTONE_LABEL, params);

			res.put(MILESTONE_LABEL, milestoneLabel);
		}

		processRichText(nodesBeans, html);
		processChain(nodesBeans);

		Data data = new Data(milestoneLabel, testCaseSortOrder, templateFileName);
		data.setProjects(populateProjectFromNode(nodesBeans));
		res.put(FILENAME, data.getFileTitleTestCasesReport());
		res.put(DATA, data);
		res.put(HTML, html);
	}

	private String retrieveTemplateFileName(Criteria templateCriteria) {
		if (Objects.nonNull(templateCriteria)) {
			try {
				return (String) templateCriteria.getValue();
			} catch (ClassCastException cce) {
				// When file does not exist anymore, it returns an EmptyCriteria which does not have any value attribute.
				LOGGER.trace("Custom template file does not exist anymore.", cce);
				// then we put the default template instead.
			}
		}
		return DEFAULT_TEMPLATE_VALUE;
	}


	private List<Long> getFolderIds(Collection<TestCase> testCaseBeans) {
		List<Long> folderIds = new ArrayList<>();
		for (TestCase testCaseBean : testCaseBeans) {
			if(testCaseBean.isFolder()){
				folderIds.add(testCaseBean.getId());
			}
		}
		return folderIds;
	}

	private Collection<Long> getTcIds(Map<String, Criteria> criteriaMap) {
		Criteria selectionMode = criteriaMap.get(TEST_CASE_SELECTION_MODE);

		Collection<Long> testCaseIdList = Collections.emptyList();
        List<Long> readableProjectIds = currentUserHelper.findReadableProjectIds();
		if (TREE_PICKER.equals(selectionMode.getValue())) {
			Criteria idsCrit = criteriaMap.get(CriteriaEntry.TEST_CASE_IDS.getValue());
			Collection<String> testCaseIds = addNodesIds(idsCrit);
			testCaseIdList = queryFinder.findIdsBySelection(testCaseIds, readableProjectIds);
		} else if (MILESTONE_PICKER.equals(selectionMode.getValue())) {
			Collection<String> milestones = (Collection<String>) criteriaMap.get(CriteriaEntry.MILESTONES.getValue()).getValue();
			testCaseIdList = queryFinder.findIdsByMilestone(milestones, readableProjectIds);
		} else if (TAG_PICKER.equals(selectionMode.getValue())) {
			Collection<String> tags = (Collection<String>) criteriaMap.get(CriteriaEntry.TAGS.getValue()).getValue();
			testCaseIdList = queryFinder.findIdsByTags(tags, readableProjectIds);
		} else {
			Criteria idsCrit = criteriaMap.get(CriteriaEntry.PROJECT_IDS.getValue());
			if (!idsCrit.getValue().toString().trim().isEmpty()) {
				List<Long> projectIds = ((Collection<String>) idsCrit.getValue())
                    .stream()
                    .map(Long::valueOf)
                    .toList();
                readableProjectIds.retainAll(projectIds);
				testCaseIdList = queryFinder.findIdsByProject(readableProjectIds);
			}
		}

		return testCaseIdList;
	}

	private Collection<Object[]> getNodeBean(Map<String, Criteria> criteriaMap, String testCaseSortOrder) {
		Criteria selectionMode = criteriaMap.get(CriteriaEntry.TEST_CASES_SELECTION_MODE.getValue());
		Collection<Object[]> result = new ArrayList<>();
        List<Long> readableProjectIds = currentUserHelper.findReadableProjectIds();
		if (TREE_PICKER.equals(selectionMode.getValue())) {
			Criteria idsCrit = criteriaMap.get(CriteriaEntry.TEST_CASE_IDS.getValue());
			Collection<String> testCaseIds = addNodesIds(idsCrit);
			result = queryFinder.getNodesByTestCasesIds(testCaseIds, readableProjectIds, testCaseSortOrder);
		} else if (MILESTONE_PICKER.equals(selectionMode.getValue())) {
			Collection<String> milestoneIds = (Collection<String>) criteriaMap.get(CriteriaEntry.MILESTONES.getValue()).getValue();
			result = queryFinder.getNodesByMilestones(milestoneIds, readableProjectIds, testCaseSortOrder);
		} else if (TAG_PICKER.equals(selectionMode.getValue())) {
			Collection<String> tags = (Collection<String>) criteriaMap.get(CriteriaEntry.TAGS.getValue()).getValue();
			result = queryFinder.getNodesTags(tags, readableProjectIds, testCaseSortOrder);
		} else {
			Criteria idsCrit = criteriaMap.get(CriteriaEntry.PROJECT_IDS.getValue());
			if (!idsCrit.getValue().toString().trim().isEmpty()) {
				List<Long> projectIds = ((Collection<String>) idsCrit.getValue())
                    .stream()
                    .map(Long::valueOf)
                    .toList();
                readableProjectIds.retainAll(projectIds);
				result = queryFinder.getNodesByProjectIds(readableProjectIds, testCaseSortOrder);
			}
		}

		return result;

	}

	public void setIdsByProjectQuery(Resource idsByProjectQuery) {
		String query = loadQuery(idsByProjectQuery);
		queryFinder.setIdsByProjectQuery(query);
	}

	public void setIdsByTagQuery(Resource idsByTagQuery) {
		String query = loadQuery(idsByTagQuery);
		queryFinder.setIdsByTagQuery(query);
	}

	public void setIdsBySelectionQuery(Resource idsBySelectionQuery) {
		String query = loadQuery(idsBySelectionQuery);
		queryFinder.setIdsBySelectionQuery(query);
	}

	public void setIdsByMilestoneQuery(Resource idsByMilestoneQuery) {
		String query = loadQuery(idsByMilestoneQuery);
		queryFinder.setIdsByMilestoneQuery(query);
	}

	public void setTestCaseDataQuery(Resource testCaseDataQuery) {
		String query = loadQuery(testCaseDataQuery);
		queryFinder.setTestCasesQuery(query);
	}

	public void setLinkedReqDataQuery(Resource linkedReqDataQuery) {
		String query = loadQuery(linkedReqDataQuery);
		queryFinder.setLinkedReqQuery(query);
	}

	public void setTestCaseStepsDataQuery(Resource testCaseStepsDataQuery) {
		String query = loadQuery(testCaseStepsDataQuery);
		queryFinder.setTestCaseStepsQuery(query);
	}

	public void setBddActionFragmentsDataQuery(Resource bddActionFragmentsDataQuery) {
		String query = loadQuery(bddActionFragmentsDataQuery);
		queryFinder.setBddActionFragmentsQuery(query);
	}

	public void setCufsDataQuery(Resource cufsDataQuery) {
		String query = loadQuery(cufsDataQuery);
		queryFinder.setCufQuery(query);
	}

	public void setNumCufsDataQuery(Resource numCufsDataQuery) {
		String query = loadQuery(numCufsDataQuery);
		queryFinder.setNumCufQuery(query);
	}

	public void setTagCufsDataQuery(Resource tagCufsDataQuery) {
		String query = loadQuery(tagCufsDataQuery);
		queryFinder.setTagCufQuery(query);
	}

	public void setRtfCufsDataQuery(Resource rtfCufsDataQuery) {
		String query = loadQuery(rtfCufsDataQuery);
		queryFinder.setRtfCufQuery(query);
	}

	public void setNodesByTestCaseIdsDataQuery(Resource nodesByTestCaseIdsDataQuery) {
		String query = loadQuery(nodesByTestCaseIdsDataQuery);
		queryFinder.setNodesByTestCaseIdsQuery(query);
	}

	public void setNodesByMilestonesDataQuery(Resource nodesByMilestonesDataQuery) {
		String query = loadQuery(nodesByMilestonesDataQuery);
		queryFinder.setNodesByMilestonesQuery(query);
	}

	public void setNodesByTagsDataQuery(Resource nodesByTagsDataQuery) {
		String query = loadQuery(nodesByTagsDataQuery);
		queryFinder.setNodesByTagsQuery(query);
	}

	public void setNodesByProjectIdsDataQuery(Resource nodesByProjectIdsDataQuery) {
		String query = loadQuery(nodesByProjectIdsDataQuery);
		queryFinder.setNodesByProjectIdsQuery(query);
	}

	public void setStepsByCallStepDataQuery(Resource stepsByCallStepDataQuery) {
		String query = loadQuery(stepsByCallStepDataQuery);
		queryFinder.setTestStepsByCallSteps(query);
	}
	public void setLinkedReqByStepsDataQuery(Resource stepsByCallStepDataQuery) {
		String query = loadQuery(stepsByCallStepDataQuery);
		queryFinder.setLinkedReqByTestStepQuery(query);
	}

	public void setMilestoneConfigurationQuery(Resource milestoneConfigurationQuery) {
		String query = loadQuery(milestoneConfigurationQuery);
		queryFinder.setMilestoneConfigurationQuery(query);
	}

	public void setParameterDataQuery(Resource parameterDataQuery) {
		String query = loadQuery(parameterDataQuery);
		queryFinder.setParametersQuery(query);
	}

	public void setDataSetDataQuery(Resource dataSetDataQuery) {
		String query = loadQuery(dataSetDataQuery);
		queryFinder.setDataSetQuery(query);
	}

	public void setDataSetParamValueDataQuery(Resource dataSetParamValueDataQuery) {
		String query = loadQuery(dataSetParamValueDataQuery);
		queryFinder.setDataSetParamValueQuery(query);
	}


	protected String loadQuery(Resource query) {
		InputStream is;
		try {
			is = query.getInputStream();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return new Scanner(is, "UTF-8").useDelimiter("\\A").next();
	}

	// This method can be optimized if we process by layer but it's more difficult to link call step to the good test case
	private void addCalledSteps(Collection<TestCaseSteps> stepsBeans, Collection<TestCaseSteps> allSteps, boolean printAttachment, boolean printStepLinkedReq, boolean printStepCufs) {

		for(TestCaseSteps testCaseStepsBean : stepsBeans) {
			if("C".equals(testCaseStepsBean.getType())) {
				Long id = testCaseStepsBean.getId();
				Collection<Object[]> objects = queryFinder.getTestCaseStepsData(id);
				testCaseStepsBean.getDataset();
				Collection<TestCaseSteps> testCaseStepsBeans = formatter.toCallStepBean(objects,testCaseStepsBean.getTestCaseId(), printAttachment, printStepLinkedReq, printStepCufs);
				addCalledSteps(testCaseStepsBeans, allSteps, printAttachment, printStepLinkedReq, printStepCufs);

			} else {
				allSteps.add(testCaseStepsBean);
			}
		}

	}

	/**
	 * We don't give a shit about nodes in report. We want to organize the test cases by projects !
	 *
	 * @param nodesBeans
	 * @return
	 */
	private List<Project> populateProjectFromNode(List<Node> nodesBeans) {

		Map<Long, Project> map = new LinkedHashMap<Long, Project>();

		for (Node node : nodesBeans) {
			Project p = map.get(node.getProjectId());
			if (p != null) {
				p.addTestCases(node.getTestCasesBeans());
			} else {
				p = new Project();
				p.setProjectId(node.getProjectId());
				p.setProjectName(node.getProjectName());
				p.setTestCases(node.getTestCasesBeans());
				map.put(p.getProjectId(), p);
			}
		}

		return new ArrayList<>(map.values());
	}

	private void processRichText(List<Node> nodesBeans, List<String> html) {
		for (Node nodes : nodesBeans) {
			richTextForTc(nodes.getTestCasesBeans(), html);
		}
	}

	private void richTextForTc(List<TestCase> tcs, List<String> html) {
		for (TestCase tc : tcs) {
			tc.setDescription(formatter.richTextReplace(tc.getDescription(), html));
			tc.setPrerequisites(formatter.richTextReplace(tc.getPrerequisites(), html));
			if(!tc.isScriptEmpty())tc.setScript(replaceScriptString(tc.getScript(), html));
			richTextForParameter(tc.getParameters(), html);
			richTextForStep(tc.getTcSteps(), html);
		}
	}

	private String replaceScriptString(String initScript, List<String> html) {

		initScript = HtmlUtils.htmlEscape(initScript);
		initScript = initScript.replaceAll("\\t", "&nbsp;&nbsp;&nbsp;&nbsp;");
		initScript = initScript.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
		initScript = initScript.replaceAll("\\n", "<br />");
		return formatter.richTextReplace("<p>" + initScript + "</p>", html);
	}

	private void richTextForStep(List<TestCaseSteps> steps, List<String> html) {
		for (TestCaseSteps step : steps) {
			/* Issue #6463: If it is a called Test Case,
			 * it's name must be encoded in html before. */
			if ("C".equals(step.getType())) {
				step.setAction(HtmlUtils.htmlEscape(step.getAction()));
			}
			step.setAction(formatter.richTextReplace(step.getAction(), html));
			step.setExpectedResult(formatter.richTextReplace(step.getExpectedResult(), html));
		}

	}

	private void richTextForParameter(List<Parameter> parameters, List<String> html) {
		for(Parameter param: parameters) {
			param.setDescription(formatter.richTextReplace(param.getDescription(), html));
		}
	}

	private void processChain(List<Node> nodeBeans) {
		String previousChain = null;
		String folderDescription = "";
		for (Node nodes : nodeBeans) {
			for (TestCase tc : nodes.getTestCasesBeans()) {
				if (!tc.isFolder()) {
					boolean shouldReprint = (!tc.getChain().equals(previousChain));
					previousChain = tc.getChain();
					tc.setPrintChain(shouldReprint);
					folderDescription = folderDescriptionForTestCase(tc, folderDescription);
				} else {
					folderDescription = tc.getDescription();
				}
			}
		}
	}

	private String folderDescriptionForTestCase(TestCase tc, String folderDescription) {
		if (!"".equals(folderDescription)) {
			tc.setChainDescription(folderDescription);
		}
		return "";
	}

	/**
	 * Should return a list of Node Ids (in this case : requirement Ids) selected from the form.
	 *
	 * @param idsCrit
	 * @return
	 */
	@SuppressWarnings({"rawtypes", "unchecked"})
	private Collection addNodesIds(Criteria idsCrit) {
		if (idsCrit != null) {
			Collection<?> ids = ((Map<String, Collection<?>>) idsCrit.getValue()).values();
			if (!ids.isEmpty()) {
				Collection nodesIds = new HashSet<String>();
				nodesIds.addAll(ids);
				return nodesIds;
			}
			return Collections.emptyList();
		}
		return Collections.emptyList();
	}

	private boolean isMilestonePicker(Map<String, Criteria> criteriaMap) {
		Criteria selectionMode = criteriaMap.get(CriteriaEntry.TEST_CASES_SELECTION_MODE.getValue());
		return MILESTONE_PICKER.equals(selectionMode.getValue());
	}

}
