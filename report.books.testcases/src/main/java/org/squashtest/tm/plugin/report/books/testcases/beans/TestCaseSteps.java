/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.testcases.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class TestCaseSteps {

	private Long id;
	private String action;
	private String expectedResult;
	private String type;
	private Long order;
	private String dataset;
	private String prerequisites;
	private Long attach;
	private Long requirement;
	private List<LinkedRequirements> linkedRequirements = new ArrayList<>();
	private Long testCaseId;
	private boolean printAttachments;
	private boolean printCufs;
	private boolean printStepLinkedReq;
	private String keyword;
	private String bddAction;
	private String datatable;
	private String docstring;
	private String comment;
	private Locale bddScriptLocale;
	private boolean delegateParameterValues;
	private boolean hasNoResult = false;

	public String getAttach() {
			return String.valueOf(attach);
	}

	public void setAttach(Long attach) {
		this.attach = attach;
	}

	public String getRequirement() {
		if (requirement == 0){
			return I18nHelper.translate(Data.KEY_PREFIX+"steps.requirement.none");
		} else {
			return String.valueOf(requirement);
		}
	}

	public void setRequirement(Long requirement) {
		this.requirement = requirement;
	}

	private List<PrintableCuf> printableCufs;

	public boolean acceptAsCuf(Cuf cufBean) {
		return  cufBean.getEntityId().equals(id);
	}
	public boolean acceptAsPrintCuf(PrintableCuf printCufBean) {
		return  printCufBean.getEntityId().equals(id);
	}

	public boolean isNoDatatable() {
		return "".equals(datatable);
	}

	public boolean isNoDocstring() {
		return "".equals(docstring);
	}

	public boolean isNoComment() {
		return "".equals(comment);
	}

	public void setOrder(Long order) {
		this.order = order;
	}
	public Long getOrder() {
		return order + 1;
	}

	/* plain getters & setters */

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}

	public String getExpectedResult() {
		return expectedResult;
	}
	public void setExpectedResult(String expectedResult) {
		this.expectedResult = expectedResult;
	}

	public void setType(String type) {
		this.type = type;
	}
	public String getType() {
		return type;
	}

	public String getDataset() {
		return dataset;
	}
	public void setDataset(String dataset) {
		this.dataset = dataset;
	}

	public String getPrerequisites() {
		return prerequisites;
	}
	public void setPrerequisites(String prerequisites) {
		this.prerequisites = prerequisites;
	}

	public List<PrintableCuf> getPrintableCufs() {
		return printableCufs;
	}
	public void setPrintableCufs(List<PrintableCuf> printableCufs) {
		this.printableCufs = printableCufs;
	}

	public Long getTestCaseId() {
		return testCaseId;
	}
	public void setTestCaseId(Long testCaseId) {
		this.testCaseId = testCaseId;
	}

	public List<LinkedRequirements> getLinkedRequirements() {
		return linkedRequirements;
	}
	public void setLinkedRequirements(List<LinkedRequirements> linkedRequirements) {
		this.linkedRequirements = linkedRequirements;
	}

	public boolean isPrintAttachments() {
		return printAttachments;
	}
	public void setPrintAttachments(boolean printAttachments) {
		this.printAttachments = printAttachments;
	}

	public boolean isPrintCufs() {
		return printCufs;
	}
	public void setPrintCufs(boolean printCufs) {
		this.printCufs = printCufs;
	}

	public boolean isPrintStepLinkedReq() {
		return printStepLinkedReq;
	}
	public void setPrintStepLinkedReq(boolean printStepLinkedReq) {
		this.printStepLinkedReq = printStepLinkedReq;
	}

	public String getKeyword() {
		if(this.keyword==null||this.bddScriptLocale==null)
			return "";
		return I18nHelper.translateTo("testcase.bdd.keyword.name."+this.keyword.toLowerCase(), this.bddScriptLocale);
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getBddAction() {
		return bddAction;
	}
	public void setBddAction(String bddAction) {
		this.bddAction = bddAction;
	}

	public String getDatatable() {
		return datatable;
	}
	public void setDatatable(String datatable) {
		this.datatable = datatable;
	}

	public String getDocstring() {
		return docstring;
	}
	public void setDocstring(String docstring) {
		this.docstring = docstring;
	}

	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

	public Locale getBddScriptLocale() {
		return bddScriptLocale;
	}
	public void setBddScriptLocale(Locale bddScriptLocale) {
		this.bddScriptLocale = bddScriptLocale;
	}

	public void setDelegateParameterValues(boolean delegateParameterValues) {
		this.delegateParameterValues = delegateParameterValues;
	}
	public boolean isDelegateParameterValues() {
		return delegateParameterValues;
	}

	public void setHasNoResult(boolean hasNoResult) {
		this.hasNoResult = hasNoResult;
	}
	public boolean isHasNoResult() {
		return hasNoResult;
	}
}
