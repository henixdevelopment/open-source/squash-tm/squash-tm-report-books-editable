/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.testcases.beans;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ActionWordParameterValueDto extends ActionWordFragmentValueDto {

	private static final String REGEX_PARAM_VALUE_LINKED_TO_TEST_CASE_PARAM = "<[^\"]+>";
	private static final String QUOTED_VALUE_FORMAT = "\"%s\"";

	public ActionWordParameterValueDto(Long keywordStepId, String value) {
		super(keywordStepId, value);
	}

	@Override
	public String getUnstyledAction() {
		if ("".equals(value) || referencesTestCaseParameter() || isNumber()) {
			return value;
		} else {
			return String.format(QUOTED_VALUE_FORMAT, value);
		}
	}

	private boolean referencesTestCaseParameter() {
		Pattern pattern = Pattern.compile(REGEX_PARAM_VALUE_LINKED_TO_TEST_CASE_PARAM);
		Matcher matcher = pattern.matcher(value);
		return matcher.matches();
	}

	private boolean isNumber() {
		return value.matches("-?\\d+(([.,])\\d+)?");
	}
}
