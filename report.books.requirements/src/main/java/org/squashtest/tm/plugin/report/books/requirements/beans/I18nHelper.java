/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.requirements.beans;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Component("report.books.requirements.i18nHelper")
public class I18nHelper implements MessageSourceAware {

	private static MessageSource msgSource;

	private static Locale currentLocale() {
		return LocaleContextHolder.getLocale();
	}

	public static String translate(String string) {
		return msgSource.getMessage(string, null, currentLocale());
	}

	public static String translate(String string, Object[] params) {
		return msgSource.getMessage(string, params, currentLocale());
	}

	public static String translate(Date date) {
		String dateFormat = msgSource.getMessage("squashtm.dateformat", null, currentLocale());
		return new SimpleDateFormat(dateFormat).format(date);
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		msgSource = messageSource;

	}

	public String getRequirementCategory(String category, String categoryType){
		if (categoryType.equals("SYS")) {
			return translate("report.books.requirements." + category.toLowerCase());
		} else {
			return category;
		}
	}

	public String getRequirementCriticality(String criticality) {

		String string;
		switch (criticality) {
			case "MAJOR":
				string = "report.books.requirements.criticality.major";
				break;
			case "MINOR":
				string = "report.books.requirements.criticality.minor";
				break;
			case "CRITICAL":
				string = "report.books.requirements.criticality.critical";
				break;
			default:
				string = "report.books.requirements.criticality.undefined";
		}

		return translate(string);
	}

	public String getRequirementStatus(String status) {
		String string;
		switch (status) {
			case "APPROVED":
				string = "report.books.requirements.status.approved";
				break;
			case "OBSOLETE":
				string = "report.books.requirements.status.obsolete";
				break;
			case "UNDER_REVIEW":
				string = "report.books.requirements.status.under_review";
				break;
			default:
				string = "report.books.requirements.status.work_in_progress";
		}
		return translate(string);
	}

	public String getTestCaseStatus(String status) {
		String msg;
		switch (status) {
			case "TO_BE_UPDATED":
				msg = "report.books.requirements.testcases.status.to_be_updated";
				break;
			case "UNDER_REVIEW":
				msg = "report.books.requirements.testcases.status.under_review";
				break;
			case "APPROVED":
				msg = "report.books.requirements.testcases.status.approved";
				break;
			case "OBSOLETE":
				msg = "report.books.requirements.testcases.status.obsolete";
				break;
			default:
				msg = "report.books.requirements.testcases.status.work_in_progress";
		}
		return translate(msg);
	}

	public String getTestCaseImportance(String importance) {
		String msg;
		switch (importance) {
			case "LOW":
				msg = "report.books.requirements.testcases.importance.low";
				break;
			case "MEDIUM":
				msg = "report.books.requirements.testcases.importance.medium";
				break;
			case "HIGH":
				msg = "report.books.requirements.testcase.importance.high";
				break;
			case "VERY_HIGH":
				msg = "report.books.requirements.testcases.importance.very_high";
				break;
			default:
				msg = "report.books.requirements.testcases.importance.undefined";
		}
		return translate(msg);
	}
}
