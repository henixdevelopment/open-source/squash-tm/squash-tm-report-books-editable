/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.requirements.beans;

import org.squashtest.tm.domain.testcase.TestCaseImportance;

public class TestCase {


	private Long tcId;
	private String projectName;
	private String tcRef;
	private String tcName;
	private String tcMilestone;
	private String tcStatus;
	private String tcImportance;

	private static final I18nHelper translator = new I18nHelper();

	public Long getTcId() {
		return tcId;
	}

	public void setTcId(Long tcId) {
		this.tcId = tcId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getTcRef() {
		return tcRef;
	}

	public void setTcRef(String tcRef) {
		this.tcRef = tcRef;
	}

	public String getTcName() {
		return tcName;
	}

	public void setTcName(String tcName) {
		this.tcName = tcName;
	}

	public String getTcMilestone() {
		return TextFormatter.formatMilestones(tcMilestone);
	}

	public void setTcMilestone(String tcMilestone) {
		this.tcMilestone = tcMilestone;
	}

	public String getTcStatus() {
		return translator.getTestCaseStatus(tcStatus);
	}

	public void setTcStatus(String tcStatus) {
		this.tcStatus = tcStatus;
	}

	public String getTcImportance() {
		return translator.getTestCaseImportance(tcImportance);
	}

	public void setTcImportance(String tcImportance) {
		this.tcImportance = tcImportance;
	}

	public int getImportanceLevel() {
		switch (this.tcImportance) {
			case "HIGH":
				return TestCaseImportance.HIGH.getLevel();
			case "MEDIUM":
				return TestCaseImportance.MEDIUM.getLevel();
			case "LOW":
				return TestCaseImportance.LOW.getLevel();
			default: return TestCaseImportance.VERY_HIGH.getLevel();
		}
	}
}
