/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.requirements.query;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.squashtest.tm.api.repository.SqlQueryRunner;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class RequirementTreeQueryFinder {

	private static final String VERSION_IDS = "versionIds";
	private static final String REQUIREMENT_IDS = "requirementIds";
	private static final String REQUIREMENTS_SORT_ORDER = "requirementsSortOrder";
	private static final String HLR_IDS = "hlrIds";
	private static final String PROJECT_IDS = "projectIds";

	private static final String SQL_FIND_MILESTONE_LABEL = "select label from MILESTONE where milestone_id = :milestoneId";


	private String idsByProjectQuery;
	private String idsBySelectionQuery;
	private String idsByMilestoneQuery;
	private String requirementDataQuery;
	private String previousVersionDataQuery;
	private String boundTCDataQuery;
	private String cufsDataQuery;
	private String attachReqDataQuery;
	private String tagCufsDataQuery;
	private String idsByTagQuery;
	private String reqLinkDataQuery;
	private String allProjects;
	private String projectsByMilestone;
	private String projectsByTags;
	private String projectsBySelection;
	private String reqVersionsIds;
	private String reqLinkedToHlr;
	private String boundTCtoHLRDataQuery;

	private String milestoneConfigurationQuery;

	protected SqlQueryRunner runner;

	RequirementTreeQueryFinder() {}


	void setIdsByProjectQuery(String idsByProjectQuery) {
		this.idsByProjectQuery = idsByProjectQuery;
	}

	void setIdsBySelectionQuery(String idsBySelectionQuery) {
		this.idsBySelectionQuery = idsBySelectionQuery;
	}

	public void setIdsByTagQuery(String idsByTagQuery) {
		this.idsByTagQuery = idsByTagQuery;
	}

	void setRequirementDataQuery(String requirementDataQuery) {
		this.requirementDataQuery = requirementDataQuery;
	}

	void setBoundTCDataQuery(String boundTCDataQuery) {
		this.boundTCDataQuery = boundTCDataQuery;
	}

	public void setCufsDataQuery(String cufsDataQuery) {
		this.cufsDataQuery = cufsDataQuery;
	}

	public void setAttachReqDataQuery(String attachReqDataQuery) {
		this.attachReqDataQuery = attachReqDataQuery;
	}

	public void setTagCufsDataQuery(String tagCufsDataQuery) {
		this.tagCufsDataQuery = tagCufsDataQuery;
	}

	public void setReqLinkDataQuery(String reqLinkDataQuery) {
		this.reqLinkDataQuery = reqLinkDataQuery;
	}

	void setRunner(SqlQueryRunner runner) {
		this.runner = runner;
	}

	void setIdsByMilestoneQuery(String idsByMilestoneQuery) {
		this.idsByMilestoneQuery = idsByMilestoneQuery;
	}

	public void setAllProjects(String allProjects) {
		this.allProjects = allProjects;
	}

	public void setProjectsByMilestone(String projectsByMilestone) {
		this.projectsByMilestone = projectsByMilestone;
	}

	public void setProjectsByTags(String projectsByTags) {
		this.projectsByTags = projectsByTags;
	}

	public void setProjectsBySelection(String projectsBySelection) {
		this.projectsBySelection = projectsBySelection;
	}

	public void setReqVersionsIds(String reqVersionsIds) {
		this.reqVersionsIds = reqVersionsIds;
	}

	public void setReqLinkedToHlr(String reqLinkedToHlr) {
		this.reqLinkedToHlr = reqLinkedToHlr;
	}

	public void setBoundTCtoHLRDataQuery(String boundTCtoHLRDataQuery) {
		this.boundTCtoHLRDataQuery = boundTCtoHLRDataQuery;
	}

	public void setPreviousVersionDataQuery(String previousVersionDataQuery) {
		this.previousVersionDataQuery = previousVersionDataQuery;
	}

	void setMilestoneConfigurationQuery(String milestoneConfigurationQuery) {
		this.milestoneConfigurationQuery = milestoneConfigurationQuery;
	}

	// ******************************** Id Finding ***********************

	Collection<Long> findIdsByProject(List<Long> projectIds) {
		if (CollectionUtils.isEmpty(projectIds)) {
			return Collections.emptyList();
		}

		Map<String, Collection<Long>> params = new HashMap<>(1);
		params.put(PROJECT_IDS, projectIds);
        List<BigInteger> foundStrIds = runner.executeSelect(idsByProjectQuery, params);

		return toIdList(foundStrIds);
	}

	public Collection<Object[]> findProjects(Collection<String> projectStrIds) {
		Collection<Long> projectIds = toIdList(projectStrIds);
		Map<String, Collection<Long>> params = new HashMap<>(1);
		params.put(PROJECT_IDS, projectIds);
		return runner.executeSelect(allProjects, params);
	}

	Collection<Long> findIdsBySelection(Collection<String> ids, List<Long> projectIds) {

		List<Long> versionIds = new LinkedList<>();

		if (ids != null && (!ids.isEmpty())) {

			Map<String, Collection<Long>> params = new HashMap<>(1);
			params.put("nodeIds", toIdList(ids));
            params.put(PROJECT_IDS, projectIds);
			List<BigInteger> foundStrIds = runner.executeSelect(idsBySelectionQuery, params);

			versionIds.addAll(toIdList(foundStrIds));
		} else {
			versionIds = new ArrayList<>();
		}

		return versionIds;

	}

	public Collection<Object[]> findProjectsBySelection(Collection<String> ids) {

		if (ids != null && (!ids.isEmpty())) {
			Map<String, Collection<Long>> params = new HashMap<>(1);
			params.put("nodeIds", toIdList(ids));
			return runner.executeSelect(projectsBySelection, params);

		} else {
			return Collections.emptyList();
		}
	}

	Collection<Long> findIdsByMilestone(Collection<String> milestoneIds, List<Long> projectIds) {

		if (!milestoneIds.isEmpty()) {
			Map<String, Collection<Long>> params = new HashMap<>();
			params.put("milestones", toIdList(milestoneIds));
			params.put(PROJECT_IDS, projectIds);
			List<BigInteger> foundIds = runner.executeSelect(idsByMilestoneQuery, params);
			return toIdList(foundIds);
		} else {
			return Collections.emptyList();
		}

	}

	public Collection<Object[]> findProjectsByMilestone(List<String> milestoneIds, List<Long> projectIds) {
		if (!milestoneIds.isEmpty()) {
			Map<String, Collection<Long>> params = new HashMap<>();
			params.put("milestones", toIdList(milestoneIds));
			params.put(PROJECT_IDS, projectIds);
			return runner.executeSelect(projectsByMilestone, params);
		} else {
			return Collections.emptyList();
		}
	}

	public Collection<Long> findIdsByTags(List<String> tags, List<Long> projectIds) {

		if (!tags.isEmpty()) {
			Map<String, Object> params = new HashMap<>();
			params.put("tags", tags);
			params.put(PROJECT_IDS, projectIds);
			List<BigInteger> foundIds = runner.executeSelect(idsByTagQuery, params);
			return toIdList(foundIds);
		} else {
			return Collections.emptyList();
		}
	}

	public Collection<Object[]> findProjectsByTags(List<String> tags, List<Long> projectIds) {
		if (!tags.isEmpty()) {
			Map<String, Object> params = new HashMap<>();
			params.put("tags", tags);
			params.put(PROJECT_IDS, projectIds);
			return runner.executeSelect(projectsByTags, params);
		} else {
			return Collections.emptyList();
		}
	}

	Collection<Object[]> getReqDataForRlnIds(Collection<Long> requirementIds, String requirementsSortOrder) {
		return execute(requirementDataQuery, REQUIREMENT_IDS, requirementIds, REQUIREMENTS_SORT_ORDER, requirementsSortOrder);
	}

	Collection<Object[]> getVersionIds(Collection<Long> requirementIds) {
		return execute(reqVersionsIds, REQUIREMENT_IDS, requirementIds);
	}

	Collection<Object[]> getPreviousVersionsData(Collection<Long> previousVersionIds) {
		return execute(previousVersionDataQuery, "previousVersionIds", previousVersionIds);
	}

	Collection<Object[]> getTestCasesDataForVersionIds(Collection<Long> requirementIds) {
		return execute(boundTCDataQuery, VERSION_IDS, requirementIds);
	}

	Collection<Object[]> getTestCasesDataForHLR(Collection<Long> nodeIds) {
		return execute(boundTCtoHLRDataQuery, HLR_IDS, nodeIds);
	}

	Collection<Object[]> getCufsDataForVersionIds(Collection<Long> versionIds) {
		return execute(cufsDataQuery, VERSION_IDS, versionIds);
	}

	Collection<Object[]> getAttachReqForReqIds(Collection<Long> requirementIds) {
		return execute(attachReqDataQuery, REQUIREMENT_IDS, requirementIds);
	}

	public Collection<Object[]> getTagCufsDataForVersionIds(Collection<Long> versionIds) {
		return execute(tagCufsDataQuery, VERSION_IDS, versionIds);
	}

	public Collection<Object[]> getReqLinkDataQueryForVersionIds(Collection<Long> versionIds) {
		return execute(reqLinkDataQuery, VERSION_IDS, versionIds);
	}

	public Collection<Object[]> getReqLinkedToHlrForHlrIds(Collection<Long> hlrIds) {
		return execute(reqLinkedToHlr, HLR_IDS, hlrIds);
	}

	String getMilestoneLabel(Integer milestoneId) {
		Map<String, Object> params = new HashMap<>();
		params.put("milestoneId", milestoneId);
		return runner.executeUniqueSelect(SQL_FIND_MILESTONE_LABEL, params);
	}

	Collection<Object[]> getMilestoneConfigurationQuery() {
		return runner.executeSelect(milestoneConfigurationQuery);
	}
	// ************************** utils **************************************

	private Collection<Object[]> execute(String query, String paramName, Collection<Long> ids) {

		if (ids.isEmpty()) {
			return Collections.emptyList();
		}

		Map<String, Collection<Long>> params = new HashMap<>(1);

		params.put(paramName, ids);

		return runner.executeSelect(query, params);
	}

	private Collection<Object[]> execute(String query, String idsParamName, Collection<Long> ids, String requirementsSortOrderParamName, String requirementsSortOrder){

		if (ids.isEmpty()){
			return Collections.emptyList();
		}
		Map <String, Object> params = new HashMap<>(2);

		params.put(idsParamName,ids);
		params.put(requirementsSortOrderParamName,Collections.singleton(requirementsSortOrder));

		return runner.executeSelect(query, params);
	}

	@SuppressWarnings("unchecked")
	protected Collection<Long> toIdList(Collection<?> ids) {
		return CollectionUtils.collect(ids, new IdTransformer());
	}



	// dirty impl
	private static class IdTransformer implements Transformer {
		public Object transform(Object arg0) {
			Class<?> argClass = arg0.getClass();

			if (argClass.equals(String.class)) {
				return Long.valueOf((String) arg0);
			}

			else if (argClass.equals(BigInteger.class)) {
				return ((BigInteger) arg0).longValue();
			}

			else if (argClass.equals(Integer.class)) {
				return ((Integer) arg0).longValue();
			} else {
				throw new RuntimeException("bug : IdTransformer cannto convert items of class " + argClass.getName());
			}
		}
	}

}
