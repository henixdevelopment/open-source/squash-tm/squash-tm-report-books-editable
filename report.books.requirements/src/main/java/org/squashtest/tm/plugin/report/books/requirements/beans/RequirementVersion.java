/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.requirements.beans;

import org.apache.commons.lang3.builder.CompareToBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RequirementVersion extends RequirementStructure {

	private Long versionNumber;
	private String status;
	private String criticality;
	private String category;
	private String categoryType;
	private String nature;
	private String createdBy;
	private Date createdOn;
	private String modifyBy;
	private Date modifyOn;
	private Long attachments;
	private String description;
	private Long reqId;
	private List<Cuf> cufs = new ArrayList<>();
	private List<TestCase> testCases;
	private List<ReqLink> reqLinks;

	private static final I18nHelper translator = new I18nHelper();

	public boolean isModify() {
		return !modifyBy.isEmpty();
	}

	public boolean isHasTestCases() { return testCases != null;}
	public boolean isHasReqLinks() { return reqLinks != null;}
	public boolean isHasCufs() { return !(cufs.isEmpty());}

	public Long getVersionNumber() {
		return versionNumber;
	}

	public void setVersionNumber(Long versionNumber) {
		this.versionNumber = versionNumber;
	}

	public String getStatus() {
		return translator.getRequirementStatus(status);
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCriticality() {
		return translator.getRequirementCriticality(criticality);
	}

	public void setCriticality(String criticality) {
		this.criticality = criticality;
	}

	public String getCategory() {
		return translator.getRequirementCategory(category, categoryType);
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}

	public String getNature() {
		return nature;
	}

	public void setNature(boolean isHlr) {
		if (isHlr) {
			this.nature = I18nHelper.translate("report.books.requirements.nature.highLevel");
		} else {
			this.nature = I18nHelper.translate("report.books.requirements.nature.classic");
		}
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		return I18nHelper.translate(createdOn);
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(String modifyBy) {
		this.modifyBy = modifyBy;
	}

	public String getModifyOn() {
		return I18nHelper.translate(modifyOn);
	}

	public void setModifyOn(Date modifyOn) {
		this.modifyOn = modifyOn;
	}

	public Long getAttachments() {
		return attachments;
	}

	public void setAttachments(Long attachments) {
		this.attachments = attachments;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) { this.description = description; }

	public Long getReqId() {
		return reqId;
	}

	public void setReqId(Long reqId) {
		this.reqId = reqId;
	}

	public List<Cuf> getCufs() {
		return cufs;
	}

	public List<TestCase> getTestCases() {
		return testCases;
	}

	public void setTestCases(List<TestCase> testCases) {
		this.testCases = testCases;
	}

	public List<ReqLink> getReqLinks() {
		return reqLinks;
	}

	public void setReqLinks(List<ReqLink> reqLinks) {
		this.reqLinks = reqLinks;
	}

	public void orderTestCases() {
		if(this.isHasTestCases()){
			this.testCases.sort((tc1, tc2) -> new CompareToBuilder()
				.append(tc1.getImportanceLevel(), tc2.getImportanceLevel())
				.append(tc1.getProjectName(), tc2.getProjectName())
				.append(tc1.getTcRef(), tc2.getTcRef())
				.append(tc1.getTcName(), tc2.getTcName())
				.toComparison());
		}
	}
}
