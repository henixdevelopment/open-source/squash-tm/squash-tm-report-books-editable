/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.requirements.query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.Resource;
import org.squashtest.tm.api.report.criteria.Criteria;
import org.squashtest.tm.api.report.query.ReportQuery;
import org.squashtest.tm.api.repository.SqlQueryRunner;
import org.squashtest.tm.api.utils.CurrentUserHelper;
import org.squashtest.tm.plugin.report.books.requirements.beans.AttachReq;
import org.squashtest.tm.plugin.report.books.requirements.beans.Cuf;
import org.squashtest.tm.plugin.report.books.requirements.beans.Data;
import org.squashtest.tm.plugin.report.books.requirements.beans.LowReq;
import org.squashtest.tm.plugin.report.books.requirements.beans.PrintOptions;
import org.squashtest.tm.plugin.report.books.requirements.beans.Project;
import org.squashtest.tm.plugin.report.books.requirements.beans.ReqLink;
import org.squashtest.tm.plugin.report.books.requirements.beans.Requirement;
import org.squashtest.tm.plugin.report.books.requirements.beans.RequirementVersion;
import org.squashtest.tm.plugin.report.books.requirements.beans.TestCase;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collectors;

public class RequirementsTreeQuery implements ReportQuery, InitializingBean {
	public static final Logger LOGGER = LoggerFactory.getLogger(RequirementsTreeQuery.class);
	private static final Map<Resource, String> cache = new HashMap<>();
	private static final String MILESTONES = "milestones";
	private static final String REQUIREMENTS_IDS = "requirementsIds";
	private static final String PROJECT_IDS = "projectIds";
	private static final String REQUIREMENTS_SELECTION_MODE = "requirementsSelectionMode";
	private static final String REPORT_OPTIONS = "reportOptions";
	private static final String REQUIREMENTS_SORT_ORDER = "requirementsSortOrder";
    private static final String TEMPLATE_FILE_NAME = "templateFileName";
	private static final String PRINT_ONLY_LAST_VERSION = "printOnlyLastVersion";
	private static final String PRINT_FOLDER_INFO = "printFolderInfo";
	private static final String PRINT_TEST_CASE = "printTestCase";
	private static final String PRINT_LINKED_REQ = "printLinkedReq";

	private static final String REQUIREMENT_FOLDERS = "requirement-folders";
	private static final String REQUIREMENTS = "requirements";
	private static final String HIGH_LEVEL_REQUIREMENTS = "high-level-requirements";
	private static final Object TAGS = "tags";
	private static final String MILESTONE_PICKER = "MILESTONE_PICKER";
    private static final String DEFAULT_SORT_ORDER = "ALPHABETICAL";
	private static final String DEFAULT_TEMPLATE_VALUE = "defaultTemplate";

	private final RequirementTreeQueryFinder queryFinder = new RequirementTreeQueryFinder();
	private RequirementTreeQueryFormatter formatter = new RequirementTreeQueryFormatter();

	protected SqlQueryRunner runner;

	private CurrentUserHelper currentUserHelper;

	public void setRunner(SqlQueryRunner runner) {
		this.runner = runner;
	}

	public void setFormater(RequirementTreeQueryFormatter formatter) {
		this.formatter = formatter;
	}

	public void setCurrentUserHelper(CurrentUserHelper currentUserHelper) {
		this.currentUserHelper = currentUserHelper;
	}

	public void afterPropertiesSet() throws Exception {
		queryFinder.setRunner(runner);
	}

	public void executeQuery(Map<String, Criteria> crit, Map<String, Object> res) {
		List<String> html = new ArrayList<>();
		Map<String, Boolean> optionsMap = new HashMap<>();

		Criteria options = crit.get(REPORT_OPTIONS);
		optionsMap.put(PRINT_ONLY_LAST_VERSION, false);
		optionsMap.put(PRINT_FOLDER_INFO, false);
		optionsMap.put(PRINT_TEST_CASE, false);
		optionsMap.put(PRINT_LINKED_REQ, false);

		Collection<String> selectedOptions = (Collection<String>) options.getValue();
		for (String option : selectedOptions) {
			optionsMap.put(option, true);
		}

        String requirementsSortOrder = Objects.nonNull(crit.get(REQUIREMENTS_SORT_ORDER))
				? (String) crit.get(REQUIREMENTS_SORT_ORDER).getValue()
				: DEFAULT_SORT_ORDER;
		String templateFileName = retrieveTemplateFileName(crit.get(TEMPLATE_FILE_NAME));

		Boolean printOnlyLastVersion = optionsMap.get(PRINT_ONLY_LAST_VERSION);
		Boolean printFolderInfo = optionsMap.get(PRINT_FOLDER_INFO);
		Boolean printTestCase = optionsMap.get(PRINT_TEST_CASE);
		Boolean printLinkedReq = optionsMap.get(PRINT_LINKED_REQ);

		Collection<RequirementVersion> previousVersionsData = new ArrayList<>();

		// get the target node ids
		Collection<Long> nodeIds = getNodeIdList(crit);

		// get the projects and req model
		Collection<Object[]> rawProjectsData = getRawProjectsData(crit);
		Collection<Object[]> rawReqsData = queryFinder.getReqDataForRlnIds(nodeIds,requirementsSortOrder);

		//get milestone configuration
		Collection<Object[]> rawConfigurationData = queryFinder.getMilestoneConfigurationQuery();
		boolean printMilestones = formatter.rawMilestoneConfigToBoolean(rawConfigurationData);

		// make dto of them
		Collection<Project> projectDataList = formatter.toProjectData(rawProjectsData);
		Collection<Requirement> reqsDataList = formatter.toRequirementData(rawReqsData, html, printMilestones);

		Collection<Requirement> hlrDataList = reqsDataList.stream().filter(Requirement::isHlr).collect(Collectors.toList());

		Collection<Long> versionAndFolderIds = getCurrentVersionAndFolderIds(reqsDataList);
		Collection<RequirementVersion> allVersions = new ArrayList<>();
		extractVersions(reqsDataList, allVersions);

		//get all previous versions
		if (Boolean.FALSE.equals(printOnlyLastVersion)) {
			Collection<Object[]> rawPreviousVersionIds = queryFinder.getVersionIds(nodeIds);
			Collection<Long> previousVersionIds = formatter.getPreviousVersionIds(rawPreviousVersionIds);
			versionAndFolderIds.addAll(previousVersionIds);
			Collection<Object[]> rawVersionsData = queryFinder.getPreviousVersionsData(previousVersionIds);
			previousVersionsData = formatter.toRequirementVersionData(rawVersionsData, html);
			allVersions.addAll(previousVersionsData);
		}

		//get TC, cufs.. for versions
		Collection<Object[]> rawCufs = queryFinder.getCufsDataForVersionIds(versionAndFolderIds);
		Collection<Object[]> rawTagCuf = queryFinder.getTagCufsDataForVersionIds(versionAndFolderIds);


		Map<Long, List<Cuf>> cufDataMap = formatter.toCufDataMap(rawCufs, html);
		Map<Long, List<Cuf>> tagCufDataMap = formatter.toCufDataMap(rawTagCuf, html);


		formatter.bindCufsToVersions(allVersions, cufDataMap);
		formatter.bindCufsToVersions(allVersions, tagCufDataMap);

		if (Boolean.TRUE.equals(printTestCase)) {
			Collection<Object[]> rawTestCase = queryFinder.getTestCasesDataForVersionIds(versionAndFolderIds);
			Map<Long, List<TestCase>> testCasesDataMap = formatter.toTestCaseDataMap(rawTestCase);
			formatter.bindTcToVersions(allVersions, testCasesDataMap);
		}
		if (Boolean.TRUE.equals(printLinkedReq)) {
			Collection<Object[]> rawAttachReq = queryFinder.getAttachReqForReqIds(nodeIds);
			Collection<Object[]> rawReqLinkedToHlr = queryFinder.getReqLinkedToHlrForHlrIds(nodeIds);
			Collection<Object[]> rawReqLink = queryFinder.getReqLinkDataQueryForVersionIds(versionAndFolderIds);

			Map<Long, List<AttachReq>> attachReqDataMap = formatter.toAttachReqDataMap(rawAttachReq);
			Map<Long, List<LowReq>> reqLinkedToHlrDataMap = formatter.toReqLinkedToHlrDataMap(rawReqLinkedToHlr);
			Map<Long, List<ReqLink>> reqLinkDataMap = formatter.toReqLinkDataMap(rawReqLink);

			formatter.bindAttachReqToReq(reqsDataList, attachReqDataMap);
			formatter.bindLowReqToHlr(hlrDataList, reqLinkedToHlrDataMap);
			formatter.bindReqLinkToVersions(allVersions, reqLinkDataMap);
		}
		if (Boolean.FALSE.equals(printOnlyLastVersion)) {
			formatter.bindVersionsToReq(reqsDataList, previousVersionsData);
		}

		if (Boolean.TRUE.equals(printTestCase)) {
			Collection<Object[]> rawTestCaseToHLR = queryFinder.getTestCasesDataForHLR(nodeIds);
			Map<Long, List<TestCase>> testCasesToHLRDataMap = formatter.toTestCaseDataMap(rawTestCaseToHLR);
			formatter.bindIndirectTestCaseToAllHighLevelRequirementVersions(hlrDataList, testCasesToHLRDataMap);
		}


		formatter.bindReqToProject(projectDataList, reqsDataList);

		// special milestone mode
		String milestoneLabel = null;
		if (isMilestonePicker(crit)) {
			List<Integer> milestoneIds = (List<Integer>) crit.get(MILESTONES).getValue();
			milestoneLabel = queryFinder.getMilestoneLabel(milestoneIds.get(0));
			res.put("milestoneLabel", milestoneLabel);
		}

		PrintOptions printOptions = new PrintOptions(requirementsSortOrder, templateFileName, printOnlyLastVersion, printFolderInfo, printTestCase, printLinkedReq);
		Data data = new Data(projectDataList, milestoneLabel, printOptions);
		res.put("fileName", data.getFileTitleRequirementsReport());
		res.put("data", data);
		res.put("html", html);
	}

	void extractVersions(Collection<Requirement> reqsDataList, Collection<RequirementVersion> allVersions) {
		for (Requirement req : reqsDataList) {
			allVersions.addAll(req.getVersions());
		}
	}

	Collection<Long> getCurrentVersionAndFolderIds(Collection<Requirement> reqsDataList) {
		return reqsDataList.stream().map(Requirement::getCurrentVersionId).collect(Collectors.toList());
	}

	// ******************** query finder configuration ********************

	public void setIdsByProjectQuery(Resource idsByProjectQuery) {
		String query = loadQuery(idsByProjectQuery);
		queryFinder.setIdsByProjectQuery(query);
	}

	public void setIdsBySelectionQuery(Resource idsBySelectionQuery) {
		String query = loadQuery(idsBySelectionQuery);
		queryFinder.setIdsBySelectionQuery(query);
	}

	public void setIdsByTagQuery(Resource idsByTagQuery) {
		String query = loadQuery(idsByTagQuery);
		queryFinder.setIdsByTagQuery(query);
	}

	public void setIdsByMilestoneQuery(Resource idsByMilestoneQuery) {
		String query = loadQuery(idsByMilestoneQuery);
		queryFinder.setIdsByMilestoneQuery(query);
	}

	public void setRequirementVersionDataQuery(Resource requirementVersionDataQuery) {
		String query = loadQuery(requirementVersionDataQuery);
		queryFinder.setRequirementDataQuery(query);
	}


	public void setBoundTCDataQuery(Resource boundTCDataQuery) {
		String query = loadQuery(boundTCDataQuery);
		queryFinder.setBoundTCDataQuery(query);
	}

	public void setCufsDataQuery(Resource cufsDataQuery) {
		String query = loadQuery(cufsDataQuery);
		queryFinder.setCufsDataQuery(query);
	}

	public void setAttachReqDataQuery(Resource attachReqDataQuery) {
		String query = loadQuery(attachReqDataQuery);
		queryFinder.setAttachReqDataQuery(query);
	}

	public void setTagCufsDataQuery(Resource tagCufsDataQuery) {
		String query = loadQuery(tagCufsDataQuery);
		queryFinder.setTagCufsDataQuery(query);
	}

	public void setReqLinkDataQuery(Resource reqLinkDataQuery) {
		String query = loadQuery(reqLinkDataQuery);
		queryFinder.setReqLinkDataQuery(query);
	}

	public void setAllProjectDataQuery(Resource allProjectDataQuery) {
		String query = loadQuery(allProjectDataQuery);
		queryFinder.setAllProjects(query);
	}

	public void setProjectsByMilestone(Resource projectsByMilestone) {
		String query = loadQuery(projectsByMilestone);
		queryFinder.setProjectsByMilestone(query);
	}

	public void setProjectsByTag(Resource projectsByTag) {
		String query = loadQuery(projectsByTag);
		queryFinder.setProjectsByTags(query);
	}

	public void setProjectsBySelection(Resource projectsBySelection) {
		String query = loadQuery(projectsBySelection);
		queryFinder.setProjectsBySelection(query);
	}

	public void setReqVersionIds(Resource reqVersionIds) {
		String query = loadQuery(reqVersionIds);
		queryFinder.setReqVersionsIds(query);
	}

	public void setReqVersionsData(Resource reqVersionsData) {
		String query = loadQuery(reqVersionsData);
		queryFinder.setPreviousVersionDataQuery(query);
	}

	public void setReqLinkedToHlr(Resource reqLinkedToHlr) {
		String query = loadQuery(reqLinkedToHlr);
		queryFinder.setReqLinkedToHlr(query);
	}


	public void setBoundTCtoHLRDataQuery(Resource boundTCtoHLRDataQuery) {
		String query = loadQuery(boundTCtoHLRDataQuery);
		queryFinder.setBoundTCtoHLRDataQuery(query);
	}

	public void setMilestoneConfigurationQuery(Resource milestoneConfigurationQuery) {
		String query = loadQuery(milestoneConfigurationQuery);
		queryFinder.setMilestoneConfigurationQuery(query);
	}

	protected SqlQueryRunner getRunner() {
		return runner;
	}

	// ********************* private stuffs ***************************

	@SuppressWarnings("unchecked")
	private Collection<Object[]> getRawProjectsData(Map<String, Criteria> criteriaMap) {

		Criteria selectionMode = criteriaMap.get(REQUIREMENTS_SELECTION_MODE);
		Collection<Object[]> rawProjects;

		if ("PROJECT_PICKER".equals(selectionMode.getValue())) {
			Criteria criteria = criteriaMap.get(PROJECT_IDS);
			Collection<String> projectIds = (Collection<String>) criteria.getValue();
			rawProjects = queryFinder.findProjects(projectIds);
		} else if (MILESTONE_PICKER.equals(selectionMode.getValue())) {
			List<String> milestoneIds = (List<String>) criteriaMap.get(MILESTONES).getValue();
			rawProjects = queryFinder.findProjectsByMilestone(milestoneIds,
				currentUserHelper.findFilteredReadableProjectIds());
		} else if ("TAG_PICKER".equals(selectionMode.getValue())) {
			List<String> tags = (List<String>) criteriaMap.get(TAGS).getValue();
			rawProjects = queryFinder.findProjectsByTags(tags, currentUserHelper.findFilteredReadableProjectIds());
		} else {
			rawProjects = queryFinder.findProjectsBySelection(getAllNodesIds(criteriaMap));
		}
		return rawProjects;
	}

	private Collection<Long> getNodeIdList(Map<String, Criteria> criteriaMap) {

		Criteria selectionMode = criteriaMap.get(REQUIREMENTS_SELECTION_MODE);

		Collection<Long> requirementIdList;
        List<Long> readableProjectIds = currentUserHelper.findReadableProjectIds();
		if ("PROJECT_PICKER".equals(selectionMode.getValue())) {
            List<Long> projectIds = ((Collection<String>) criteriaMap.get(PROJECT_IDS).getValue())
                .stream()
                .map(Long::valueOf)
                .toList();
            readableProjectIds.retainAll(projectIds);
			requirementIdList = queryFinder.findIdsByProject(readableProjectIds);
		} else if (MILESTONE_PICKER.equals(selectionMode.getValue())) {
			List<String> milestoneIds = (List<String>) criteriaMap.get(MILESTONES).getValue();
			requirementIdList = queryFinder.findIdsByMilestone(milestoneIds, readableProjectIds);
		} else if ("TAG_PICKER".equals(selectionMode.getValue())) {
			List<String> tags = (List<String>) criteriaMap.get(TAGS).getValue();
			requirementIdList = queryFinder.findIdsByTags(tags, readableProjectIds);
		} else {
			requirementIdList = queryFinder.findIdsBySelection(getAllNodesIds(criteriaMap), readableProjectIds);
		}

		return requirementIdList;
	}

	private Collection<String> getAllNodesIds(Map<String, Criteria> criteriaMap) {
		Criteria criteria = criteriaMap.get(REQUIREMENTS_IDS);
		Map<String, Collection<?>> selectedIds = (Map<String, Collection<?>>) (criteria.getValue());

		Collection<String> allStrIds = (Collection<String>) selectedIds.get(REQUIREMENTS);
		if (allStrIds == null) {
			allStrIds = new ArrayList<>();
		}
		Collection<String> highLvlReqIds = (Collection<String>) selectedIds.get(HIGH_LEVEL_REQUIREMENTS);
		if (highLvlReqIds != null && !highLvlReqIds.isEmpty()) {
			allStrIds.addAll(highLvlReqIds);
		}
		Collection<String> folderIds = (Collection<String>) selectedIds.get(REQUIREMENT_FOLDERS);
		if (folderIds == null) {
			folderIds = new ArrayList<>();
		}

		allStrIds.addAll(folderIds);
		return allStrIds;
	}

	private boolean isMilestonePicker(Map<String, Criteria> criteriaMap) {
		Criteria selectionMode = criteriaMap.get(REQUIREMENTS_SELECTION_MODE);

		return MILESTONE_PICKER.equals(selectionMode.getValue());
	}

	protected static String loadQuery(Resource query) {
		if (cache.get(query) == null) {
			InputStream is;
			try {
				is = query.getInputStream();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			Scanner scan = new Scanner(is, "UTF-8");
			String value = scan.useDelimiter("\\A").next();
			scan.close();
			cache.put(query, value);
		}
		return cache.get(query);
	}

	private String retrieveTemplateFileName(Criteria templateCriteria) {
		if (Objects.nonNull(templateCriteria)) {
			try {
				return (String) templateCriteria.getValue();
			} catch (ClassCastException cce) {
				// When file does not exist anymore, it returns an EmptyCriteria which does not have any value attribute.
				LOGGER.trace("Custom template file does not exist anymore.", cce);
				// then we put the default template instead.
			}
		}
		return DEFAULT_TEMPLATE_VALUE;
	}
}
