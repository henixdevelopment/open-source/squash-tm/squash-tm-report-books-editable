/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.requirements.beans;

import org.apache.commons.lang3.StringUtils;

public class TextFormatter {

	private static final String TEXT_SEPARATOR = "=Sep=,";
	private static final String END_TEXT_SEPARATOR = "=Sep=";

	private static final String TAG_SEPARATOR = " | ";
	private static final String MILESTONE_SEPARATOR = ", ";

	public static String formatMilestones(String milestones) {
		if (milestones != null) {
			milestones = milestones.replace(TEXT_SEPARATOR, MILESTONE_SEPARATOR);
			return milestones.replace(END_TEXT_SEPARATOR, "");
		} else {
			return StringUtils.EMPTY;
		}
	}

	public static String formatTagCuf(String tagCuf) {
		if (tagCuf != null) {
			tagCuf = tagCuf.replace(TEXT_SEPARATOR, TAG_SEPARATOR);
			return tagCuf.replace(END_TEXT_SEPARATOR, "");
		} else {
			return StringUtils.EMPTY;
		}
	}
}
