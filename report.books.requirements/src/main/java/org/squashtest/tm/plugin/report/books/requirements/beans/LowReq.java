/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.requirements.beans;

public class LowReq extends RequirementStructure{

	private String criticality;
	private String project;
	private String status;

	private static final I18nHelper translator = new I18nHelper();

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getCriticality() {
		return translator.getRequirementCriticality(criticality);
	}

	public void setCriticality(String criticality) {
		this.criticality = criticality;
	}

	public String getStatus() {
		return translator.getRequirementStatus(status);
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
