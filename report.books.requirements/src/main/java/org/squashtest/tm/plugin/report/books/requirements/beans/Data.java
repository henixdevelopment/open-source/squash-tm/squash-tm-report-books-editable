/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.requirements.beans;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

public class Data {


	private static final String MILESTONE_TITLE = "report.books.requirements.requirement.title.milestone";
	private static final String GENERATED_TITLE = "report.books.requirements.generated.title";
	private static final String GENERATED_DATE = "report.books.requirements.dateformat";
	private static final String GENERATED_HOUR = "report.books.requirements.hourformat";

	private static final String LOW_REQ_REQUIREMENTS_TITLE = "report.books.requirements.title.lowReq";
	private static final String LINK_TEST_CASE_REQUIREMENTS_TITLE = "report.books.requirements.title.linkTestCase";
	private static final String ATTACH_REQ_REQUIREMENTS_TITLE = "report.books.requirements.title.attachReq";
	private static final String LINKED_REQ_REQUIREMENTS_TITLE = "report.books.requirements.title.linkedReq";

	private static final String SUMMARY = "report.books.requirements.summary";
	private static final String SUMMARY_UPDATE = "report.books.requirements.summary.update";

	private static final String ATTACHMENTS_LABEL = "report.books.requirements.label.attachments";
	private static final String HLR_LABEL = "report.books.requirements.label.hlr";
	private static final String CATEGORY_LABEL = "report.books.requirements.label.category";
	private static final String CREATION_LABEL = "report.books.requirements.label.creation";
	private static final String BY_LABEL = "report.books.requirements.label.by";
	private static final String ON_LABEL = "report.books.requirements.label.on";
	private static final String REQUIREMENT_ID_LABEL = "report.books.requirements.label.requirement.id";
	private static final String NATURE_LABEL = "report.books.requirements.label.nature";
	private static final String MODIFICATION_LABEL = "report.books.requirements.label.modification";
	private static final String STATUS_LABEL = "report.books.requirements.label.status";
	private static final String VERSION_NUMBER_LABEL = "report.books.requirements.label.version.number";
	private static final String VERSION_ID_LABEL = "report.books.requirements.label.version.id";
	private static final String CRITICALITY_LABEL = "report.books.requirements.label.criticality";
	private static final String IMPORTANCE_LABEL = "report.books.requirements.label.importance";
	private static final String DESCRIPTION_LABEL = "report.books.requirements.label.description";
	private static final String NAME_LABEL = "report.books.requirements.label.name";
	private static final String REFERENCE_LABEL = "report.books.requirements.label.reference";
	private static final String REQUIREMENTS_REPORT_LABEL = "report.books.requirements.label.requirementsReport";
	private static final String MOTHER_REQ_LABEL = "report.books.requirements.label.parentReq";
	private static final String DAUGHTER_REQ_LABEL = "report.books.requirements.label.childrenReq";
	private static final String MILESTONES_LABEL = "report.books.requirements.label.milestones";
	private static final String PROJECT_LABEL = "report.books.requirements.label.project";
	private static final String ROLE_LABEL = "report.books.requirements.label.role";

	private static final String VIEW_TITLE_REQUIREMENTS_REPORT = "report.books.requirements.view.title.requirements.report";
	private static final String VIEW_TITLE_DATEFORMAT = "report.books.requirements.view.title.dateformat";

	private static final String GENRERATED_LABEL = "report.books.requirements.footer.generated_label";
	private static final String DATE_FOOTER = "report.books.requirements.footer.date";

	private final Collection<Project> projects;
	private final String milestoneName;
	private final String requirementsSortOrder;
    private final String templateFileName;
	private final boolean printOnlyLastVersion;
	private final boolean printFolderInfo;
	private final boolean printTestCase;
	private final boolean printLinkedReq;

	public Data(Collection<Project> projects, String milestoneName, PrintOptions printOptions) {
		this.projects = projects;
		this.milestoneName = milestoneName;
		this.requirementsSortOrder = printOptions.requirementsSortOrder();
		this.printOnlyLastVersion = printOptions.printOnlyLastVersion();
		this.printFolderInfo = printOptions.printFolderInfo();
		this.printTestCase = printOptions.printTestCase();
		this.printLinkedReq = printOptions.printLinkedReq();
        this.templateFileName = printOptions.templateFileName();
	}

	public boolean isPrintOnlyLastVersion() {
		return printOnlyLastVersion;
	}
	public boolean isPrintFolderInfo() {
		return printFolderInfo;
	}
	public boolean isPrintTestCase() {
		return printTestCase;
	}
	public boolean isPrintLinkedReq() {
		return printLinkedReq;
	}

	//HEADER
	public String getRequirementsReportLabel() {
		return I18nHelper.translate(REQUIREMENTS_REPORT_LABEL);
	}

	//FIRST PAGE
	public String getViewTitleRequirementsReport() {
		return I18nHelper.translate(VIEW_TITLE_REQUIREMENTS_REPORT);
	}
	public String getGeneratedTitle() {
		Object[] params = new Object[2];
		params[0] = getGeneratedDate();
		params[1] = getGeneratedHour();
		return I18nHelper.translate(GENERATED_TITLE, params);
	}

	private String getGeneratedDate() {
		SimpleDateFormat sdfDate = new SimpleDateFormat(I18nHelper.translate(GENERATED_DATE));
		Calendar cal = Calendar.getInstance();
		return sdfDate.format(cal.getTime());
	}

	private String getGeneratedHour() {
		SimpleDateFormat sdfHour = new SimpleDateFormat(I18nHelper.translate(GENERATED_HOUR));
		Calendar cal = Calendar.getInstance();
		return sdfHour.format(cal.getTime());
	}
	public String getTitleMilestone() {
		return (milestoneName != null) ? I18nHelper.translate(MILESTONE_TITLE) + " " + milestoneName : "";
	}

	//SUMMARY
	public String getSummary() {
		return I18nHelper.translate(SUMMARY);
	}
	public String getSummaryUpdate() {
		return I18nHelper.translate(SUMMARY_UPDATE);
	}

	//TABLE TITLE
	public String getLowReqTitle() {
		return I18nHelper.translate(LOW_REQ_REQUIREMENTS_TITLE);
	}
	public String getTestCaseLinkTitle() {
		return I18nHelper.translate(LINK_TEST_CASE_REQUIREMENTS_TITLE);
	}
	public String getAttachReqTitle() {
		return I18nHelper.translate(ATTACH_REQ_REQUIREMENTS_TITLE);
	}
	public String getLinkedReqTitle() {
		return I18nHelper.translate(LINKED_REQ_REQUIREMENTS_TITLE);
	}

	//LABELS
	public String getLabelDescription() {
		return I18nHelper.translate(DESCRIPTION_LABEL);
	}
	public String getLabelId() {
		return I18nHelper.translate(REQUIREMENT_ID_LABEL);
	}
	public String getLabelVersion() {
		return I18nHelper.translate(VERSION_NUMBER_LABEL);
	}
	public String getLabelVersionId() {
		return I18nHelper.translate(VERSION_ID_LABEL);
	}
	public String getLabelStatus() {
		return I18nHelper.translate(STATUS_LABEL);
	}
	public String getLabelCriticality() {
		return I18nHelper.translate(CRITICALITY_LABEL);
	}
	public String getLabelCreation() {
		return I18nHelper.translate(CREATION_LABEL);
	}
	public String getLabelBy() {
		return I18nHelper.translate(BY_LABEL);
	}
	public String getLabelOn() {
		return I18nHelper.translate(ON_LABEL);
	}
	public String getLabelCategory() {
		return I18nHelper.translate(CATEGORY_LABEL);
	}
	public String getLabelModification() {
		return I18nHelper.translate(MODIFICATION_LABEL);
	}
	public String getLabelNature() { return I18nHelper.translate(NATURE_LABEL);}
	public String getLabelAttachment() {
		return I18nHelper.translate(ATTACHMENTS_LABEL);
	}
	public String getLabelHlr() {
		return I18nHelper.translate(HLR_LABEL);
}
	public String getLabelMilestones() {
		return I18nHelper.translate(MILESTONES_LABEL);
	}
	public String getLabelProject() {
		return I18nHelper.translate(PROJECT_LABEL);
	}
	public String getLabelReference() {
		return I18nHelper.translate(REFERENCE_LABEL);
	}
	public String getLabelName() {
		return I18nHelper.translate(NAME_LABEL);
	}
	public String getLabelRole() {
		return I18nHelper.translate(ROLE_LABEL);
	}
	public String getLabelImportance() {
		return I18nHelper.translate(IMPORTANCE_LABEL);
	}
	public String getLabelMotherReq() {
		return I18nHelper.translate(MOTHER_REQ_LABEL);
	}
	public String getLabelDaughterReq() {
		return I18nHelper.translate(DAUGHTER_REQ_LABEL);
	}

	//FOOTER
	public String getGeneratedLabel() { return I18nHelper.translate(GENRERATED_LABEL);}
	public String getDateFooter() {
		Object[] params = new Object[1];
		params[0] = getGeneratedDate();
		return I18nHelper.translate(DATE_FOOTER, params);
	}

	//GET PROJECTS
	public Collection<Project> getProjects() {
		return projects;
	}

	public String getFileTitleRequirementsReport() {
		String title = I18nHelper.translate(VIEW_TITLE_REQUIREMENTS_REPORT);
		SimpleDateFormat formatter = new SimpleDateFormat(I18nHelper.translate(VIEW_TITLE_DATEFORMAT));
		String strDate = formatter.format(new Date());
		return title + "-" + strDate;
	}

	public String getRequirementsSortOrder() {
		return requirementsSortOrder;
	}

    public String getTemplateFileName() {
        return templateFileName;
    }
}

