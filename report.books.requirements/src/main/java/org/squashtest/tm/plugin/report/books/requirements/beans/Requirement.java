/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.requirements.beans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class Requirement {

	private Long reqId;
	private boolean isFolder;
	private boolean isHlr;
	private Long level;
	private Long projectId;
	private String projectName;
	private Long numberOfVersions;
	private String highLevelReqLabel;
	private List<RequirementVersion> versions = new ArrayList<>();
	private List<AttachReq> attachReqs;
	private List<LowReq> lowReqs;

	private String paragraph;

	private Long currentVersionId;
	private String currentVersionName;
	private String currentVersionRef;
	private boolean printMilestones;

	public boolean isHasHighLevelReq() {
		return !highLevelReqLabel.isEmpty();
	}

	public boolean isHasReference() {
		return !currentVersionRef.isEmpty();
	}

	public boolean isHasAttachReq() {
		return this.attachReqs != null;
	}
	public boolean isHasLowReq() {
		return this.lowReqs != null;
	}

	public String getParagraph() {
		return paragraph;
	}

	public void setParagraph(List<Integer> paragraphLevel) {
		paragraph = "";
		if (paragraphLevel != null && !paragraphLevel.isEmpty()) {
			Iterator<Integer> itr = paragraphLevel.iterator();
			String delimiter = "";
			while (itr.hasNext()) {
				paragraph = paragraph + delimiter + itr.next();
				delimiter = ".";
			}
		}
	}

	public Long getReqId() {
		return reqId;
	}

	public void setReqId(Long reqId) {
		this.reqId = reqId;
	}

	public String getHighLevelReqLabel() {
		return highLevelReqLabel;
	}

	public void setHighLevelReqLabel(String highLevelReqLabel) {
		this.highLevelReqLabel = highLevelReqLabel;
	}

	public boolean isFolder() {
		return isFolder;
	}

	public void setFolder(Long folder) {
		isFolder = (folder == 1L);
	}

	public boolean isHlr() {
		return isHlr;
	}

	public void setHlr(Long hlr) {
		this.isHlr = (hlr == 1L);
	}

	public Long getLevel() {
		return level;
	}

	public void setLevel(Long level) {
		this.level = level;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Long getNumberOfVersions() {
		return numberOfVersions;
	}

	public void setNumberOfVersions(Long numberOfVersions) {
		this.numberOfVersions = numberOfVersions;
	}

	public List<RequirementVersion> getVersions() {
		return versions;
	}

	public void setVersions(List<RequirementVersion> versions) {
		this.versions = versions;
	}

	public List<AttachReq> getAttachReqs() {
		return attachReqs;
	}

	public void setAttachReqs(List<AttachReq> attachReqs) {
		this.attachReqs = attachReqs;
	}

	public List<LowReq> getLowReqs() {
		return lowReqs;
	}

	public void setLowReqs(List<LowReq> lowReqs) {
		this.lowReqs = lowReqs;
	}

	public Long getCurrentVersionId() {
		return currentVersionId;
	}

	public void setCurrentVersionId(Long currentVersionId) {
		this.currentVersionId = currentVersionId;
	}

	public String getCurrentVersionName() {
		return currentVersionName;
	}

	public void setCurrentVersionName(String currentVersionName) {
		this.currentVersionName = currentVersionName;
	}

	public String getCurrentVersionRef() {
		return currentVersionRef;
	}

	public void setCurrentVersionRef(String currentVersionRef) {
		this.currentVersionRef = currentVersionRef;
	}

	public void addLastVersion(RequirementVersion version) {
		this.versions.add(version);
	}

	public void addPreviousVersions(Collection<RequirementVersion> previousVersions) {
		this.versions.addAll(previousVersions);
	}

	public void orderVersions() {
		this.versions.sort((v1, v2) -> (int) (v1.getVersionNumber() - v2.getVersionNumber()));
	}

	public boolean isPrintMilestones() { return printMilestones; }

	public void setPrintMilestones(boolean printMilestones) { this.printMilestones = printMilestones; }
}
