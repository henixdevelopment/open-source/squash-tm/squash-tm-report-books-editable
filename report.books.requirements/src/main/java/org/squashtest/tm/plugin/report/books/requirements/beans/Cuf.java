/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.requirements.beans;

public class Cuf {

	private String type;
	private String label;
	private String value;
	private String inputType;

	public String getInputType() {
		return inputType;
	}

	public void setInputType(String inputType) {
		this.inputType = inputType;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getValue() {
		if ("CHECKBOX".equals(inputType)) {
			if (Boolean.TRUE.toString().equals(value)) {
				return I18nHelper.translate("report.books.requirements.label.true");
			} else {
				return I18nHelper.translate("report.books.requirements.label.false");
			}
		} else if ("TAG".equals(type)) {
			return TextFormatter.formatTagCuf(value);
		} else {
			return value;
		}
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isRichText() {
		return "RTF".equals(type);
	}

}
