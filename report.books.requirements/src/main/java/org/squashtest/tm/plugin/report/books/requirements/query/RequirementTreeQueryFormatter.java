/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.requirements.query;

import org.apache.commons.lang3.StringUtils;
import org.squashtest.tm.api.utils.AttachmentImageHelper;
import org.squashtest.tm.plugin.report.books.requirements.beans.AttachReq;
import org.squashtest.tm.plugin.report.books.requirements.beans.Cuf;
import org.squashtest.tm.plugin.report.books.requirements.beans.I18nHelper;
import org.squashtest.tm.plugin.report.books.requirements.beans.LowReq;
import org.squashtest.tm.plugin.report.books.requirements.beans.Project;
import org.squashtest.tm.plugin.report.books.requirements.beans.ReqLink;
import org.squashtest.tm.plugin.report.books.requirements.beans.Requirement;
import org.squashtest.tm.plugin.report.books.requirements.beans.RequirementVersion;
import org.squashtest.tm.plugin.report.books.requirements.beans.TestCase;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class RequirementTreeQueryFormatter {

	private static final String REQLINK_RELATED = "requirement-version.link.type.related";
	private static final String REQLINK_PARENT = "requirement-version.link.type.parent";
	private static final String REQLINK_CHILD = "requirement-version.link.type.child";
	private static final String REQLINK_DUPLICATE = "requirement-version.link.type.duplicate";
    private static final String END_TEXT_SEPARATOR = "=Sep=";
	private static final String MILESTONE_SEPARATOR = ", ";
	private static final int MAX_IMAGE_SIZE = 700;

	private List<Integer> paragraphs;
	private String projectName;

	private AttachmentImageHelper attachmentImageHelper;


	public void setAttachmentImageHelper(AttachmentImageHelper attachmentImageHelper) {
		this.attachmentImageHelper = attachmentImageHelper;
	}

	// ************************ DTO making *****************************

	static <T> void addToMap(Long key, T data, Map<Long, List<T>> result) {
		if (result.get(key) == null) {
			result.put(key, new ArrayList<>(Collections.singletonList(data)));
		} else {
			result.get(key).add(data);
		}
	}

	Collection<Project> toProjectData(Collection<Object[]> tuples) {

		Konvertor konvertor = new Konvertor();
		Collection<Project> result = new ArrayList<>(tuples.size());

		for (Object[] array : tuples) {
			Project newData = new Project();

			Long id = konvertor.from(array[0]).toLong();
			String name = konvertor.from(array[1]).toString();

			newData.setProjectId(id);
			newData.setProjectName(name);

			result.add(newData);
		}
		return result;
	}

	Collection<Requirement> toRequirementData(Collection<Object[]> tuples, List<String> html, boolean printMilestones) {
		Konvertor konvertor = new Konvertor();
		Collection<Requirement> result = new LinkedList<>();

		for (Object[] array : tuples) {

			Requirement newData = new Requirement();
			RequirementVersion lastVersion = new RequirementVersion();

			newData.setReqId(konvertor.from(array[0]).toLong());
			newData.setFolder(konvertor.from(array[1]).toLong());
			newData.setHlr(konvertor.from(array[2]).toLong());
			newData.setLevel(konvertor.from(array[3]).toLong());
			newData.setProjectId(konvertor.from(array[5]).toLong());
			newData.setProjectName(konvertor.from(array[6]).toString());
			newData.setNumberOfVersions(konvertor.from(array[7]).toLong());
			newData.setHighLevelReqLabel(konvertor.from(array[8]).toString());
			newData.setPrintMilestones(printMilestones);
			if(newData.isFolder()) {
				newData.setCurrentVersionId(newData.getReqId());
			} else {
				newData.setCurrentVersionId(konvertor.from(array[9]).toLong());
			}
			newData.setCurrentVersionRef(konvertor.from(array[10]).toString());
			newData.setCurrentVersionName(konvertor.from(array[11]).toString());
			if (newData.isFolder()) {
				lastVersion.setVersionId(newData.getReqId());
			} else {
				lastVersion.setVersionId(konvertor.from(array[9]).toLong());
			}
			lastVersion.setReference(konvertor.from(array[10]).toString());
			lastVersion.setName(konvertor.from(array[11]).toString());
			lastVersion.setVersionNumber(konvertor.from(array[12]).toLong());
			lastVersion.setStatus(konvertor.from(array[13]).toString());
			lastVersion.setCriticality(konvertor.from(array[14]).toString());
			lastVersion.setCategory(konvertor.from(array[15]).toString());
			lastVersion.setCategoryType(konvertor.from(array[16]).toString());
			lastVersion.setCreatedBy(konvertor.from(array[17]).toString());
			lastVersion.setCreatedOn(konvertor.from(array[18]).toDate());
			lastVersion.setModifyBy(konvertor.from(array[19]).toString());
			lastVersion.setModifyOn(konvertor.from(array[20]).toDate());
			String milestones = konvertor.from(array[21]).toString();
			lastVersion.setMilestones(milestones.replace(",", MILESTONE_SEPARATOR));
			lastVersion.setAttachments(konvertor.from(array[22]).toLong());
			lastVersion.setDescription(konvertor.from(array[23]).toRichText(html));
			lastVersion.setNature(newData.isHlr());

			newData.addLastVersion(lastVersion);

			result.add(newData);
		}
		return result;
	}
	boolean rawMilestoneConfigToBoolean(Collection<Object[]> rawConfigurationData) {
		Konvertor konvertor = new Konvertor();
		if (rawConfigurationData.isEmpty()) {
			return false;
		} else if (rawConfigurationData.size() == 1) {
			Object[] array = rawConfigurationData.iterator().next();
			String value = konvertor.from(array[1]).toString();
			return "true".equalsIgnoreCase(value);
		} else {
			throw new IndexOutOfBoundsException("bug : sql query results exceed expected quantity, returned: "
				+ rawConfigurationData.size());
		}
	}
	Map<Long, List<TestCase>> toTestCaseDataMap(Collection<Object[]> tuples) {

		Konvertor konvertor = new Konvertor();
		Map<Long, List<TestCase>> result = new HashMap<>();

		for (Object[] array : tuples) {

			TestCase data = new TestCase();

			data.setTcId(konvertor.from(array[0]).toLong());
			data.setProjectName(konvertor.from(array[2]).toString());
			data.setTcRef(konvertor.from(array[3]).toString());
			data.setTcName(konvertor.from(array[4]).toString());
			data.setTcMilestone(konvertor.from(array[5]).toString());
			data.setTcStatus(konvertor.from(array[6]).toString());
			data.setTcImportance(konvertor.from(array[7]).toString());

			addToMap(konvertor.from(array[1]).toLong(), data, result);
		}

		return result;

	}

	Map<Long, List<AttachReq>> toAttachReqDataMap(Collection<Object[]> tuples) {
		Konvertor konvertor = new Konvertor();
		Map<Long, List<AttachReq>> result = new HashMap<>();

		for (Object[] array : tuples) {

			AttachReq data = new AttachReq();

			data.setReference(konvertor.from(array[2]).toString());
			data.setName(konvertor.from(array[3]).toString());
			data.setMilestones(konvertor.from(array[4]).toString());
			data.setCriticality(konvertor.from(array[5]).toString());
			data.setParent(konvertor.from(array[6]).toLong());

			addToMap(konvertor.from(array[0]).toLong(), data, result);
		}
		return result;
	}

	Map<Long, List<Cuf>> toCufDataMap(Collection<Object[]> tuples, List<String> html) {

		Konvertor konvertor = new Konvertor();
		Map<Long, List<Cuf>> result = new HashMap<>();

		for (Object[] array : tuples) {

			Cuf data = new Cuf();
			data.setType(konvertor.from(array[1]).toString());
			data.setLabel(konvertor.from(array[2]).toString());
			if ("RTF".equals(data.getType())) {
				data.setValue(konvertor.from(array[3]).toRichText(html));
			} else {
				data.setValue(konvertor.from(array[3]).toString());
			}
			data.setInputType(konvertor.from(array[4]).toString());

			addToMap(konvertor.from(array[0]).toLong(), data, result);
		}
		return result;
	}

	Map<Long, List<ReqLink>> toReqLinkDataMap(Collection<Object[]> tuples) {

		Konvertor konvertor = new Konvertor();
		Map<Long, List<ReqLink>> result = new HashMap<>();

		for (Object[] array : tuples) {

			ReqLink data = new ReqLink();

			Boolean direction = konvertor.from(array[1]).toBoolean();
			String role1 = konvertor.from(array[2]).toString();
			String role2 = konvertor.from(array[3]).toString();
			data.setProject(konvertor.from(array[4]).toString());
			data.setReference(konvertor.from(array[5]).toString());
			data.setName(konvertor.from(array[6]).toString());
			data.setVersionNumber(konvertor.from(array[7]).toLong());
			data.setMilestones(konvertor.from(array[8]).toString());

			if (role1.equals(REQLINK_RELATED) || role1.equals(REQLINK_PARENT) || role1.equals(REQLINK_CHILD) || role1.equals(REQLINK_DUPLICATE)
			) {
				role1 = I18nHelper.translate(role1);
				role2 = I18nHelper.translate(role2);
			}
			if (Boolean.TRUE.equals(direction)) {
				data.setRole(role1);
			} else {
				data.setRole(role2);
			}

			addToMap(konvertor.from(array[0]).toLong(), data, result);
		}

		return result;

	}

	Collection<RequirementVersion> toRequirementVersionData(Collection<Object[]> tuples, List<String> html) {

		Konvertor konvertor = new Konvertor();
		Collection<RequirementVersion> result = new LinkedList<>();

		for (Object[] array : tuples) {

			RequirementVersion version = new RequirementVersion();

			version.setVersionId(konvertor.from(array[0]).toLong());
			version.setReference(konvertor.from(array[1]).toString());
			version.setName(konvertor.from(array[2]).toString());
			version.setVersionNumber(konvertor.from(array[3]).toLong());
			version.setStatus(konvertor.from(array[4]).toString());
			version.setCriticality(konvertor.from(array[5]).toString());
			version.setCategory(konvertor.from(array[6]).toString());
			version.setCategoryType(konvertor.from(array[7]).toString());
			version.setCreatedBy(konvertor.from(array[8]).toString());
			version.setCreatedOn(konvertor.from(array[9]).toDate());
			version.setModifyBy(konvertor.from(array[10]).toString());
			version.setModifyOn(konvertor.from(array[11]).toDate());
			version.setMilestones(konvertor.from(array[12]).toString());
			version.setAttachments(konvertor.from(array[13]).toLong());
			version.setDescription(konvertor.from(array[14]).toRichText(html));
			version.setReqId(konvertor.from(array[15]).toLong());

			result.add(version);
		}
		return result;
	}

	public Map<Long, List<LowReq>> toReqLinkedToHlrDataMap(Collection<Object[]> tuples) {

		Konvertor konvertor = new Konvertor();
		Map<Long, List<LowReq>> result = new HashMap<>();

		for (Object[] array : tuples) {

			LowReq data = new LowReq();

			data.setVersionId(konvertor.from(array[1]).toLong());
			data.setProject(konvertor.from(array[2]).toString());
			data.setReference(konvertor.from(array[3]).toString());
			data.setName(konvertor.from(array[4]).toString());
			data.setMilestones(konvertor.from(array[5]).toString());
			data.setCriticality(konvertor.from(array[6]).toString());
			data.setStatus(konvertor.from(array[7]).toString());

			addToMap(konvertor.from(array[0]).toLong(), data, result);
		}
		return result;
	}

	// **************************** DTO binding ***************************


	public void bindReqToProject(Collection<Project> projectDataList, Collection<Requirement> reqDataList) {
		if (!projectDataList.isEmpty()) {
			paragraphs = new ArrayList<>();
			projectName = Objects.requireNonNull(projectDataList.stream().findFirst().orElse(null)).getProjectName();
			for (Project project : projectDataList) {
				project.setRequirements(groupReqForProject(project.getProjectId(), reqDataList));
			}
		}
	}

	private Collection<Requirement> groupReqForProject(Long projectId, Collection<Requirement> requirementCollection) {
		Collection<Requirement> requirements = new ArrayList<>();
		for (Requirement req : requirementCollection) {
			if (req.getProjectId().equals(projectId)) {
				if (!(projectName.equals(req.getProjectName()))) {
					paragraphs = new ArrayList<>();
					projectName = req.getProjectName();
				}
				changeParagraph(req);
				req.setParagraph(paragraphs);
				requirements.add(req);
			}
		}
		return requirements;
	}

	private void changeParagraph(Requirement requirement) {
		int level = requirement.getLevel().intValue();
		if (paragraphs.isEmpty()) {
			paragraphs.add(1);
		} else {
			if (paragraphs.size() == level) {
				paragraphs.add(1);
			} else {
				int index = paragraphs.size();
				while (index > level + 1) {
					paragraphs.remove(index - 1);
					index--;
				}
				paragraphs.set(level, paragraphs.get(level) + 1);
			}
		}
	}

	public void bindVersionsToReq(Collection<Requirement> reqDataList, Collection<RequirementVersion> versionDataList) {
		for (Requirement requirement : reqDataList) {
			requirement.addPreviousVersions(groupVersionForReq(requirement, versionDataList));
			requirement.orderVersions();
		}
	}

	private Collection<RequirementVersion> groupVersionForReq(Requirement req, Collection<RequirementVersion> versionDataList) {
		Collection<RequirementVersion> versions = new ArrayList<>();
		for (RequirementVersion version : versionDataList) {
			if (version.getReqId().equals(req.getReqId())) {
				version.setNature(req.isHlr());
				versions.add(version);
			}
		}
		return versions;
	}

	public void bindCufsToVersions(Collection<RequirementVersion> allVersions, Map<Long, List<Cuf>> cufsDataMap) {
		for (RequirementVersion version : allVersions) {
			if (cufsDataMap.get(version.getVersionId()) != null) {
				version.getCufs().addAll(cufsDataMap.get(version.getVersionId()));
			}
		}
	}

	public void bindTcToVersions(Collection<RequirementVersion> allVersions, Map<Long, List<TestCase>> testCasesDataMap) {
		for (RequirementVersion version : allVersions) {
			version.setTestCases(testCasesDataMap.get(version.getVersionId()));
			version.orderTestCases();
		}
	}

	public void bindTcToHLRVersions(Collection<RequirementVersion> allVersions, Map<Long, List<TestCase>> testCasesDataMap) {
		for (RequirementVersion version : allVersions) {
			version.setTestCases(testCasesDataMap.get(version.getVersionId()));
			version.orderTestCases();
		}
	}

	public void bindIndirectTestCaseToAllHighLevelRequirementVersions(Collection<Requirement> requirements, Map<Long, List<TestCase>> testCasesDataMap) {
		for (Requirement requirement : requirements) {
			if (testCasesDataMap.containsKey(requirement.getReqId())) {
				for (RequirementVersion version : requirement.getVersions()) {
					bindIndirectTestCaseToHLReqVersion(testCasesDataMap, requirement, version);
				}
			}
		}
	}

	private void bindIndirectTestCaseToHLReqVersion(Map<Long, List<TestCase>> testCasesDataMap, Requirement requirement, RequirementVersion version) {
		List<TestCase> boundTestCasesToHlReq = testCasesDataMap.get(requirement.getReqId());

		if (version.isHasTestCases()) {
			addNotLinkedTestCase(boundTestCasesToHlReq, version);
		} else {
			version.setTestCases(boundTestCasesToHlReq);
		}
		version.orderTestCases();
	}

	private void addNotLinkedTestCase(List<TestCase> boundTestCasesToHlReq, RequirementVersion version) {
		for (TestCase tc : boundTestCasesToHlReq) {
			boolean versionAlreadyHasThisTC = version.getTestCases().stream().anyMatch(testCase -> Objects.equals(testCase.getTcId(), tc.getTcId()));
			if (!versionAlreadyHasThisTC) {
				version.getTestCases().add(tc);
			}
		}
	}

	public void bindAttachReqToReq(Collection<Requirement> requirements, Map<Long, List<AttachReq>> attachReqDataMap) {
		for (Requirement req : requirements) {
			req.setAttachReqs(attachReqDataMap.get(req.getReqId()));
		}
	}

	public void bindReqLinkToVersions(Collection<RequirementVersion> allVersions, Map<Long, List<ReqLink>> reqLinkDataMap) {
		for (RequirementVersion version : allVersions) {
			version.setReqLinks(reqLinkDataMap.get(version.getVersionId()));
		}
	}

	public void bindLowReqToHlr(Collection<Requirement> reqsDataList, Map<Long, List<LowReq>> reqLinkedToHlrDataMap) {
		for (Requirement req : reqsDataList) {
			req.setLowReqs(reqLinkedToHlrDataMap.get(req.getReqId()));
		}
	}

	public Collection<Long> getPreviousVersionIds(Collection<Object[]> tuples) {
		Collection<Long> previousVersionIds = new HashSet<>();
		Konvertor konvertor = new Konvertor();

		for (Object[] array : tuples) {
			previousVersionIds.addAll(konvertor.fromStringToIdList((String) array[1]));
		}
		return previousVersionIds;
	}

	// ****************************** private stuff *****************************

	// because a Konvertor is more fun than a mere Converter
	private class Konvertor {

		private Object object;

		Konvertor from(Object toConvert) {
			this.object = toConvert;
			return this;
		}

		@Override
		// it's not really the toString().
		public String toString() {
			if (object == null) {
				return "";
			} else {
				return object.toString();
			}
		}

		public String toRichText(List<String> html) {
			if (object == null) {
				return null;
            }
            String string = encodeSpecialChars(object.toString());

            if (string.contains("img")) {
                string = findAndReplaceImageHeightWidthValue(string);
            }

            if (string.contains("/attachments/download/")) {
                // pattern should match each image tag with an src from server and capture them separately
                Pattern pattern = Pattern.compile("<img.*?src=([\"'])?(?<src>(?:(?!\\2)[^\"'])+attachments\\/download\\/(?<id>\\d+)([^\"'])?).*?>");
                Matcher matcher = pattern.matcher(string);
                while (matcher.find()) {
                    String src = matcher.group("src");
                    String id = matcher.group("id");

                    String str = attachmentImageHelper.getImageBase64String(Long.parseLong(id));

                    if(!str.isBlank()) {
                        string = string.replace(src, "data:image/png;base64," + str);
                    }
                }
            }

            StringBuilder sb = new StringBuilder(string);
            sb.insert(0, "<html>");
            sb.append("</html>");
            String newVal = "<w:altChunk r:id='toto" + html.size() + "' />";
            html.add(sb.toString());

            return newVal;

		}

		/**
		 * Replace image height/width attribute values if they exceed the max
		 * @param string containing html image tag
		 * @return string containing html image tag with new values if applicable
		 */
		private String findAndReplaceImageHeightWidthValue(String string) {
			String regexPattern = "(?<=\")([0-9]+)(?=\")";
			Pattern pattern = Pattern.compile(regexPattern);
			Matcher matcher = pattern.matcher(string);
			while(matcher.find())	{
				String stringAttribute = matcher.group();
				int intAttribute = Integer.parseInt(stringAttribute);
				intAttribute = Math.min(intAttribute, MAX_IMAGE_SIZE);
				string = string.replace(stringAttribute, Integer.toString(intAttribute));
			}
			return string;
		}

		private String encodeSpecialChars(String richText) {
			return richText.replace("“", "&ldquo;")
				.replace("”", "&rdquo;")
				.replace("’", "&rsquo;")
                // issue 9 - remove heading html tags (from Jira or GitLab) because it is used for the table of contents
                .replaceAll("<h\\d\\b[^>]*>", "<p>")
                .replaceAll("</h\\d+>", "</p>");
		}

		public Long toLong() {

			if (object == null) {
				return null;
			}

			Class<?> clazz = object.getClass();
			Long result;

			if (clazz.equals(String.class)) {
				result = Long.valueOf((String) object);
			} else if (clazz.equals(BigInteger.class)) {
				result = ((BigInteger) object).longValue();
			} else if (clazz.equals(Long.class)) {
				result = (Long) object;
			} else if (clazz.equals(Integer.class)) {
				result = ((Integer) object).longValue();
			} else if (clazz.equals(Timestamp.class)) {
				result = ((Timestamp) object).getTime();
			} else if (clazz.equals(Date.class)) {
				result = ((Date) object).getTime();
			} else {
				// well, let's try the leap of faith
				result = Long.valueOf(object.toString());
			}

			return result;
		}

		public Boolean toBoolean() {

			if (object == null) {
				return false;
			}

			Class<?> clazz = object.getClass();
			Boolean result = null;

			if (clazz.equals(String.class)) {
				result = Boolean.valueOf((String) object);
			} else if (clazz.equals(Boolean.class)) {
				result = (Boolean) object;
			}
			return result;
		}

		public Date toDate() {

			if (object instanceof Date) {
				return (Date) object;
			} else {
				return new Date(0);
			}
		}

		public Collection<Long> fromStringToIdList(String strIds) {
            String strIdsWithoutSeparator = strIds.replace(END_TEXT_SEPARATOR, StringUtils.EMPTY);
			Collection<Long> previousIdList = new HashSet<>();
			for (String strId : strIdsWithoutSeparator.split(",")) {
				previousIdList.add(this.from(strId).toLong());
			}
			return previousIdList;
		}
	}
}
