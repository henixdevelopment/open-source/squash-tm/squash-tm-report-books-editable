select P.PROJECT_ID,
       P.NAME
from PROJECT P
where PROJECT_ID in (:projectIds)
and P.PROJECT_TYPE = 'P'
group by P.PROJECT_ID, P.NAME
order by P.NAME ASC
