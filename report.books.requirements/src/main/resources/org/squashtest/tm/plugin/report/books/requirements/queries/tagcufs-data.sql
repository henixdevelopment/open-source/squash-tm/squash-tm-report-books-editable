SELECT
cfv.BOUND_ENTITY_ID as version_id,
cfv.FIELD_TYPE,
cf.LABEL as label,
group_concat(concat(cfvo.label, '=Sep=') order by cfvo.label asc) as value,
cf.INPUT_TYPE as inputType
FROM CUSTOM_FIELD_VALUE cfv
JOIN CUSTOM_FIELD_BINDING cfb ON cfv.CFB_ID = cfb.CFB_ID
JOIN CUSTOM_FIELD cf ON cf.CF_ID = cfb.CF_ID
JOIN CUSTOM_FIELD_VALUE_OPTION cfvo ON cfv.CFV_ID = cfvo.CFV_ID
WHERE cfv.BOUND_ENTITY_TYPE in ('REQUIREMENT_FOLDER', 'REQUIREMENT_VERSION')
AND cfv.BOUND_ENTITY_ID in (:versionIds)
GROUP BY cfv.CFV_ID, cf.CF_ID, cfb.POSITION
order by cfb.POSITION
