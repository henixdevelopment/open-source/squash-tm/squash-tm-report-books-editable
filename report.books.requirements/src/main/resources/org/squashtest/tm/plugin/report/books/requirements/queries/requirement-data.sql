SELECT
    rln.RLN_ID as req_id,
    MAX(case when rf.RES_ID is not null then '1' else '0' end) as isfolder,
    MAX(case when hlr.RLN_ID is not null then '1' else '0' end) as ishlr,
    MAX(clos.DEPTH) as level,
    group_concat(concat(COALESCE(f.REFERENCE, '') ,e.NAME) order by clos.DEPTH desc) as path,
    p.PROJECT_ID as project_Id,
    p.NAME as project_name,
    rv.VERSION_NUMBER as total_Version,
    MAX(case when hlr_rv.REFERENCE = '' then hlr_res.NAME else concat(hlr_rv.REFERENCE, ' - ', hlr_res.NAME) end) as hlr_label,
    rv.RES_ID as version_id,
    rv.REFERENCE as reference,
    res.NAME as name,
    rv.VERSION_NUMBER as version_number,
    coalesce(rv.REQUIREMENT_STATUS, 'WORK_IN_PROGRESS') as status,
    coalesce(rv.CRITICALITY, 'UNDEFINED') as criticality,
    coalesce(ili.LABEL, 'UNDEFINED') as category,
    coalesce(ili.ITEM_TYPE, 'UNDEFINED') as category_type,
    res.CREATED_BY as created_by,
    res.CREATED_ON as created_on,
    res.LAST_MODIFIED_BY as modify_by,
    res.LAST_MODIFIED_ON as modify_on,
    GROUP_CONCAT(distinct case when m.LABEL is not null then concat(m.LABEL, '=Sep=') end) as milestone,
    count(distinct a.ATTACHMENT_ID) as attachments,
    res.DESCRIPTION as description,
    case
        when (:requirementsSortOrder) = 'POSITIONAL'
            then concat(
            LPAD(cast(p.RL_ID as char(5)), 5, '0'),
            '-',
            group_concat(
                coalesce(LPAD(cast(rln_rel_parent.CONTENT_ORDER as char(5)), 5, '0'), LPAD(cast(rlc_parent.CONTENT_ORDER as char(5)), 5, '0')) order by clos.DEPTH desc
            ))
        else REPLACE(group_concat(concat(COALESCE(f.REFERENCE, '') ,e.NAME) order by clos.DEPTH desc),' ','')
    end as order_key
from RLN_RELATIONSHIP_CLOSURE clos
         left join REQUIREMENT_LIBRARY_NODE b on clos.ANCESTOR_ID = b.RLN_ID
         left join REQUIREMENT c on c.RLN_ID = b.RLN_ID
         left join REQUIREMENT_FOLDER d on d.RLN_ID = b.RLN_ID
         left join RESOURCE e on (e.RES_ID = c.CURRENT_VERSION_ID or e.RES_ID = d.RES_ID)
         left join REQUIREMENT_VERSION f on f.RES_ID = e.RES_ID
         inner join REQUIREMENT_LIBRARY_NODE rln on clos.DESCENDANT_ID = rln.RLN_ID
         left join REQUIREMENT_LIBRARY_CONTENT rlc_parent on clos.ANCESTOR_ID = rlc_parent.CONTENT_ID
         left join REQUIREMENT r on rln.RLN_ID = r.RLN_ID
         left join REQUIREMENT_VERSION rv on rv.RES_ID = r.CURRENT_VERSION_ID
         left join REQUIREMENT_FOLDER rf on rf.RLN_ID = rln.RLN_ID
         left join HIGH_LEVEL_REQUIREMENT hlr on hlr.RLN_ID = rln.RLN_ID
         left join RESOURCE res on (res.RES_ID = rv.RES_ID or res.RES_ID = rf.RES_ID)
         left join MILESTONE_REQ_VERSION mrv on e.RES_ID = mrv.REQ_VERSION_ID
         left join MILESTONE m on mrv.MILESTONE_ID = m.MILESTONE_ID
         left join ATTACHMENT a on e.ATTACHMENT_LIST_ID = a.ATTACHMENT_LIST_ID
         left join PROJECT p on rln.PROJECT_ID = p.PROJECT_ID
         left join INFO_LIST_ITEM ili on rv.CATEGORY = ili.ITEM_ID
         left join REQUIREMENT hlr_req on hlr.RLN_ID = hlr_req.RLN_ID
         left join REQUIREMENT_VERSION hlr_rv on hlr_rv.REQUIREMENT_ID = r.HIGH_LEVEL_REQUIREMENT_ID and hlr_req.CURRENT_VERSION_ID = hlr_rv.RES_ID
         left join RESOURCE hlr_res on hlr_res.RES_ID = hlr_rv.RES_ID
         left join RLN_RELATIONSHIP rln_rel_parent on clos.ANCESTOR_ID = rln_rel_parent.DESCENDANT_ID
where clos.DESCENDANT_ID in (select clos1.ANCESTOR_ID from RLN_RELATIONSHIP_CLOSURE clos1 where clos1.DESCENDANT_ID in (:requirementIds))
   or clos.DESCENDANT_ID in (select clos2.ANCESTOR_ID from RLN_RELATIONSHIP_CLOSURE clos2 where clos2.ANCESTOR_ID in (:requirementIds))
group by clos.DESCENDANT_ID, rln.RLN_ID, rf.RES_ID, hlr.RLN_ID, p.PROJECT_ID, rv.RES_ID, res.RES_ID, ili.ITEM_ID
order by order_key
