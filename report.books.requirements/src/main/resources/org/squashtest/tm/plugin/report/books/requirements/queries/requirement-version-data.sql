select res.RES_ID as versionId,
       rv.REFERENCE as reference,
       res.NAME as name,
       rv.VERSION_NUMBER as version_number,
	   coalesce(rv.REQUIREMENT_STATUS, 'WORK_IN_PROGRESS') as status,
	   coalesce(rv.CRITICALITY, 'UNDEFINED') as criticality,
	   coalesce(ili.LABEL, 'UNDEFINED') as category,
	   coalesce(ili.ITEM_TYPE, 'UNDEFINED') as category_type,
       res.CREATED_BY as created_by,
       res.CREATED_ON as created_on,
       res.LAST_MODIFIED_BY as modified_by,
       res.LAST_MODIFIED_ON as modified_on,
	   GROUP_CONCAT(case when m.LABEL is not null then concat(m.LABEL, '=Sep=') end) as milestone,
       count(a.ATTACHMENT_ID) as attachments,
       res.DESCRIPTION as description,
       rv.REQUIREMENT_ID
from RESOURCE res
join REQUIREMENT_VERSION rv on res.RES_ID = rv.RES_ID
join REQUIREMENT r on rv.REQUIREMENT_ID = r.RLN_ID
left join MILESTONE_REQ_VERSION mrv on rv.RES_ID = mrv.REQ_VERSION_ID
left join MILESTONE m on mrv.MILESTONE_ID = m.MILESTONE_ID
left join ATTACHMENT a on res.ATTACHMENT_LIST_ID = a.ATTACHMENT_LIST_ID
left join INFO_LIST_ITEM ili on rv.CATEGORY = ili.ITEM_ID
where res.RES_ID in (:previousVersionIds)
group by res.RES_ID, rv.RES_ID, r.MODE, ili.LABEL, ili.ITEM_TYPE
