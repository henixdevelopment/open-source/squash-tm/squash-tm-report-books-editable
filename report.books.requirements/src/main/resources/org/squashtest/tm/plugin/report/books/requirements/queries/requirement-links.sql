SELECT link.REQUIREMENT_VERSION_ID as versionId,
link.LINK_DIRECTION as linkDirection,
ltype.ROLE_1 as role1,
ltype.ROLE_2 as role2,
project.NAME as projectName,
       reqversion.REFERENCE as reference,
resource.NAME as name,
reqversion.VERSION_NUMBER as versionNumber,
	   GROUP_CONCAT(case when m.LABEL is not null then concat(m.LABEL, '=Sep=') end) as milestone
FROM REQUIREMENT_VERSION_LINK link
JOIN REQUIREMENT_VERSION_LINK_TYPE ltype ON link.LINK_TYPE_ID = ltype.TYPE_ID
JOIN RESOURCE resource ON resource.RES_ID = link.RELATED_REQUIREMENT_VERSION_ID
JOIN REQUIREMENT_VERSION reqversion ON reqversion.RES_ID = link.RELATED_REQUIREMENT_VERSION_ID
JOIN REQUIREMENT_LIBRARY_NODE rln ON reqversion.REQUIREMENT_ID = rln.RLN_ID
JOIN PROJECT project ON project.PROJECT_ID = rln.PROJECT_ID
left join MILESTONE_REQ_VERSION mrv on reqversion.RES_ID = mrv.REQ_VERSION_ID
left join MILESTONE m on mrv.MILESTONE_ID = m.MILESTONE_ID
WHERE link.REQUIREMENT_VERSION_ID in (:versionIds)
group by link.REQUIREMENT_VERSION_ID, link.LINK_DIRECTION, ltype.ROLE_1, ltype.ROLE_2, project.NAME, reqversion.RES_ID, resource.NAME
order by link.REQUIREMENT_VERSION_ID, ltype.ROLE_1, ltype.ROLE_2

