SELECT P.PROJECT_ID,
       P.NAME
from PROJECT P
join MILESTONE_BINDING MB on P.PROJECT_ID = MB.PROJECT_ID
where MB.MILESTONE_ID in (:milestones)
and P.PROJECT_TYPE = 'P'
and P.PROJECT_ID in (:projectIds)
group by P.PROJECT_ID
order by P.NAME
