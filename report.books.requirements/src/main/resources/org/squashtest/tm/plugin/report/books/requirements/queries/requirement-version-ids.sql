select r.RLN_ID,
       group_concat(concat(rv.RES_ID, '=Sep=')) as version_ids
from REQUIREMENT_VERSION rv
    left join REQUIREMENT r on rv.REQUIREMENT_ID = r.RLN_ID
where r.RLN_ID in (:requirementIds)
  and rv.RES_ID != r.CURRENT_VERSION_ID
group by r.RLN_ID;
