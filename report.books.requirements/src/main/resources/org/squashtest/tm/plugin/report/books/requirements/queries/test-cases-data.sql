select distinct TCLN.TCLN_ID,
       VERSION.RES_ID,
	   PROJ.NAME as PROJECTNAME,
       TC.REFERENCE as REFERENCE,
       TCLN.NAME as TESTCASENAME,
	   GROUP_CONCAT(case when MSTONES.LABEL is not null then concat(MSTONES.LABEL, '=Sep=') end) as milestone,
       TC.TC_STATUS as STATUS,
       TC.IMPORTANCE
from TEST_CASE_LIBRARY_NODE TCLN
inner join TEST_CASE TC on TC.TCLN_ID = TCLN.TCLN_ID
inner join PROJECT PROJ on PROJ.PROJECT_ID = TCLN.PROJECT_ID
inner join REQUIREMENT_VERSION_COVERAGE VERIF on VERIF.VERIFYING_TEST_CASE_ID = TCLN.TCLN_ID
inner join REQUIREMENT_VERSION VERSION on VERSION.RES_ID = VERIF.VERIFIED_REQ_VERSION_ID
left join MILESTONE_TEST_CASE TCSTONES on TC.TCLN_ID = TCSTONES.TEST_CASE_ID
left join MILESTONE MSTONES on TCSTONES.MILESTONE_ID = MSTONES.MILESTONE_ID
where VERSION.RES_ID in (:versionIds)
group by TCLN.TCLN_ID, VERSION.RES_ID, PROJ.NAME, TC.REFERENCE, TCLN.NAME, TC.IMPORTANCE, TC.TCLN_ID
order by VERSION.RES_ID
