/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.requirements.query

import org.springframework.context.MessageSource
import org.squashtest.tm.plugin.report.books.requirements.beans.AttachReq
import org.squashtest.tm.plugin.report.books.requirements.beans.Cuf
import org.squashtest.tm.plugin.report.books.requirements.beans.I18nHelper
import org.squashtest.tm.plugin.report.books.requirements.beans.LowReq
import org.squashtest.tm.plugin.report.books.requirements.beans.Project
import org.squashtest.tm.plugin.report.books.requirements.beans.ReqLink
import org.squashtest.tm.plugin.report.books.requirements.beans.Requirement
import org.squashtest.tm.plugin.report.books.requirements.beans.RequirementVersion
import org.squashtest.tm.plugin.report.books.requirements.beans.TestCase
import spock.lang.Specification
import spock.lang.Unroll

class RequirementTreeQueryFormatterTest extends Specification{

	RequirementTreeQueryFormatter formatter = new RequirementTreeQueryFormatter()
	MessageSource messageSource = Mock()

	def setup() {
		I18nHelper.msgSource = messageSource
	}

	//TEST CONVERTER METHODS

	def "should return projects data"() {

		given :
			Collection<Object[]> rawProjects = new ArrayList<Object[]>(){{
				add([1, "project 1"] as Object[])
				add([2, "project 2"] as Object[])
			}}

		when :
			Collection<Project> projects = formatter.toProjectData(rawProjects)

		then :
			projects.size() == 2
			projects[0].getProjectId() == 1
			projects[1].projectName == "project 2"
	}

	def "should return requirements data"() {
		given :
			Collection<Object[]> rawRequirements = new ArrayList<Object[]>(){{
				add([1, 0, 0, 0, "path", 1, "project 1", 2, "HlrLabel", 1, "ref", "reqName", 1, "OBSOLETE", "MINOR", "functional", "SYS", "admin", null, null, null, null, 0, "description", 0, "classique"] as Object[])
				add([1, 0, 1, 0, "path", 1, "project 1", 2, "", 1, null, "reqName", 1, "OBSOLETE", "MINOR", "functional", "SYS", "admin", null, null, null, null, 0, "description", 1, "classique"] as Object[])
				add([1, 1, 0, 0, "path", 1, "project 1", 2, null, 1, "", "reqName", 1, "OBSOLETE", "MINOR", "functional", "SYS", "admin", null, null, null, null, 0, "description", 2, "classique"] as Object[])
			}}
		boolean printMilestones = false;

		when :
			Collection<Requirement> requirements = formatter.toRequirementData(rawRequirements, new ArrayList<String>(), printMilestones)

		then :
			requirements.size() == 3
			requirements[0].getVersions().size() == 1
			requirements[0].getVersions().get(0).getName() == "reqName"
			!requirements[0].isFolder()
			requirements[2].isFolder()
			!requirements[0].isHlr()
			requirements[1].isHlr()
			requirements[0].isHasReference()
			requirements[0].getCurrentVersionRef() == "ref"
			!requirements[1].isHasReference()
			!requirements[2].isHasReference()
			requirements[0].isHasHighLevelReq()
			requirements[0].getHighLevelReqLabel() == "HlrLabel"
			!requirements[1].isHasHighLevelReq()
			!requirements[2].isHasHighLevelReq()


	}

	@Unroll
	def "should return testCase Data Map with status : #status and importance : #importance"() {
		given :
			def tcStatus = status
			def tcImportance = importance
			Collection<Object[]> rawTestCases = new ArrayList<Object[]>(){{
				add([1, 121, "project 1", "TCRef", "TCName", null, "TCStatus", "Importance"] as Object[])
				add([2, 215, "project 1", "TCRef2", "TCName2", null, tcStatus, tcImportance] as Object[])
				add([3, 215, "project 1", "TCRef3", "TCName3", null, "TCStatus3", "Importance3"] as Object[])
			}}
			I18nHelper.msgSource.getMessage(statusKey, null, _) >> expectedStatus
			I18nHelper.msgSource.getMessage(importanceKey, null, _) >> expectedImportance

		when :
			Map <Long, List<TestCase>> testCaseDataMap = formatter.toTestCaseDataMap(rawTestCases)

		then :
			testCaseDataMap.get(121L).size() == 1
			testCaseDataMap.get(215L).size() == 2
			testCaseDataMap.get(215L)[0].getTcId() == 2
			testCaseDataMap.get(215L).get(0).getTcStatus() == expectedStatus
			testCaseDataMap.get(215L).get(0).getTcImportance() == expectedImportance

		where:
			status          | expectedStatus          | statusKey                                                     | importance  | expectedImportance | importanceKey
			"TO_BE_UPDATED" | "À mettre à jour"       | "report.books.requirements.testcases.status.to_be_updated"    | "LOW"       | "Faible"           | "report.books.requirements.testcases.importance.low"
		    "UNDER_REVIEW"  | "À approuver"           | "report.books.requirements.testcases.status.under_review"     | "MEDIUM"    | "Moyenne"          | "report.books.requirements.testcases.importance.medium"
			"APPROVED"      | "APPROVED"              | "report.books.requirements.testcases.status.approved"         | "HIGH"      | "Haute"            | "report.books.requirements.testcase.importance.high"
			"OBSOLETE"      | "Obsolète"              | "report.books.requirements.testcases.status.obsolete"         | "VERY_HIGH" | "Très haute"       | "report.books.requirements.testcases.importance.very_high"
			"status"        | "En cours de rédaction" | "report.books.requirements.testcases.status.work_in_progress" | "importace" | "indéfinie"        | "report.books.requirements.testcases.importance.undefined"
	}

	def "should return attachReq Data Map"() {
		given :
		Collection<Object[]> rawAttachReq = new ArrayList<Object[]>(){{
			add([121, 1, "ref1", "reqName1", null, "criticality1", 0] as Object[])
			add([215, 1, "ref2", "reqName2", null, "criticality2", 0] as Object[])
			add([215, 1, "ref3", "reqName3", null, "criticality3", 1] as Object[])
		}}

		when :
		Map<Long, List<AttachReq>> attachReqDataMap = formatter.toAttachReqDataMap(rawAttachReq)

		then :
			attachReqDataMap.get(121L).size() == 1
			attachReqDataMap.get(215L).size() == 2
			attachReqDataMap.get(215L).get(1).isParent()
	}

	def "should return cuf Data Map"() {
		given :
		Collection<Object[]> rawCuf = new ArrayList<Object[]>(){{
			add([121, "Num", "cufNum1", 44, "InputType"] as Object[])
			add([215, "RTF", "cufRichText", "this is a rich text", "InputType"] as Object[])
			add([215, "Num", "cufNum2", 94, "InputType"] as Object[])
		}}

		when :
		Map<Long, List<Cuf>> cufDataMap = formatter.toCufDataMap(rawCuf, new ArrayList<String>())

		then :
		cufDataMap.get(121L).size() == 1
		cufDataMap.get(215L).size() == 2
		cufDataMap.get(215L).get(0).getValue().contains("<w:altChunk")
	}

	def "should return ReqLinkDataMap"() {
		given :
		Collection<Object[]> rawReqLink = new ArrayList<Object[]>(){{
			add([1, 0, 'requirement-version.link.type.parent', 'requirement-version.link.type.child', 'project', 'ref', 'name1', 1, ''] as Object[])
			add([1, 1, 'requirement-version.link.type.parent', 'requirement-version.link.type.child', 'project', 'ref', 'name2', 1, ''] as Object[])
			add([2, 1, 'requirement-version.link.type.parent', 'requirement-version.link.type.child', 'project', 'ref', 'name3', 1, ''] as Object[])
		}}
		I18nHelper.msgSource.getMessage('requirement-version.link.type.parent', null, _) >> "Parent"
		I18nHelper.msgSource.getMessage('requirement-version.link.type.child', null, _) >> "Enfant"

		when :
			Map<Long, List<ReqLink>> reqLinkDataMap = formatter.toReqLinkDataMap(rawReqLink)

		then :
			reqLinkDataMap.size() == 2
			reqLinkDataMap.get(1L).size() == 2
			reqLinkDataMap.get(1L)[0].getRole() == "Enfant"
	}

	@Unroll
	def "should return requirement version data where criticality : #criticality and status : #status"() {
		given :
		def reqCriticality = criticality
		def reqStatus = status
		Collection<Object[]> rawRequirementsVersion = new ArrayList<Object[]>(){{
			add([1, "ref", "reqName", 1,reqStatus, reqCriticality, "requirement.category.cat_ergonomic", "SYS", "admin", "2022-04-06 14:50:22", null, "", null, 0, "description", 121] as Object[])
			add([2, null, "reqName2", 1,reqStatus, reqCriticality, "category", "cat", "admin", "2022-04-06 14:50:22", "admin", "2022-04-06 14:50:22", null, 0, "description", 121] as Object[])
			add([3, "", "reqName2", 1,reqStatus, reqCriticality, "category", "cat", "admin", "2022-04-06 14:50:22", "admin", "2022-04-06 14:50:22", null, 0, "description", 121] as Object[])
		}}
		I18nHelper.msgSource.getMessage("report.books.requirements.requirement.category.cat_ergonomic", null, _) >> "Ergonomique"
		I18nHelper.msgSource.getMessage(criticalityKey, null, _) >> expectedCriticality
		I18nHelper.msgSource.getMessage(statusKey, null, _) >> expectedStatus

		when :
			Collection<RequirementVersion> requirementVersionData = formatter.toRequirementVersionData(rawRequirementsVersion, new ArrayList<String>())

		then :
			requirementVersionData.size() == 3

			requirementVersionData[0].getName() == "reqName"
			!requirementVersionData[0].isModify()
			requirementVersionData[0].isHasReference()
			requirementVersionData[0].getReference() == "ref"
			requirementVersionData[0].getCategory() == "Ergonomique"
			requirementVersionData[0].getStatus() == expectedStatus
			requirementVersionData[0].getCriticality() == expectedCriticality

			requirementVersionData[1].getCategory() == "category"
			requirementVersionData[1].isModify()
			!requirementVersionData[1].isHasReference()

			!requirementVersionData[2].isHasReference()

		where :
		criticality   | expectedCriticality | criticalityKey                                    | status         | expectedStatus          | statusKey
		"MAJOR"       | "Majeure"           | "report.books.requirements.criticality.major"     | "APPROVED"     | "Approuvée"             | "report.books.requirements.status.approved"
		"MINOR"       | "Mineure"           | "report.books.requirements.criticality.minor"     | "OBSOLETE"     | "Obsolète"              | "report.books.requirements.status.obsolete"
		"CRITICAL"    | "Critique"          | "report.books.requirements.criticality.critical"  | "UNDER_REVIEW" | "À approuver"           | "report.books.requirements.status.under_review"
		"criticality" | "Non définie"       | "report.books.requirements.criticality.undefined" | "status"       | "En cours de rédaction" | "report.books.requirements.status.work_in_progress"
	}

	def "should return req linked to hlr Data Map"() {
		given :
		Collection<Object[]> rawReqLinkedToHlr = new ArrayList<Object[]>(){{
			add([121, 1, "project 1", "ref1", "name1", null, "criticality1", "status1"] as Object[])
			add([215, 1, "project 1", "ref2", "name2", null, "criticality2", "status2"] as Object[])
			add([215, 1, "project 2", "ref3", "name3", null, "criticality3", "status3"] as Object[])
		}}
		when :
		Map<Long, List<LowReq>> reqLinkedToHlr = formatter.toReqLinkedToHlrDataMap(rawReqLinkedToHlr)

		then :
		reqLinkedToHlr.get(121L).size() == 1
		reqLinkedToHlr.get(215L).size() == 2
		reqLinkedToHlr.get(215L).get(0).getReference() == "ref2"
	}

	// TEST BIDING METHODS

	def "should bind requirements to project"() {
		given :
			Project project1 = new Project()
			project1.setProjectId(1L)
			project1.setProjectName("projectName")
			Project project2 = new Project()
			project2.setProjectId(2L)
			project2.setProjectName("projectName2")
			Collection<Project> projects = Arrays.asList(project1, project2)
			Requirement req1 = new Requirement()
			req1.setProjectId(1L)
			req1.setProjectName("projectName")
			req1.setLevel(0)
			Requirement req2 = new Requirement()
			req2.setProjectId(1L)
			req2.setProjectName("projectName")
			req2.setLevel(1)
			Requirement req3 = new Requirement()
			req3.setProjectId(1L)
			req3.setProjectName("projectName")
			req3.setLevel(0)
			Requirement req4 = new Requirement()
			req4.setProjectId(2L)
			req4.setProjectName("projectName2")
			req4.setLevel(0)
			Collection<Requirement> requirements = new ArrayList<>(Arrays.asList(req1, req2, req3, req4))

		when :
			formatter.bindReqToProject(projects, requirements)

		then :
			project1.getRequirements().size() == 3
			project1.getRequirements()[0].getParagraph() == "1"
			project1.getRequirements()[1].getParagraph() == "1.1"
			project1.getRequirements()[2].getParagraph() == "2"
			project2.getRequirements()[0].getParagraph() == "1"
	}

	def "should bind req versions to a requirement"() {

		given :
			RequirementVersion version1 = new RequirementVersion()
			version1.setReqId(121L)
			version1.setVersionNumber(2L)
			version1.setName("version1")
			RequirementVersion version2 = new RequirementVersion()
			version2.setReqId(121L)
			version2.setVersionNumber(1L)
			RequirementVersion version3 = new RequirementVersion()
			version3.setReqId(215L)
			version3.setVersionNumber(1L)
			Collection<RequirementVersion> versions = new ArrayList<>(Arrays.asList(version1, version2, version3))
			Requirement req1 = new Requirement()
			req1.setReqId(121L)
			Requirement req2 = new Requirement()
			req2.setReqId(215L)
			Collection<Requirement> requirements = new ArrayList<>(Arrays.asList(req1, req2))

		when :
			formatter.bindVersionsToReq(requirements, versions)

		then :
			req1.getVersions().size() == 2
			req1.getVersions()[1].getName() == "version1"
			req2.getVersions().size() == 1
	}

	def "should bind cufs to requirement version"() {
		given :
			RequirementVersion version1 = new RequirementVersion()
			version1.setVersionId(1L)
			RequirementVersion version2 = new RequirementVersion()
			version2.setVersionId(2L)
			RequirementVersion version3 = new RequirementVersion()
			version3.setVersionId(3L)
			Collection<RequirementVersion> versions = new ArrayList<>(Arrays.asList(version1, version2))
			Map<Long, List<Cuf>> cufDataMap = new HashMap<Long, List<Cuf>>(){{
				put(1L, Arrays.asList(new Cuf(), new Cuf()))
				put(2L, Arrays.asList(new Cuf()))
			}}

		when :
			formatter.bindCufsToVersions(versions, cufDataMap)

		then :
			version1.getCufs().size() == 2
			version1.isHasCufs()
			version2.getCufs().size() == 1
			!version3.isHasCufs()
	}

	def "should bind Test Case to requirement version"() {
		given :
		RequirementVersion version1 = new RequirementVersion()
		version1.setVersionId(1L)
		RequirementVersion version2 = new RequirementVersion()
		version2.setVersionId(2L)
		Collection<RequirementVersion> versions = new ArrayList<>(Arrays.asList(version1, version2))
		Map<Long, List<TestCase>> tcDataMap = new HashMap<Long, List<TestCase>>(){{
			put(1L, Arrays.asList(new TestCase()))
		}}

		when :
		formatter.bindTcToVersions(versions, tcDataMap)

		then :
		version1.isHasTestCases()
		version1.getTestCases().size() == 1
		!version2.isHasTestCases()
	}

	def "should bind requirement links to requirement version"() {
		given :
		RequirementVersion version1 = new RequirementVersion()
		version1.setVersionId(1L)
		RequirementVersion version2 = new RequirementVersion()
		version2.setVersionId(2L)
		Collection<RequirementVersion> versions = new ArrayList<>(Arrays.asList(version1, version2))
		Map<Long, List<ReqLink>> reqLinkDataMap = new HashMap<Long, List<ReqLink>>(){{
			put(1L, Arrays.asList(new ReqLink()))
		}}

		when :
		formatter.bindReqLinkToVersions(versions, reqLinkDataMap)

		then :
		version1.isHasReqLinks()
		version1.getReqLinks().size() == 1
		!version2.isHasReqLinks()
	}

	def "should bind attach reqs to requirement"() {
		given :
		Requirement req1 = new Requirement()
		req1.setReqId(1L)
		Requirement req2 = new Requirement()
		req2.setReqId(2L)
		Collection<Requirement> requirements = new ArrayList<>(Arrays.asList(req1, req2))
		Map<Long, List<AttachReq>> attachReqDataMap = new HashMap<Long, List<AttachReq>>(){{
			put(1L, Arrays.asList(new AttachReq()))
		}}

		when :
		formatter.bindAttachReqToReq(requirements, attachReqDataMap)

		then :
		req1.isHasAttachReq()
		req1.getAttachReqs().size() == 1
		!req2.isHasAttachReq()
	}

	def "should bind low reqs to high level requirement"() {
		given :
		Requirement req1 = new Requirement()
		req1.setReqId(1L)
		Requirement req2 = new Requirement()
		req2.setReqId(2L)
		Collection<Requirement> requirements = new ArrayList<>(Arrays.asList(req1, req2))
		Map<Long, List<LowReq>> lowReqDataMap = new HashMap<Long, List<LowReq>>(){{
			put(1L, Arrays.asList(new LowReq()))
		}}

		when :
		formatter.bindLowReqToHlr(requirements, lowReqDataMap)

		then :
		req1.isHasLowReq()
		req1.getLowReqs().size() == 1
		!req2.isHasLowReq()
	}

	def "should bind test cases to high level requirement"() {
		given :
		Requirement req1 = new Requirement()
		req1.setReqId(1L)
		req1.setHlr(1L)
		req1.setVersions(new ArrayList<>(Arrays.asList(new RequirementVersion(), new RequirementVersion())))
		Collection<Requirement> requirements = new ArrayList<>(Arrays.asList(req1))
		Map<Long, List<TestCase>> testCaseDataMap = new HashMap<Long, List<TestCase>>(){{
			put(1L, Arrays.asList(new TestCase()))
		}}

		when :
		formatter.bindIndirectTestCaseToAllHighLevelRequirementVersions(requirements, testCaseDataMap)

		then :
		req1.getVersions()[0].isHasTestCases()
		req1.getVersions()[0].getTestCases().size() == 1
		req1.getVersions()[1].isHasTestCases()
		req1.getVersions()[1].getTestCases().size() == 1
	}

	def "should get previous ids"() {
		given :
			Collection<Object[]> idList = new ArrayList<Object[]>(){{
				add([1, "121,215,44"] as Object[])
				add([2, "94,89"] as Object[])
			}}

		when :
			Collection<Long> previousVersionsIds = formatter.getPreviousVersionIds(idList)

		then :
			previousVersionsIds.size() == 5
	}
}
