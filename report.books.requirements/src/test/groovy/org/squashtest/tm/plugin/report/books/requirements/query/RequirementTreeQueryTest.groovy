/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.requirements.query

import org.squashtest.tm.plugin.report.books.requirements.beans.Requirement
import org.squashtest.tm.plugin.report.books.requirements.beans.RequirementVersion
import spock.lang.Specification

class RequirementTreeQueryTest extends Specification{

	RequirementsTreeQuery requirementTreeQuery = new RequirementsTreeQuery()
	Collection<Requirement> requirements
	def setup() {
		RequirementVersion version1 = new RequirementVersion()
		version1.setVersionId(1L)
		RequirementVersion version2 = new RequirementVersion()
		version2.setVersionId(2L)
		RequirementVersion version3 = new RequirementVersion()
		version3.setVersionId(3L)
		Requirement req1 = new Requirement()
		req1.setVersions(Arrays.asList(version1, version2))
		req1.setFolder(0L)
		Requirement req2 = new Requirement()
		req2.setVersions(Arrays.asList(version3))
		req2.setFolder(0L)
		Requirement folder = new Requirement()
		folder.setFolder(1L)
		folder.setVersions(new ArrayList<RequirementVersion>())
		requirements = new ArrayList<>(Arrays.asList(req1, req2, folder))
	}

	def "should extract versions from requirements"() {
		given :
		Collection<RequirementVersion> allVersions = new ArrayList<>()

		when :
			requirementTreeQuery.extractVersions(requirements, allVersions)
		then :
		allVersions.size() == 3

	}

}
